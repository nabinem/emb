<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('CmsRoute', 'Lib/Router');

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
//	Router::connect('/', array('controller' => 'pages', 'action' => 'view', 'test'));

	Router::connect('/admin', array('controller' => 'pages', 'action' => 'read', 'admin' => false));
	Router::connect('/admin/', array('controller' => 'pages', 'action' => 'read', 'admin' => false));
    Router::connect('/l2g1n/*', array('controller' => 'users', 'action' => 'login', 'admin' => true));
    // prefix routing default routes with admin prefix
    Router::connect("/admin/:controller", array('action' => 'index', 'prefix' => 'admin', 'admin' => true));
    Router::connect("/admin/:controller/:action/*", array('prefix' => 'admin', 'admin' => true));


/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
//	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * This router connect uses CmsRoute which uses database to find route.
 * If found it will override anything below it.
 */
//    Router::connect('/contact',array('controller' => 'pages', 'action' => 'contact', 'admin' => false));
    Router::connect('/pages/overview_menu',array('controller' => 'pages', 'action' => 'overview_menu', 'admin' => false));
    Router::connect('/pages/ajax_navmenu/*',array('controller' => 'pages', 'action' => 'ajax_navmenu', 'admin' => false));
    Router::connect('/pages/ajax_action/*',array('controller' => 'pages', 'action' => 'ajax_action', 'admin' => false));
    Router::connect('/pages/ajax_navfootmenu/*',array('controller' => 'pages', 'action' => 'ajax_navfootmenu', 'admin' => false));
    Router::connect('/pages/search/*', array('controller' => 'pages', 'action' => 'search', 'admin' => false ), array('named' => array('page' => '[\d]+'), ) );
    Router::connect('/:slug1/:slug2', array('controller' => 'pages', 'action' => 'show', 'admin' => false), array('pass' => array('slug2')));
    Router::connect('/',array('controller' => 'pages', 'action' => 'load_home', 'admin' => false));
    Router::connect('/*',array('controller' => 'pages', 'action' => 'view'), array('routeClass' => 'CmsRoute'));


/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';