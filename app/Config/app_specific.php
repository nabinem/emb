<?php

/**
 * Specific app settings and functions goes here.
 * 
 */

Configure::write('inputDefaults', array(
        'between'   => '<div class="controls">',
        'after'     => '</div>',
        'div'       => array('class' => 'control-group'),
        'label'     => array('class' => 'control-label'),
        'format'    => array('before', 'label', 'between', 'input', 'after', 'error'),
        //'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline'), 'class' => 'inputError')
    ));

