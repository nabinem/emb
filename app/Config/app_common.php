<?php

/**
 * Common app settings and functions goes  here that can be used in other projects.
 *
 */

/**
 * Load Plugins
 */
CakePlugin::load(array('Upload', 'AuditLog'));

/**
 * Available languages for the site content.
 *
 * English 'eng' must be always there.
 */
Configure::write('Setting.languages', array(
    'eng' => 'English',
    'dut' => 'Dutch',
    'tha' => 'Thai'
));

/**
 * Starting language for the site .
 */
Configure::write('Config.language', 'eng'); // do not change from 'eng', change in app_controller
Configure::write('Setting.languageDefault', Configure::read('Config.language'));

/**
 * Translation Cache.
 */
 Cache::config('translation', array(
	'engine' => 'File', //[required]
	'duration'=> 3600*24*7, //[optional]
	'prefix' => 'cake_translation_', //[optional]  prefix every cache file with this string
	'lock' => false, //[optional]  use file locking
	'serialize' => true, // [optional]
	'mask' => 0666, // [optional] permission mask to use when creating cache files
));

/**
 * Returns timezone array with diff in Hours OR TimeZone array of single key.
 *
 * @param       string  $key              Optional: key i.e. time difference.
 * @param       boolean $returnValue      returns just the value if key is not empty and this set to true.
 * @return      mixed   Returns an array of timezones or (array or value) of just one timezone if key is set and found,
 *                      else returns false
 */
function timeZoneList($key = null, $returnValue = true) {
        $timeZones = array(
                "-12.0" => "(GMT -12:00) Eniwetok, Kwajalein",
                "-11.0" => "(GMT -11:00) Midway Island, Samoa",
                "-10.0" => "(GMT -10:00) Hawaii",
                "-9.0" => "(GMT -9:00) Alaska",
                "-8.0" => "(GMT -8:00) Pacific Time (US &amp; Canada)",
                "-7.0" => "(GMT -7:00) Mountain Time (US &amp; Canada)",
                "-6.0" => "(GMT -6:00) Central Time (US &amp; Canada), Mexico City",
                "-5.0" => "(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima",
                "-4.0" => "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz",
                "-3.5" => "(GMT -3:30) Newfoundland",
                "-3.0" => "(GMT -3:00) Brazil, Buenos Aires, Georgetown",
                "-2.0" => "(GMT -2:00) Mid-Atlantic",
                "-1.0" => "(GMT -1:00 hour) Azores, Cape Verde Islands",
                "0.0" => "(GMT) Western Europe Time, London, Lisbon, Casablanca",
                "1.0" => "(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris",
                "2.0" => "(GMT +2:00) Kaliningrad, South Africa",
                "3.0" => "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg",
                "3.5" => "(GMT +3:30) Tehran",
                "4.0" => "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi",
                "4.5" => "(GMT +4:30) Kabul",
                "5.0" => "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent",
                "5.5" => "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi",
                "5.75" => "(GMT +5:45) Kathmandu",
                "6.0" => "(GMT +6:00) Almaty, Dhaka, Colombo",
                "7.0" => "(GMT +7:00) Bangkok, Hanoi, Jakarta",
                "8.0" => "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong",
                "9.0" => "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
                "9.5" => "(GMT +9:30) Adelaide, Darwin",
                "10.0" => "(GMT +10:00) Eastern Australia, Guam, Vladivostok",
                "11.0" => "(GMT +11:00) Magadan, Solomon Islands, New Caledonia",
                "12.0" => "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka"
        );

        if($key == null){
                return $timeZones;
        }

        if(key_exists($key, $timeZones)){
                if($returnValue){
                        return $timeZones[$key];
                }
                return array($key => $timeZones[$key]);
        }
        // if no key is found.
        return false;
}

/**
 * Returns array with keys of controller/action used for urls.
 *
 * @param type $action
 * @param type $controller
 * @param type $
 * @return array
 */
function aca() {

    $args = func_get_args();

    $action=($args[0])? $args[0] : null;
    $controller=($args[1])? $args[1] : null;
    unset($args[0]);
    unset($args[1]);
    rsort($args);
    $return = array();

    if($action) $return['action'] = $action;
    if($controller) $return['controller'] = $controller;

    $return = $return + $args;
    return $return;

}

/**
 * Language function for static strings.
 *
 */
function ___() {

	$params = func_get_args();

	if(empty($params)){
		return "";
	}

	$locale = Configure::read('Config.language');

	$defaultLocale = Configure::read('Setting.languageDefault');

	$staticTranslationModel = 'TranslationString';

	$Translations = ClassRegistry::init($staticTranslationModel);

	$allTranslations = $Translations->getAllString();

	$keyFind = noLineBreak($params[0]);
	$keyFind_lowercase = strtolower($keyFind);

	if(!empty($allTranslations[$locale.'#'.$keyFind]) OR !empty($allTranslations[$locale.'#'.$keyFind_lowercase])){
		$params[0] = empty($allTranslations[$locale.'#'.$keyFind]) ? $allTranslations[$locale.'#'.$keyFind_lowercase] : $allTranslations[$locale.'#'.$keyFind];
	}else if(!empty($allTranslations[$defaultLocale.'#'.$keyFind]) OR !empty($allTranslations[$defaultLocale.'#'.$keyFind])){
                $params[0] = empty($allTranslations[$defaultLocale.'#'.$keyFind]) ? $allTranslations[$defaultLocale.'#'.$keyFind_lowercase] : $allTranslations[$defaultLocale.'#'.$keyFind];
	}else{
		$Translations->newEntry($params[0]);
	}

	return call_user_func_array('sprintf', $params);

}

function noLineBreak($string = '') {
	$re = str_replace(array("\n\n","\r","\r\n", "\n\r", "\n"), "", $string);
	return $re;
}


function prd($d=null) {
        pr($d);die;
}
