<?php
App::uses('AppModel', 'Model');
/**
 * Page Model
 *
 */
class Page extends AppModel {

public $actsAs = array(
        'Upload.Upload' => array(

			 'image' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '100x100'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'video_chrome' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',

                        'mimetypes' => array(
                                'video/mp4'
                        ),
                        'deleteOnUpdate' => true
                ),
                'video_others' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',

                        'mimetypes' => array(
                                'video/ogg'
                        ),
                        'deleteOnUpdate' => true
                ),
                
                'dimage' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '128x320'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'dimage1' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '100x35'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'dimage2' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '128x320'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'himage1' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '100x35'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'himage2' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '128x320'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                )
                                 
        ),
        'MultiTranslate' => array(
            'title',
            'content_short',
            'content_long'
        )
);

// Use a different model (and table)
    public $translateModel = 'PageI18n';
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

        public $belongsTo = array(
            'Metum' => array(
                'className' => 'Metum',
                'foreignKey' => 'metum_id'
            ),
        );

        public $hasOne = array(
            'Menu' => array(
                'className' => 'Menu',
                'foreignKey' => 'extra_params',
                'conditions' => array('Menu.controller_name' => 'pages'),
                'recursive' => 2
            ),
        );
/*       public $hasMany = array(
            'PageImage' => array(
                'className' => 'PageImage',
                'foreignKey' => 'page_id',           
                'dependent' => true
            ),
        );
*/
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),	        
        'dimage' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Image is required',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
        ),
        'dimage1' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Image is required',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
        ),
        'dimage2' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Image is required',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
        )
        
	);
}
