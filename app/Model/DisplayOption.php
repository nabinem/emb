<?php
App::uses('AppModel', 'Model');
/**
 * DisplayOption Model
 *
 * @property Menu $Menu
 */
class DisplayOption extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

        
        public $belongsTo = array(
                'HeaderElement' => array(
                        'className' => 'PageElement',
                        'foreignKey' => 'header',
                        'conditions' => array('HeaderElement.type' => 'header'),
                        'fields' => '',
                        'order' => ''
                ),
                'FooterElement' => array(
                        'className' => 'PageElement',
                        'foreignKey' => 'footer',
                        'conditions' => array('FooterElement.type' => 'footer'),
                        'fields' => '',
                        'order' => ''
                ),
                'TopBannerElement' => array(
                        'className' => 'PageElement',
                        'foreignKey' => 'top_banner',
                        'conditions' => array('TopBannerElement.type' => 'top-banner'),
                        'fields' => '',
                        'order' => ''
                ),
                'PageDescriptionElement' => array(
                        'className' => 'PageElement',
                        'foreignKey' => 'page_description',
                        'conditions' => array('PageDescriptionElement.type' => 'page-description'),
                        'fields' => '',
                        'order' => ''
                ),
        );

}
