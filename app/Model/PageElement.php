<?php
App::uses('AppModel', 'Model');
/**
 * PageElement Model
 *
 */
class PageElement extends AppModel {
        
        public $actsAs = array(
                'AuditLog.Auditable'
        );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
        
        
        public $elementType = array(
                'header' => 'Header',
                'footer' => 'Footer',
                'top_banner' => 'Top Banner',
                'page_description' => 'Page Description',
                'others' => 'Others'
        );
        
        public $defaultElement = array(
                'header' => 'first',
                'footer' => 'first',
                'top_banner' => 'first',
                'page_description' => null,
                'other' => null
        );
        
        
        public function getElementList($type=null) {
                $options = array();
                if($type) {
                        $options['conditions'] = array('type' => $type);
                }
                
                return $this->find('list', $options);
        }
        
        public function getDefaultElement($type, $id=null) {
                $options = array(
                                'id' => $id,
                                'type' => $type
                        );
                $return = $this->field('content', $options);
                if(!$return AND !is_null($this->defaultElement[$type])) {
                        $return = $this->find('first', array('conditions' => array('PageElement.type' => $type)));
                        if($return) return $return['PageElement']['content'];
                }else if($return){
                        return $return;
                }
                return '';
        }
                
                
}
