<?php

App::uses('AppModel', 'Model');

/**
 * Menu Model
 *
 * @property Menu $ParentMenu
 * @property Menu $ChildMenu
 */
class Menu extends AppModel {

        /**
         *  Acts as a tree.
         */
        public $actsAs = array('Tree',
            'MultiTranslate' => array(
                'name'
           )
        );

        /**
         * Display field
         *
         * @var string
         */
        public $displayField = 'name';

        /**
         * Nesting limit.
         *
         * If the limit is 5 levels then there can be only 0 > 1 > 2 > 3 > 4 levels.
         *
         * @var integer
         */
        public $nestingLimit = 3;

        /**
         * Validation rules
         *
         * @var array
         */
        public $validate = array(
                'name' => array(
                        'notempty' => array(
                                'rule' => array('notempty'),
                        //'message' => 'Your custom message here',
                        //'allowEmpty' => false,
                        //'required' => false,
                        //'last' => false, // Stop validation after this rule
                        //'on' => 'create', // Limit validation to 'create' or 'update' operations
                        ),
                ),
                'is_visible' => array(
                        'boolean' => array(
                                'rule' => array('boolean'),
                        //'message' => 'Your custom message here',
                        //'allowEmpty' => false,
                        //'required' => false,
                        //'last' => false, // Stop validation after this rule
                        //'on' => 'create', // Limit validation to 'create' or 'update' operations
                        ),
                ),
                'routing_link' => array(
                        'isUnique' => array(
                                'rule' => array('isUnique'),
                                'message' => 'Routing link is already in use, no duplicates',
                        'allowEmpty' => true,
                        //'required' => false,
                        //'last' => false, // Stop validation after this rule
                        //'on' => 'create', // Limit validation to 'create' or 'update' operations
                        ),
                        'checkLink' => array(
                                'rule' => array('checkRoutingLink'),
                                'message' => 'Routing link is invalid. Starts with /, no spaces',
                        )
                ),
                'is_active' => array(
                        'boolean' => array(
                                'rule' => array('boolean'),
                        //'message' => 'Your custom message here',
                        //'allowEmpty' => false,
                        //'required' => false,
                        //'last' => false, // Stop validation after this rule
                        //'on' => 'create', // Limit validation to 'create' or 'update' operations
                        ),
                ),
                'parent_id' => array(
                        'nestingLimit' => array(
                                'rule' => array('checkNestingLimit', null),
                                'message' => 'Nesting Limit Reached. Please choose a higher level parent.',
                        //'allowEmpty' => false,
                        //'required' => false,
                        //'last' => false, // Stop validation after this rule
                        //'on' => 'create', // Limit validation to 'create' or 'update' operations
                        )
                )
        );

        //The Associations below have been created with all possible keys, those that are not needed can be removed

        /**
         * belongsTo associations
         *
         * @var array
         */
        public $belongsTo = array(
                'ParentMenu' => array(
                        'className' => 'Menu',
                        'foreignKey' => 'parent_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => ''
                ),
                'Page' => array(
                        'className' => 'Page',
                        'foreignKey' => 'extra_params',
                        'conditions' => array('Menu.controller_name' => 'pages'),
                        'fields' => '',
                        'order' => '',
                        'recursive' => 1
                ),
                'Metum' => array(
                        'className' => 'Metum',
                        'foreignKey' => 'menu_metum_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'recursive' => 1
                ),
                'DisplayOption' => array(
                        'className' => 'DisplayOption',
                        'foreignKey' => 'display_option_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'recursive' => 1
                )
        );

        /**
         * hasMany associations
         *
         * @var array
         */
        public $hasMany = array(
                'ChildMenu' => array(
                        'className' => 'Menu',
                        'foreignKey' => 'parent_id',
                        'dependent' => false,
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'limit' => '',
                        'offset' => '',
                        'exclusive' => '',
                        'finderQuery' => '',
                        'counterQuery' => ''
                )
        );

        /**
         * Get Admin menus.
         *
         * Gets the admin menu in tree/nested arrays.
         *
         * @param type $active If active show menu at frontend else not.
         * @param type $type Three types of filter 'all' (default), 'visible', 'hidden'
         * @param type $level If false (default) all are returned, if level like 1,2,3 is given then only that levelitems is returned.
         * @param type $findType all the find types of model $Model->find('findType')
         * @return type
         */
        public function getMenuTreeForDisplay($active=null,$type = 'all', $level = false, $findType = 'threaded', $childrenOf = null) {

                switch ($type) {
                        case 'visible':
                                $visibility = array(1);
                                break;

                        case 'hidden':
                                $visibility = array(0);
                                break;

                        default:
                                $visibility = array(0, 1);
                                break;
                }

                if($level AND is_numeric($level) AND empty($childrenOf)) {
                        $parents = null;

                        for($i=1;$i<$level;$i++) {
                                $parents = $this->find('list', array(
                                        'conditions' => array(
                                                'Menu.parent_id' => $parents
                                        ),
                                        'fields' => array('Menu.id', 'Menu.id')
                                ));
                        }

                        if($parents === null) { $parents = 'first'; }
                }

                if($childrenOf){
                        $parents = $this->field('parent_id', array('Menu.id' => $childrenOf));
                        if(!$parents) $parents=$childrenOf;
                }

                if ($active) {
                    $options = array(
                        'contain' => array(),
                        'order' => 'Menu.lft',
                        'conditions' => array(
                                // 'Menu.is_visible' => $visibility
                                'Menu.is_visible' => 1,
                                'Menu.is_active' => 1
                        ));
                } else {
                    $options = array(
                        'contain' => array(),
                        'order' => 'Menu.lft',
                        'conditions' => array(
                                // 'Menu.is_visible' => $visibility
                                'Menu.is_visible' => 1
                        ));
                }

                if(isset($parents)){
                        $parents = ($parents == 'first')? null: $parents;
                        $options['conditions']['Menu.parent_id'] = $parents;
                }

                $menus = $this->find($findType, $options);
                return $menus;
        }

        /**
         * Finds the matching route for the url given.
         *
         * Only for Routing purpose, disable config option when calling.
         *
         * @param type $url
         * @param type $config Disable to false when calling this function
         * @return boolean
         */
        public function getMatchingRoute($url, $config=true) {

                if($url === '') $url = '/';
                // remove named parameters
                $urlArr = explode("/", $url);
                $namedConfig = Router::namedConfig();

                foreach ($urlArr as $k => $urlAr) {
                        if(strpos($urlAr, $namedConfig['separator'])){
                                unset($urlArr[$k]);
                                $namedFound[] = $urlAr;
                        }
                }

                $secondUrl = $urlArr;
                $lastPop = array_pop($secondUrl);
                array_push($secondUrl, '*');

                $url = implode("/", $urlArr);
                $url2 = implode("/", $secondUrl);

                $route = $this->find('first', array(
                        'conditions' => array(
                                'Menu.routing_link' => $url,
                                'Menu.is_active' => 1
                        ),
                        'recursive' => -1
                        ));

                if(!$route){
                        $route = $this->find('first', array(
                        'conditions' => array(
                                'Menu.routing_link' => $url2,
                                'Menu.is_active' => 1
                        ),
                        'recursive' => -1
                        ));
                }

                if ($route) {

                        if($route['Menu']['extra_params'] === '*'){
                                $route['Menu']['extra_params'] = $lastPop;
                                $route['Menu']['routing_link'] = str_replace("*", $lastPop, $route['Menu']['routing_link']);
                                $route['Menu']['from_link'] = str_replace("*", $lastPop, $route['Menu']['from_link']);
                                if($config) Configure::write('currentMenuDataSelect', $route['Menu']['id']);
                        }

                        // stores matched menu info, for to be used later
                        if($config) Configure::write('currentMenuData', $route);

                        $namedUrl = (!empty($namedFound)) ? "/".implode("/", $namedFound) : "";
                        return $route['Menu']['from_link'].$namedUrl;
                }

                return false;
        }

        public function getRoutingMatch($url) {

                // no url translation for admin
                if(!empty($url['admin'])){
                        return false;
                }

                if(isset($url['controller'])) $conditions['Menu.controller_name'] = $url['controller'];
                if(isset($url['action'])) $conditions['Menu.action_name'] = $url['action'];
                if(isset($url[0])) $conditions['Menu.extra_params'] = $url[0];
                if(isset($url[1])) $conditions['Menu.extra_params'] .= "/".$url[1];
                if(isset($url[2])) $conditions['Menu.extra_params'] .= "/".$url[2];

                $match = $this->find('first', array(
                        'conditions' => $conditions
                ));

                if($match){
                        unset($url['controller'], $url['action']);
                        $named = "";
                        foreach($url as $k => $val){
                                if($val && $k === "page") $named .= "/$k:$val";
                        }
                        $return = $match['Menu']['routing_link'].$named;
                        return $return;
                }

                return false;
        }

        public function convertUrlToRouterParams($url) {
                $params = Router::parse($url);
                return serialize($params);
        }


        public function checkRoutingLink($link, $extra=null) {
                return true;
                if($link['routing_link']){
                        return false;
                }

        }

}
