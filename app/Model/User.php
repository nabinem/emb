<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'username';

/**
 * hasMany associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'username' => array(
            'alphanumeric' => array(
                'rule' => array('alphanumeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email' => array(
            'isUnique' => array (
                'rule' => 'isUnique',
                'message' => 'This email address already exists in our database.'
                ),
            'valid' => array (
                'rule' => array('email', false),
                'message' => 'Invalid email.'
                )
        ),
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'status' => array(
            'inlist' => array(
                'rule' => array('inList', array('0','1','2')),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'new_password' => array(
            'minLength' => array(
                'rule' => array('minLength', 5),
                'message' => 'Password minimum length is 5',
                'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'new_password' => array(
                'rule' => array('checkPasswordMatch', null),
                'message' => 'New password is invalid or mismatch',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password' => array(
            'minLength' => array(
                'rule' => array('minLength', 5),
                'message' => 'Password minimum length is 5',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'time_zone' => array(
            'time_zone' => array(
                'rule' => array('checkTimeZone', null),
                'message' => 'Invalid Time Zone',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

    );


/**
 * User States.
 *
 * Defines the states of user in the system.
 */
        protected $_userStates = array(
                0 => 'Inactive',
                1 => 'Active',
                2 => 'Suspended'
        );


/**
 * beforeSave implementation for User.
 *
 * Hash the password before saving.
 */
        public function beforeSave($options = array()) {
                parent::beforeSave($options);

                if(isset($this->data[$this->alias]['password'])){
                        $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
                }
                return true;
        }

/**
 * Gets the available User states.
 */
        public function getUserStates(){
                return $this->_userStates;
        }
/**
 * Validate password match.
 */
        public function checkPasswordMatch($newPassword) {

                if(isset($this->data[$this->alias]['new_password']) AND isset($this->data[$this->alias]['retype_password'])){
                        if(!Validation::notEmpty($this->data[$this->alias]['new_password']) AND !Validation::notEmpty($this->data[$this->alias]['retype_password'])){
                                return true;
}
                        // continue
                }else if(!isset($this->data[$this->alias]['new_password']) AND !isset($this->data[$this->alias]['retype_password'])){
                        return true;
                }else{
                        return false;
                }

                $res = $newPassword['new_password'] === $this->data[$this->alias]['retype_password'];

                if($res === true){
                        $this->data[$this->alias]['password'] = $newPassword['new_password'];
                        return true;
                }
                return false;
        }


/**
 * Validate time zone.
 */
        public function checkTimeZone($timeZone) {

                if(timeZoneList($timeZone['time_zone']) === false){
                        return false;
                }
                return true;
        }

}


