<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    
    
    public $actsAs = array('Containable');
    
    
/**
 * Get data for editing tranlation field. 
 */
	public function translationEdit($id = null) {
		$this->locale = Configure::read('Setting.languageDefault');
		$result = $this->read(null, $id);
		
                
                if(!isset($this->actsAs['Translate'])){
                        return $result;
                }
		
		// load the i18n model.
                if (isset($this->translateModel)) {
                        $className = $this->translateModel;
                }else{
                        $className = 'I18nModel';
                }
		
		$I18nModel = ClassRegistry::init($className, 'Model');
		
		// search the translation for that field and then format if accordingly.
                $translations = $I18nModel->find('all', array('conditions' => array('model' => $this->alias, 'foreign_key' => $id)));

		foreach($translations as $translation) {
			
			if(!is_array($result[$this->alias][$translation[$I18nModel->alias]['field']])){
				$result[$this->alias][$translation[$I18nModel->alias]['field']] = array(
					$result[$this->alias][$translation[$I18nModel->alias]['field']]
				);
			}
			$result[$this->alias][$translation[$I18nModel->alias]['field']][$translation[$I18nModel->alias]['locale']] = $translation[$I18nModel->alias]['content'];
			
		}
		
		return $result;
		
	}
        
/**
 * Validation for the nesting level.
 * 
 * @return  boolean  true if valid
 */        
        public function checkNestingLimit($parentId, $limit = null) {
                
                if($limit === null){
                        $limit = $this->nestingLimit;
                }
                
                if(empty($limit) OR empty($parentId['parent_id'])){
                        return true;
                }
                
                $parentLevel = $this->getNestingLevel($parentId['parent_id']);
                
                if($parentLevel === false){
                        return false;
                }
                
                if($parentLevel < $limit-1){
                        return true;
                }
                return false;
        }
        

/**
 * Get the level of nesting for the given id.
 * 
 * Returns the nesting level. i.e. if root parent 0, false if not found.
 * Requires TreeBehavior to work.
 */
      
        public function getNestingLevel($id = '') {
                
                $path = $this->getPath($id);
                
                $count = count($path);
                
                if($count == 0){
                        return false;
                }
                
                return $count - 1;
        }
        
/**
 * Move up/down tree element.
 */        
        public function moveTreeElement($dir, $id, $places=1) {
                if($dir == 'up'){
                        return $this->moveUp(intval($id), $places);
                }else if($dir == 'down'){
                        return $this->moveDown(intval($id), $places);
                }
        }

/**
 * Gets the menu tree for the purpose of adding/editing.
 * 
 * The level of nesting is limited by limiting the choices.
 */
        public function getTreeList() {
                
                $menus = $this->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
                
                return $menus;
        }

/**
 * Override the default validation error stings.
 * 
 * @param type $field
 * @param type $value 
 */        
        public function invalidate($field, $value = true) {
                $value = ___($value);
                parent::invalidate($field, $value);
        }
        
/**
 * Before save for all the models.
 * 
 * Implements for the FileUpload plugin here, for deleting previous files.
 * 
 * @param type $options
 * @return type 
 */        
        public function beforeSave($options = array()) {
                
                // delete previous file before saving new file for FileUploader plugin
//                if(in_array($this->name, array('Sticker', 'QrCode', 'Category'))){
//                        $previousFile = $this->field('icon');
//                        if($previousFile && $this->data[$this->name]['icon'] != $previousFile) {
//                                $filename = 'files'.DS.$previousFile;
//                                file_exists($filename)? @unlink($filename) : null;
//                        }
//                }
                return parent::beforeSave($options);
        }  

       public function afterValidate() {
                parent::afterValidate();
                array_walk_recursive($this->validationErrors, array($this, 'translateValidationMessage'));
        }
        
        public function translateValidationMessage(&$lang, $key) {
                $lang = ___($lang);
        }      
    
}
