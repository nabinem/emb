<?php
App::uses('AppModel', 'Model');
/**
 * Metum Model
 *
 */
class Metum extends AppModel {

 public $actsAs = array(
        'MultiTranslate' => array(
            'meta_title',
            'meta_description',
            'meta_keywords'
        )
    );

// Use a different model (and table)
public $translateModel = 'MetumI18n';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		// 'name' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
	);
}
