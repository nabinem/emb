<?php
App::uses('AppModel', 'Model');
/**
 * TranslationString Model
 *
 */
class TranslationString extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

        public $translateModel = 'StaticI18n';

	public $actsAs = array(
            'Translate' => array(
                'value' => 'valueTranslationString'
            )
        );

	public $translateTable = 'static_i18ns';

        public $order = array(
            'name'
        );

        /**
         * Configuration key of cache.
         *
         * @var string
         */
        protected $_cacheConfig = 'translation';


        protected $_translationStrings = null;


        public $validate = array(
            'name' => array(
                'duplicate' => array(
                    'rule' => array('checkTextUnique'),
                    'message' => 'Duplicate Entry'
                ),
                'notempty' => array(
                    'rule' => array('notempty')
                )
            )
        );

        /**
 * Delets the cache after a successful add/edit operation.
 * @param type $created
 */
	public function afterSave($created) {

		parent::afterSave($created);

		$this->deleteCache();

	}

/**
 * Delets the cache after a successful delete operation.
 * @param type $created
 */
	public function afterDelete() {

		parent::afterDelete();

		$this->deleteCache();

	}


	/**
 * Gets all the strings in searchable format with cache.
 */
	public function getAllString() {

		if($this->_translationStrings){
			return $this->_translationStrings;
		}

		$allTranslations = Cache::read('staticTranslations', $this->_cacheConfig);

		if(!$allTranslations){
                        $this->locale = Configure::read('Setting.languageDefault');
			$allTranslations = $this->find('all', array('recursive' => 1));
			$allTranslations = $this->_formatTranslations($allTranslations);
            Cache::write('staticTranslations', $allTranslations, $this->_cacheConfig);

		}

		$this->_translationStrings = $allTranslations;

		return $this->_translationStrings;
	}

	protected function _formatTranslations($translations) {

		$formatted = array();
		foreach($translations as $k => $t) {
			foreach($t['valueTranslationString'] as $k1 => $t1) {
				$formatted[$t1['locale'].'#'.noLineBreak($t['TranslationString']['name'])] = $t1['content'];
				$formatted[$t1['locale'].'#'.  strtolower(noLineBreak($t['TranslationString']['name']))] = $t1['content'];
			}
		}
		return $formatted;
	}

/**
 * Enters the string into the tranlation system if not found.
 *
 * @param type $string
 * @return type
 */
	public function newEntry($string) {
		$data['name'] = $string;

                // important to set local
                $locale = Configure::read('Setting.languageDefault');
                if(empty($locale)){
                        throw new CakeException('Locale is empty for translate string');
                }
		$data['value'][$locale] = $string;
		$this->create();
		$res = $this->save($data);
		//$this->log($this->validationErrors);
		return $res;
	}


	public function deleteCache() {
		Cache::delete('staticTranslations', $this->_cacheConfig);
	}

    public function checkTextUnique($text) {
            // field is important do not remove
            return !(boolean)$this->find('first', array('conditions' => array('TranslationString.name' => $text['name']), 'fields' => 'TranslationString.name'));

    }

}
