<?php
App::uses('AppModel', 'Model');
/**
 * Banner Model
 *
 */
class Banner extends AppModel {
        
        
        public $actsAs = array(
                'Upload.Upload' => array(
                        'icon' => array(
                                'pathMethod'	=> 'flat',
                                'path'		=> '{ROOT}webroot{DS}files{DS}{model}{DS}',
                                'thumbnailMethod' => 'php',
                                'thumbnailSizes' => array(
                                'small' => '120l',
                                'medium' => '250l',
                                'big' => '350l'
                                ),
                                'mimetypes' => array(
                                        'image/bmp',
                                        'image/gif',
                                        'image/jpeg',
                                        'image/pjpeg',
                                        'image/png',
                                ),
                                'deleteOnUpdate' => true
                        )
                )
        );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'url' => array(
				'rule' => array('url'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
