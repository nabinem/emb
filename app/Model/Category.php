<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Category $ParentCategory
 * @property Metum $Metum
 * @property Category $ChildCategory
 * @property Product $Product
 */
class Category extends AppModel {

/**
 *  Acts as a tree.
 */
        public $actsAs = array(
                'Tree',
                'Upload.Upload' => array(
                        'icon' => array(
                                'pathMethod'    => 'flat',
                                'path'      => '{ROOT}webroot{DS}files{DS}{model}{DS}',
                                'thumbnailMethod' => 'php',
                                'thumbnailSizes' => array(
                                'small' => '155x115',
                                'medium' => '192x111',
                                'big' => '210x110'
                                ),
                                'mimetypes' => array(
                                        'image/bmp',
                                        'image/gif',
                                        'image/jpeg',
                                        'image/pjpeg',
                                        'image/png',
                                ),
                                'deleteOnUpdate' => true
                        ),
                        'image' => array(
                                'pathMethod'	=> 'flat',
                                'path'		=> '{ROOT}webroot{DS}files{DS}{model}{DS}',
                                'thumbnailMethod' => 'php',
                                'thumbnailSizes' => array(
                                'small' => '155x115',
                                'medium' => '192x111',
                                'big' => '210x110'
                                ),
                                'mimetypes' => array(
                                        'image/bmp',
                                        'image/gif',
                                        'image/jpeg',
                                        'image/pjpeg',
                                        'image/png',
                                ),
                                'deleteOnUpdate' => true
                        )
                ),
                'MultiTranslate' => array(
                    'name'
                )
        );

// Use a different model (and table)
    public $translateModel = 'CategoryI18n';

/**
 * Nesting limit.
 *
 * If the limit is 5 levels then there can be only 0 > 1 > 2 > 3 > 4 levels.
 *
 * @var integer
 */
        public $nestingLimit = 2;

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'category';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'slug' => array(
			'rule1' => array(
				'rule' => array('notempty'),
				'message' => 'Slug is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'rule2' => array(
				'rule' => array('isUnique'),
				'message' => 'Slug is not uniqie',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'rule3' => array(
				'rule'    => 'alphaNumericDashUnderscore',
            	'message' => 'Slug can only be letters, numbers, dash and underscore'
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'icon' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Image is required',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
        ),
        'image' => array(
	            'rule' => array('isFileUpload'),
	            'message' => 'Image is required',
	            //'allowEmpty' => true,
	            //'required' => false,
	            //'last' => false, // Stop validation after this rule
	            'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
		'parent_id' => array(
                        'nestingLimit' => array(
                                'rule' => array('checkNestingLimit', null),
                                'message' => 'Nesting Limit Reached. Please choose a higher level parent.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
                        )
                )
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ParentCategory' => array(
			'className' => 'Category',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

public function alphaNumericDashUnderscore($check) {
        // $data array is passed using the form field name as the key
        // have to extract the value to make the function generic
        $value = array_values($check);
        $value = $value[0];

        return preg_match('|^[0-9a-zA-Z_-]*$|', $value);
    }
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChildCategory' => array(
			'className' => 'Category',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
