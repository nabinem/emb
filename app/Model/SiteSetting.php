<?php
App::uses('AppModel', 'Model');
/**
 * SiteSetting Model
 *
 */
class SiteSetting extends AppModel {

    public $actsAs = array(
        'Upload.Upload' => array(
                'image' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',
                        'thumbnailMethod' => 'php',
                        'thumbnailSizes' => array(
                        'small' => '100x100'
                        ),
                        'mimetypes' => array(
                                'image/bmp',
                                'image/gif',
                                'image/jpeg',
                                'image/pjpeg',
                                'image/png',
                        ),
                        'deleteOnUpdate' => true
                ),
                'video_chrome' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',

                        'mimetypes' => array(
                                'video/mp4'
                        ),
                        'deleteOnUpdate' => true
                ),
                'video_ie' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',

                        'mimetypes' => array(
                                'video/webm'
                        ),
                        'deleteOnUpdate' => true
                ),
                'video_others' => array(
                        'pathMethod'    => 'flat',
                        'path'      => '{ROOT}webroot{DS}files{DS}video{DS}',

                        'mimetypes' => array(
                                'video/ogg'
                        ),
                        'deleteOnUpdate' => true
                )
        ),
);

        public $cacheName = 'SiteSettingCache';

        /**
 * Display field
 *
 * @var string
 */
	public $displayField = 'site_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'site_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'site_email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'activate_google_analytics' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


        public function getSiteSetting() {

                $cacheName = $this->cacheName;

                $setting = Cache::read($cacheName);
                if($setting){
                        $cacheUptodate = 1;
                }

                if(!$setting){
                        $setting = $this->find('first', array('order' => 'id ASC'));
                }

                if(!$setting){
                        $this->create();
                        $setting = $this->save();
                }

                if(empty($cacheUptodate)){
                        Cache::write($cacheName, $setting);
                }

                return $setting;
        }

        public function cacheDelete() {
                Cache::delete($this->cacheName);
        }


        public function afterSave($created) {
                $this->cacheDelete();
                parent::afterSave($created);
        }

        public function afterDelete() {
                $this->cacheDelete();
                parent::afterDelete();
        }
}
