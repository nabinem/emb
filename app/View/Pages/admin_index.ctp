
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="pages index pos">
	<?php echo $this->Session->flash(); ?>
	    <div id="page-heading">

        <?php if ($this->params['controller']=='system'): ?>
            <strong>Menus [List]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Menus [List]', array('controller' => 'menus')); ?>
        <?php endif ?>
        |
        <?php if ($this->params['controller']=='pages'): ?>
            <strong>Pages [List]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Pages [List]', array('controller' => 'pages')); ?>
        <?php endif ?>

    </div>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('title'); ?></th>
			<th class="actions imp_1"><?php echo ___('Edit'); ?></th>
		<th class="actions imp_1"><?php echo ___('Delete'); ?></th>
	</tr>
	<?php
	foreach ($pages as $page): ?>
	<?php if (!empty($page['Menu']['id'])): ?>
		<?php //continue; ?>
	<?php endif ?>
	<tr>
		<td class="imp_1"><?php echo h($page['Page']['id']); ?>&nbsp;</td>
        <td class="imp_1"><?php echo h($page['Page']['title']); ?>&nbsp;</td>
		<td class="actions imp_1"  style="text-align:center;">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $page['Page']['id']), array('class'=>"tt", 'data-original-title'=>"edit", 'escape' => false)); ?>
		</td>
		<td class="actions imp_1"  style="text-align:center;">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $page['Page']['id']), array('class'=>"tt", 'data-original-title'=>"delete", 'escape' => false), ___('Are you sure you want to delete # %s?', $page['Page']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => ___('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="pagination">
	<?php
		echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>