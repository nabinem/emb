<link href="<?php echo $this->Html->assetUrl('front-assets/css/flexslider.css'); ?>?t=2" rel="stylesheet" type="text/css">
<div class="content_pos item_pos detail_pos transp_bt">    
    <div class="item_detail" style="float: left">        
        <h2><?php echo $this->Cms->uImage($page['Page']['dimage1'], 'page'); ?></h2>
        <p><?php echo $page['Page']['detail_text_left']; ?></p>
    </div>
    <div class="static-block">
        <?php echo $this->Cms->uImage($page['Page']['dimage2'], 'page', 'small'); ?>
    </div>
    <div class="item_grid_cont">
    <div class="item_grid">
        <div class ="grid_i">
          <?php if(!empty($page_images)): ?>
            <div id="slider" class="flexslider">
            <ul class="slides">
              
               <?php foreach($page_images as $key=>$page_image):?>
               <li> 
              	<?php 
		             if(file_exists(  'files/PageImage/' . $page['Page']['id'].'/'. $page_image['PageImage']['image'])){
					 echo $this->Html->image('../files/PageImage/' . $page['Page']['id']. '/' .$page_image['PageImage']['image']);
		 }?>    </li>
		 <?php endforeach;?>
              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>
          <?php endif; ?>
        </div>
    </div>
    </div>    
</div>

<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">

	<div class="bottom_item">

                <?php if (!empty($page['Page']['embed'])): ?>
                    <a class="fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>"><img width="210" height="110" src="http://i1.ytimg.com/vi/<?= $this->Cms->getVideoId($page['Page']['embed']) ?>/3.jpg" alt=""></a>
                    <a class="link fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>">Instruction Film</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                            height = 600;

                            jQuery(".fancybox").fancybox({
                                helpers: {
                                    media: true
                                },
                                width: 16 / 9. * height,
                                height: height,
                                aspectRatio: true,
                                scrolling: 'no',
                                padding: 0,
                                margin: 0
                            });
                        });
                    </script>
                <?php else: ?>
                    <?php if (!empty($page['Page']['mimage1'])): ?>
                        <?php echo $this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'); ?>
                    <?php endif ?>
                    <a class="link" href="javascript:void(0)">Instruction link</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                        });
                    </script>
                <?php endif ?>

            </div>
            
    <div class="cell_big transp">
        <span class="bold_tag"><?php echo $page['Page']['detail_text_bottom']; ?></span>
    </div>
  <?php if ($isMobile && strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')): ?>

                    <span div="start" style="position: relative; z-index: 99999;">                      

                            <?php echo $this->Html->link($this->Cms->aImage('start.png'), 'javascript:void(0)', array('escape' => false, 'class' => 'link-video-start')); ?>

                        

                </span>

            <?php else: ?>

                <span class="start" style="position: relative;
    z-index: 99999;">            

                        <?php echo $this->Cms->aImage('start.png'); ?>                   

                </span>

            <?php endif; ?>
</div>
<div class="clear"></div>

<div class="clear"></div>
<script src="<?php echo $this->Html->assetUrl('front-assets/js/jquery.flexslider.js'); ?>"></script>

<script> 
	   jQuery(document).ready(function($){
		   var photo_height;
		   function calc_bluebar(){
			   
				photo_height = jQuery('.transp_bt').height();
				jQuery('.static-block').css({'height':photo_height*0.9});
				jQuery('.item_grid .grid_i').css({'height':photo_height, 'width':'auto'});
				jQuery('.item_grid_cont .flexslider .slides > li img').css({'height':photo_height*0.9, 'width':'auto'});
				//jQuery('.item_grid_cont .flexslider .slides').css({'height':photo_height*0.9});
		   }
		   calc_bluebar();
		   
		   jQuery(window).resize(function() {
            	calc_bluebar();
       		 });
		});
</script>

<?php echo $this->element('front/footer'); ?>