<div class="pin">
<div class="pin_details">
    <?php echo $page['Page']['pin1'] ?>
</div>
</div>
<div class="pin" style="left:400px;top:230px;">
<div class="pin_details">
    <?php echo $page['Page']['pin2'] ?>
</div>
</div>
<div class="pin" style="left:500px;top:400px;">
<div class="pin_details">
    <?php echo $page['Page']['pin3'] ?>
</div>
</div>
<div class="pin" style="right:300px;top:430px;left:auto">
<div class="pin_details">
    <?php echo $page['Page']['pin4'] ?>
</div>
</div>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/jquery.validate-rules.js'); ?>"></script>
<div class="contact" style="position: relative; z-index: 99;">
<div class="contact_inner">
<h1 class="contact_title">Contact us <?=$this->Cms->aImage('globe.png')?></h1>
<div class="contact_bg transp_bt"></div>
    <h2><?php echo $page['Page']['content_short']; ?></h2>
    <?php echo $this->Form->create('Page',array('class'=>'transp_bt','id'=>'contact_form')); ?>
        <div class="input_slot">
            <?php echo $this->Form->input('name',array('class'=>'ip_text  required','label'=>'Name *'));?>
        </div>
        <div class="input_slot">
            <?php echo $this->Form->input('email',array('class'=>'ip_text  required','label'=>'E-mail *'));?>
        </div>
         <div class="input_slot">
            <?php echo $this->Form->input('tel',array('class'=>'ip_text','label'=>'Phone number'));?>
        </div>
        <div class="input_slot">
            <?php echo $this->Form->input('subject',array('class'=>'ip_text  required','label'=>'Subject *'));?>
        </div>
        <div class="input_slot">
            <?php echo $this->Form->input('message',array('type'=>'textarea','class'=>'message required','label'=>'Your Message *'));?>
        </div>
        <div class="input_slot">
            <?php echo $this->Form->submit('SEND', array('id'=>'submitbutton','class'=>'submit','div'=>false)); ?>
        </div>
    <?php echo $this->Form->end();?>
</div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">
  <div class="cell_big transp"> <h3 class="address_title"><?php echo $page['Page']['title']; ?></h3>
<address>
<?php echo $page['Page']['content_long']; ?>
</address></div>
</div>
<div class="clear"></div>
<style type="text/css">
 input[type='submit'][disabled] {
  opacity: 0.5;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){
    function wr(){
        if(jQuery(window).width()/jQuery(window).height()<1.77){
        	jQuery('.bg_video_container').addClass('no_wide');
        }
        else{
        	jQuery('.bg_video_container').removeClass('no_wide');
        }
    }

    jQuery(window).resize(function(){ wr(); });
    wr();



    jQuery('.pin').click(function(event){

        if(!jQuery(this).find('.pin_details').is('.visible')){
        	jQuery(this).find('.pin_details').fadeIn(200);
        	jQuery(this).find('.pin_details').addClass('visible')
        }
        else if(jQuery(this).find('.pin_details').is('.visible')){
        	jQuery(this).find('.pin_details').fadeOut(200);
        	jQuery(this).find('.pin_details').removeClass('visible')
        }
        event.preventDefault();
    })
});
</script>
<?php echo $this->element('front/footer'); ?>
</div>
</div>