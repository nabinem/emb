<?php
$selected_tab = '';
if ($this->name == 'Pages' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Page (Edit)</span></a></li>\n";
        $this->end();
}
$trans['titleTranslation'][0]['content']=(!empty($trans['titleTranslation'][0]['content'])) ? $trans['titleTranslation'][0]['content']: '';
$trans['titleTranslation'][1]['content']=(!empty($trans['titleTranslation'][1]['content'])) ? $trans['titleTranslation'][1]['content']: '';
$trans['titleTranslation'][2]['content']=(!empty($trans['titleTranslation'][2]['content'])) ? $trans['titleTranslation'][2]['content']: '';

$trans['bodyTranslation'][0]['content']=(!empty($trans['bodyTranslation'][0]['content'])) ? $trans['bodyTranslation'][0]['content']: '';
$trans['bodyTranslation'][1]['content']=(!empty($trans['bodyTranslation'][1]['content'])) ? $trans['bodyTranslation'][1]['content']: '';
$trans['bodyTranslation'][2]['content']=(!empty($trans['bodyTranslation'][2]['content'])) ? $trans['bodyTranslation'][2]['content']: '';

$trans['shortTranslation'][0]['content']=(!empty($trans['shortTranslation'][0]['content'])) ? $trans['shortTranslation'][0]['content']: '';
$trans['shortTranslation'][1]['content']=(!empty($trans['shortTranslation'][1]['content'])) ? $trans['shortTranslation'][1]['content']: '';
$trans['shortTranslation'][2]['content']=(!empty($trans['shortTranslation'][2]['content'])) ? $trans['shortTranslation'][2]['content']: '';

$type_area=($this->request->data['Page']['id']==1) ? 'textarea': 'hidden';
$lblShortContent=($this->request->data['Page']['id']==1) ? 'One-Liners': 'Banner Text';
$txtlink=($this->request->data['Page']['id']==1) ? 'Start Link': 'Parts Link';
$isChild=(!empty($this->request->data['Menu']['parent_id'])) ? true: false;
$isFooter=(!empty($this->request->data['Menu']['parent_id']) && $this->request->data['Menu']['parent_id']==28) ? true: false;
if ($isFooter) {
  $type_area='textarea';
  $isContact=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']=='/contact123') ? true: false;
  $isContent=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']!='/contact123') ? true: false;
  if (!$isContact) {
   $type_area='hidden';
  }
}

?>

<div class="pages form pos">
<?php echo $this->Session->flash(); ?>
<div id="page-heading">

        <?php if ($this->params['action']=='admin_edit'): ?>
            <strong>Page [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Page [Edit]', array('controller' => 'pages','action'=>'edit',$this->request->data['Page']['id'])); ?>
        <?php endif ?>
        |
        <?php if ($this->params['action']=='admin_overview'): ?>
            <strong>Overview [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Overview [Edit]', array('controller' => 'pages','action'=>'overview',$this->request->data['Page']['id'])); ?>
        <?php endif ?>

    </div><br>
<?php echo $this->Form->create('Page', array('type'=>'file','action' => 'admin_overview', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
          echo $this->Form->input('id');

          echo $this->Form->hidden('Page.title.eng', array(
                  'value'=>$trans['titleTranslation'][0]['content'],
                   'label' => array('text'=>'Name','class' => 'control-label')
                  ));

           echo $this->Form->hidden('Page.content_short.eng',array('value'=>$trans['shortTranslation'][0]['content'],'class'=>'longtxtarea','type' => $type_area,'label' => array('text'=>$lblShortContent,'class' => 'control-label')));

            echo $this->Form->hidden('Page.content_long.eng',array('value'=>$trans['bodyTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Text','class' => 'control-label'),'class'=>'ckeditor'));

           echo $this->Form->input('detail_text_left',array('class'=>'ckeditor'));
           echo $this->Form->input('detail_text_bottom',array('class'=>'ckeditor'));

             //dutch
            echo $this->Form->hidden('Page.title.dut', array(
                'value'=>$trans['titleTranslation'][1]['content'],
                'label' => array('text'=>'Name (Dutch)','class' => 'control-label')
                ));
             echo $this->Form->hidden('Page.content_short.dut',array('value'=>$trans['shortTranslation'][1]['content'],'type' => $type_area,'label' => array('text'=>'Short Content (Dutch)','class' => 'control-label')));
             echo $this->Form->hidden('Page.content_long.dut',array('value'=>$trans['bodyTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Content (Dutch)','class' => 'control-label')));

             //thai
            echo $this->Form->hidden('Page.title.tha', array(
                'value'=>$trans['titleTranslation'][2]['content'],
                'label' => array('text'=>'Name (Thai)','class' => 'control-label')
                ));
             echo $this->Form->hidden('Page.content_short.tha',array('value'=>$trans['shortTranslation'][2]['content'],'type' => $type_area,'label' => array('text'=>'Short Content (Thai)','class' => 'control-label')));
             echo $this->Form->hidden('Page.content_long.tha',array('value'=>$trans['bodyTranslation'][2]['content'],'type' => 'textarea','label' => array('text'=>'Content (Thai)','class' => 'control-label')));

             //overview max 4 images
             if(isset($trans['Page']['dimage1'])){
                 $after = $this->Cms->uImage($trans['Page']['dimage1'], 'page', 'small');
             }
             else{
                 $after = '';
             }
              echo $this->Form->input('Page.dimage1', array(
              'type' => 'file',
              'class' => 'input-large',
              'label' => array('text'=>'Top Image','class' => 'control-label'),
              'after' => $after.'</div>'
              ));     
              if(isset($trans['Page']['dimage1'])){
                 $after = $this->Cms->uImage($trans['Page']['dimage2'], 'page', 'small');
             }
             else{
                 $after = '';
             }          
             echo $this->Form->input('Page.dimage2', array(
                  'type' => 'file',
                  'class' => 'input-large',
                  'label' => array('text'=>'Middle Image','class' => 'control-label'),
                  'after' => $after.'</div>'
                  )); 
             
            echo $this->Form->input('image.',array('label'=>'Carousel','type'=>'file','multiple'));
           

             echo $this->Form->input('dir', array('type' => 'hidden'));

             echo $this->Form->input('metum_id', array('type' => 'hidden'));
        ?>
         <?php $page = $this->request->data;
		?>
        <div class="edit_ad_images">
         <ul >
               <?php if(!empty($page['PageImage'])): foreach ($page['PageImage'] as $key => $page_image) : ?>
               <li> 
              	<?php                    
                        if (file_exists(
                                'files/PageImage/' . $page['Page']['id'] . '/thumb/thumb_'
                                        . $page_image['image'])) {
                            echo $this->Html->image(
                                   '../'.'files/PageImage/' . $page['Page']['id']
                                            . '/thumb/thumb_' . $page_image['image'],
                                    array(
                                            'width' => '90',
                                            'height' => '90',
                                            'alt' => "thumb",
                                            'title' => "thumb"
                                    ));
                        }
				  ?> 
 				<a class="delete_image_link" rel="<?php echo $page_image['id']?>">Delete</a>	
 				</li>
 				
 <?php endforeach; endif; ?>
            </ul>
            </div>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<?php
// add forms for tabs, need exact order as tabs.
// class "pos" div is needed
if(!$this->request->is('ajax')) {
        echo $this->element('../Meta/admin_edit');
}

?>
<style type="text/css">
  .dvopt{
    margin-left: 116px;
  }  
  .edit_ad_images{
      display:block;
      max-height: 204px;
        overflow-y: scroll;
        padding: 10px 22px;
        margin-bottom: 10px;
  }
  .edit_ad_images ul{
      display:block;
      list-style: none;
  }
  .edit_ad_images ul li{
     float: left;
    margin-right: 16px;
    margin-bottom: 4px;
    position: relative;  
    
  }
  .delete_image_link{
      display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        color: #FFF !important;
        cursor: pointer;
        background: #FF0000;
        padding: 2px;
        z-index: 999;
        font-size:10px;
        line-height:12px;
  }
</style>
<script>
    jQuery(document).ready(function(){
        jQuery('.delete_image_link').click(function(){
        var currentdom = jQuery(this);
           jQuery.ajax({
               type: "POST",
               url: PCMS.webroot+'admin/Pages/delete_image',  //file name
               data: "id="+ jQuery(this).attr('rel'),  //data
               dataType: "json",
               success: function(data){ 
                   if(data.status)//if user name does not exist return value "0"
                   {
                       currentdom.parent().remove();
                   }
                   else 
                   {
                       alert('We experience some technical problem. Please try again.');
                   }

               }

            });
       });
    });
</script>
