<div class="content_pos_outer">
<div class="content_pos item_pos transp_bt">
    <h2>Search result</h2>
    <div class="item_detail content_detail">
        <?php
         foreach ($search as $skey => $list):
            if (!empty($list['Menu']['parent_id'])) {
                if ($list['Menu']['parent_id']==28) {
                    $plink=$list['Menu']['routing_link'];
                }else{
                    $parentCode=$this->Cms->getParent($list['Menu']['id']);
                    $plink=$parentCode.$list['Menu']['routing_link'];
                }
            }else{
            $plink=$list['Menu']['routing_link'];
            }
         ?>
            <h3>
            <?php echo $this->Html->link($list['Page']['title'], $plink,array('escape'=>false,'class'=>'link')); ?>
            </h3>
        <?php endforeach ?>
    </div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">
    <div class="transp">

    </div>
</div>
<div class="clear"></div>
<?php echo $this->element('front/footer'); ?>
</div>
</div>