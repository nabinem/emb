<div class="content_pos_outer"  id="content_pos_top">
<div  style="padding-left:16px;"  class="content_pos item_pos transp_bt">
    <div class="item_detail content_detail">
	    <h2><?php echo $page['Page']['title']; ?></h2>
        <p><?php echo $page['Page']['content_long']; ?></p>
		<div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">
    <div class="transp">

    </div>
</div>
<div class="clear"></div>
<?php echo $this->element('front/footer'); ?>
</div>
</div>