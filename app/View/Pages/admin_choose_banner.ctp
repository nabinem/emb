
<?php

$selected_tab = '';
if ($this->name == 'Pages' AND $this->view == 'admin_choose_banner' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Page Banner</span></a></li>\n";
        $this->end();
}

?>

<div class="pages form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Page', array('action' => 'admin_choose_banner', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
	<?php
                echo $this->Form->input('id');
		echo $this->Form->input('banner_id', array('empty' => ___('No Banner')));
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>