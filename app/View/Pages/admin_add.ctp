
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="pages form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Page', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
        echo $this->Form->input('Page.title.eng',array('label' => array('text'=>'Title','class' => 'control-label')));
        echo $this->Form->input('Page.content_short.eng',array('class'=>'ckeditor','type' => 'hidden','label' => array('text'=>'Short Content','class' => 'control-label')));
        echo $this->Form->input('Page.content_long.eng',array('class'=>'ckeditor','type' => 'textarea','label' => array('text'=>'Content','class' => 'control-label')));

        echo $this->Form->hidden('Page.title.dut',array('label' => array('text'=>'Title (Dutch)','class' => 'control-label')));
        echo $this->Form->input('Page.content_short.dut',array('class'=>'ckeditor','type' => 'hidden','label' => array('text'=>'Short Content (Dutch)','class' => 'control-label')));
        echo $this->Form->hidden('Page.content_long.dut',array('class'=>'ckeditor','type' => 'textarea','label' => array('text'=>'Content (Dutch)','class' => 'control-label')));

        echo $this->Form->hidden('Page.title.tha',array('label' => array('text'=>'Title (Thai)','class' => 'control-label')));
        echo $this->Form->input('Page.content_short.tha',array('class'=>'ckeditor','type' => 'hidden','label' => array('text'=>'Short Content (Thai)','class' => 'control-label')));
        echo $this->Form->hidden('Page.content_long.tha',array('class'=>'ckeditor','type' => 'textarea','label' => array('text'=>'Content (Thai)','class' => 'control-label')));


	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
