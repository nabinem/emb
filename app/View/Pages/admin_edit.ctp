<?php
$selected_tab = '';
if ($this->name == 'Pages' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Page (Edit)</span></a></li>\n";
        $this->end();
}
$trans['titleTranslation'][0]['content']=(!empty($trans['titleTranslation'][0]['content'])) ? $trans['titleTranslation'][0]['content']: '';
$trans['titleTranslation'][1]['content']=(!empty($trans['titleTranslation'][1]['content'])) ? $trans['titleTranslation'][1]['content']: '';
$trans['titleTranslation'][2]['content']=(!empty($trans['titleTranslation'][2]['content'])) ? $trans['titleTranslation'][2]['content']: '';

$trans['bodyTranslation'][0]['content']=(!empty($trans['bodyTranslation'][0]['content'])) ? $trans['bodyTranslation'][0]['content']: '';
$trans['bodyTranslation'][1]['content']=(!empty($trans['bodyTranslation'][1]['content'])) ? $trans['bodyTranslation'][1]['content']: '';
$trans['bodyTranslation'][2]['content']=(!empty($trans['bodyTranslation'][2]['content'])) ? $trans['bodyTranslation'][2]['content']: '';

$trans['shortTranslation'][0]['content']=(!empty($trans['shortTranslation'][0]['content'])) ? $trans['shortTranslation'][0]['content']: '';
$trans['shortTranslation'][1]['content']=(!empty($trans['shortTranslation'][1]['content'])) ? $trans['shortTranslation'][1]['content']: '';
$trans['shortTranslation'][2]['content']=(!empty($trans['shortTranslation'][2]['content'])) ? $trans['shortTranslation'][2]['content']: '';

$type_area=($this->request->data['Page']['id']==1) ? 'textarea': 'hidden';
$lblShortContent=($this->request->data['Page']['id']==1) ? 'One-Liners': 'Banner Text';
$txtlink=($this->request->data['Page']['id']==1) ? 'Start Link': 'Parts Link';
//$isChild=(!empty($this->request->data['Menu']['parent_id'])) ? true: false;
$isFooter=(!empty($this->request->data['Menu']['parent_id']) && $this->request->data['Menu']['parent_id']==28) ? true: false;
if ($isFooter) {
  $type_area='textarea';
  $isContact=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']=='/contact123') ? true: false;
  $isContent=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']!='/contact123') ? true: false;
//  if (!$isContact) {
   $type_area='hidden';
//  }
}

?>

<div class="pages form pos">
<?php echo $this->Session->flash(); ?>
<?php if (!$isFooter): //if ($isChild && !$isFooter): ?>
<div id="page-heading">

        <?php if ($this->params['action']=='admin_edit'): ?>
            <strong>Page [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Page [Edit]', array('controller' => 'pages','action'=>'edit',$this->request->data['Page']['id'])); ?>
        <?php endif ?>
        |
        <?php if ($this->params['action']=='admin_overview'): ?>
            <strong>Overview [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Overview [Edit]', array('controller' => 'pages','action'=>'overview',$this->request->data['Page']['id'])); ?>
        <?php endif ?>

    </div><br>

<?php endif ?>
<?php if ($isFooter && $isContact): ?>
<div id="page-heading">

        <?php if ($this->params['action']=='admin_edit'): ?>
            <strong>Page [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Page [Edit]', array('controller' => 'pages','action'=>'edit',$this->request->data['Page']['id'])); ?>
        <?php endif ?>
        |
        <?php if ($this->params['action']=='admin_points'): ?>
            <strong>Points [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Points [Edit]', array('controller' => 'pages','action'=>'points',$this->request->data['Page']['id'])); ?>
        <?php endif ?>

    </div><br>

<?php endif ?>
<?php if ($this->request->data['Page']['id']==30): ?>
<div id="page-heading">

        <?php if ($this->params['action']=='admin_edit'): ?>
            <strong>Page [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Page [Edit]', array('controller' => 'pages','action'=>'edit',$this->request->data['Page']['id'])); ?>
        <?php endif ?>
        |
        <?php if ($this->params['action']=='admin_overview'): ?>
            <strong>Overview [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Overview [Edit]', array('controller' => 'pages','action'=>'overview',$this->request->data['Page']['id'])); ?>
        <?php endif ?>

    </div><br>


<?php endif ?>
<?php echo $this->Form->create('Page', array('type'=>'file','action' => 'admin_edit', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
            echo $this->Form->input('id');

            echo $this->Form->input('Page.title.eng', array(
                    'value'=>$trans['titleTranslation'][0]['content'],
                     'label' => array('text'=>'Name','class' => 'control-label')
                    ));

             echo $this->Form->input('Page.content_short.eng',array('value'=>$trans['shortTranslation'][0]['content'],'class'=>'longtxtarea','type' => $type_area,'label' => array('text'=>$lblShortContent,'class' => 'control-label')));


             if (isset($isContent) && $isContent) {
              if ($this->request->data['Page']['id']!=30) {
               echo $this->Form->input('Page.content_long.eng',array('value'=>$trans['bodyTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Text','class' => 'control-label'),'class'=>'ckeditor'));
              }else{
               echo $this->Form->hidden('Page.content_long.eng',array('value'=>$trans['bodyTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Text','class' => 'control-label'),'class'=>'ckeditor'));
              }
             }else{
              echo $this->Form->input('Page.content_long.eng',array('value'=>$trans['bodyTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Text','class' => 'control-label'),'class'=>'ckeditor'));

             }

            echo $this->Form->hidden('Page.title.dut', array(
                'value'=>$trans['titleTranslation'][1]['content'],
                'label' => array('text'=>'Name (Dutch)','class' => 'control-label')
                ));
             echo $this->Form->hidden('Page.content_short.dut',array('value'=>$trans['shortTranslation'][1]['content'],'type' => $type_area,'label' => array('text'=>'Short Content (Dutch)','class' => 'control-label')));
             echo $this->Form->hidden('Page.content_long.dut',array('value'=>$trans['bodyTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Content (Dutch)','class' => 'control-label')));

            echo $this->Form->hidden('Page.title.tha', array(
                'value'=>$trans['titleTranslation'][2]['content'],
                'label' => array('text'=>'Name (Thai)','class' => 'control-label')
                ));
             echo $this->Form->hidden('Page.content_short.tha',array('value'=>$trans['shortTranslation'][2]['content'],'type' => $type_area,'label' => array('text'=>'Short Content (Thai)','class' => 'control-label')));
             echo $this->Form->hidden('Page.content_long.tha',array('value'=>$trans['bodyTranslation'][2]['content'],'type' => 'textarea','label' => array('text'=>'Content (Thai)','class' => 'control-label')));

             if ($this->request->data['Page']['id']==1) {   //homepage
               echo $this->Form->input('link',array('label'=>array('text'=>$txtlink,'class' => 'control-label')));

                 echo $this->Form->input('Page.himage1', array(
                      'type' => 'file',
                      'class' => 'input-large',
                      'label' => array('text'=>'Image1','class' => 'control-label'),
                      'after' => $this->Html->link(
						$this->Cms->uImage($trans['Page']['himage1'], 'page','small',array('class'=>'fancybox')).'</div>'
   ,'/files/page/small_'.$trans['Page']['himage1'], 
 array('escape' => false, 'class'=>'fancybox'))
					  
                      )); 
//					  $this->Cms->uImage($trans['Page']['himage1'], 'page', 'small').'</div>'
                 echo $this->Form->input('Page.himage2', array(
                      'type' => 'file',
                      'class' => 'input-large',
                      'label' => array('text'=>'Image2','class' => 'control-label'),
                      'after' => $this->Html->link(
						$this->Cms->uImage($trans['Page']['himage2'], 'page','small',array('class'=>'fancybox')).'</div>'
   ,'/files/page/small_'.$trans['Page']['himage2'], 
 array('escape' => false, 'class'=>'fancybox'))
					  
					 
                    ));
// $this->Cms->uImage($trans['Page']['himage2'], 'page', 'small').'</div>'
             }else{
                if (!$isFooter) { //$isChild &&                   

                     echo $this->Form->input('Page.dimage', array(
                      'type' => 'file',
                      'class' => 'input-large',
                      'label' => array('text'=>'Image','class' => 'control-label'),
                      'after' => $this->Cms->uImage($trans['Page']['dimage'], 'page', 'small').'</div>'
                      ));

                } else {
                  if ($isFooter) {
                    if (isset($isContent) && !$isContent) {
                       echo $this->Form->input('Page.mimage1', array(
                        'type' => 'file',
                        'class' => 'input-large',
                        'label' => array('text'=>'Image1','class' => 'control-label'),
                        'after' => $this->Cms->uImage($trans['Page']['mimage1'], 'page', 'small').'</div>'
                        ));
                    }
                  }else{
                  //echo $this->Form->input('embed',array('label'=>array('text'=>'Embed Link','class' => 'control-label')));
                  }
                }

             }
             
             echo $this->Form->input('embed',array('label'=>array('text'=>'Embed Link','class' => 'control-label')));
             
             echo $this->Form->input('Page.video_chrome', array(
                          'type' => 'file',
                          'class' => 'input-large',
                          'label' => array('text'=>'Video MP4','class' => 'control-label'),
                          'after' => $trans['Page']['video_chrome']
                          ));
       
        echo $this->Form->input('Page.video_others', array(
                          'type' => 'file',
                          'class' => 'input-large',
                          'label' => array('text'=>'Video OGV','class' => 'control-label'),
                          'after' => $trans['Page']['video_others']
                          ));
						  
			  
		 echo $this->Form->input('Page.image', array(
                        'type' => 'file',
                        'class' => 'input-large',
                        'label' => array('text'=>'Site Background','class' => 'control-label'),
                        'after' => $this->Html->link(
						$this->Cms->uImage($trans['Page']['image'], 'video','small',array('class'=>'fancybox')).'</div>'
   ,'/files/video/'.$trans['Page']['image'], // or an array('controller' => 'mycontroller', 'action' => 'myaction')
 array('escape' => false, 'class'=>'fancybox'))
                        ));			    
		
             
             
             echo $this->Form->input('dir', array('type' => 'hidden'));

             echo $this->Form->input('metum_id', array('type' => 'hidden'));
        ?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<?php
// add forms for tabs, need exact order as tabs.
// class "pos" div is needed
if(!$this->request->is('ajax')) {
        echo $this->element('../Meta/admin_edit');
}

?>
<link href="<?php echo $this->webroot.'/source/jquery.fancybox.css'; ?>?t=4" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $this->webroot.'/source/jquery.fancybox.js'; ?>"></script>
<script type="text/javascript">
jQuery(document).ready(function() { jQuery("a.fancybox").fancybox(); });
</script>