<section class="content intro">
	<h1 class="font_l b_tag" id="lcontainer">
	<?php $liners = explode(',', $page['Page']['content_short']); ?>

            <?php
            foreach ($liners as $l => $liner):

                if ($l == 6) {

                    continue;
                }
                ?>

                <div class="display" id="div<?= $l ?>"><?= trim($liners[$l]) ?></div>

            <?php endforeach ?>
    </h1>
    
  </section>
  <aside class="bottom clearfix">
    <div class="box box_small floatL">
     <?php if (!empty($page['Page']['embed'])): ?>
                    <a class="fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>"><img width="210" height="110" src="http://i1.ytimg.com/vi/<?= $this->Cms->getVideoId($page['Page']['embed']) ?>/3.jpg" alt=""></a>
                    <a class="link fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>">Instruction Film</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                            height = 600;

                            jQuery(".fancybox").fancybox({
                                helpers: {
                                    media: true
                                },
                                width: 16 / 9. * height,
                                height: height,
                                aspectRatio: true,
                                scrolling: 'no',
                                padding: 0,
                                margin: 0
                            });
                        });
                    </script>
                <?php else: ?>
                    <?php if (!empty($page['Page']['mimage1'])): ?>
                        <?php echo $this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'); ?>
                    <?php endif ?>
                    <a class="link" href="javascript:void(0)">Instruction link</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                        });
                    </script>
                <?php endif ?>
    
    </div>
    <div class="box box_med  floatR"><span class="font_m"><?php echo $page['Page']['content_long']?></span></div>
  </aside>
  
  <style type="text/css">

    .b_tag{

        min-height: 276px;

        position: relative;;

    }

    .b_tag .display{

        position: absolute;

    }

    .js .display { display:none; }

</style>

<script type="text/javascript">

    $('html').addClass('js');



    $(function() {



        initialize_fancy();

        styleTargets();



        //var numItems = $('.display').length;

        var numItems = 6;

        var stopper = 6;



        var timer = setInterval(showDiv, 1000);



        var counter = 0;



        function showDiv() {



            $('div', '#lcontainer')

                    .stop()

                    .filter(function() {
                return this.id.match('div' + counter);
            })

                    .show('fast');

            counter == numItems ? counter = stopper : counter++;

        }

    });



    function styleTargets()

    {
        $(".display").each(function(index)

        {

            if (index < 6) {

                var tpos = index * 41;

                var tpos = tpos + 'px';

                var tdiv = '.b_tag #div' + index;

                $(tdiv).css({'top': tpos});

            }

        });

    }



    function initialize_fancy() {

        height = 600;

        $(".fancybox").fancybox({
            helpers: {
                media: true

            },
            width: 16 / 9. * height,
            height: height,
            aspectRatio: true,
            scrolling: 'no',
            padding: 0,
            margin: 0

        });

    }

</script>