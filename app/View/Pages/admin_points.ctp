<?php
$selected_tab = '';
if ($this->name == 'Pages' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Page (Edit)</span></a></li>\n";
        $this->end();
}
$trans['titleTranslation'][0]['content']=(!empty($trans['titleTranslation'][0]['content'])) ? $trans['titleTranslation'][0]['content']: '';
$trans['titleTranslation'][1]['content']=(!empty($trans['titleTranslation'][1]['content'])) ? $trans['titleTranslation'][1]['content']: '';
$trans['titleTranslation'][2]['content']=(!empty($trans['titleTranslation'][2]['content'])) ? $trans['titleTranslation'][2]['content']: '';

$trans['bodyTranslation'][0]['content']=(!empty($trans['bodyTranslation'][0]['content'])) ? $trans['bodyTranslation'][0]['content']: '';
$trans['bodyTranslation'][1]['content']=(!empty($trans['bodyTranslation'][1]['content'])) ? $trans['bodyTranslation'][1]['content']: '';
$trans['bodyTranslation'][2]['content']=(!empty($trans['bodyTranslation'][2]['content'])) ? $trans['bodyTranslation'][2]['content']: '';

$trans['shortTranslation'][0]['content']=(!empty($trans['shortTranslation'][0]['content'])) ? $trans['shortTranslation'][0]['content']: '';
$trans['shortTranslation'][1]['content']=(!empty($trans['shortTranslation'][1]['content'])) ? $trans['shortTranslation'][1]['content']: '';
$trans['shortTranslation'][2]['content']=(!empty($trans['shortTranslation'][2]['content'])) ? $trans['shortTranslation'][2]['content']: '';

$type_area=($this->request->data['Page']['id']==1) ? 'textarea': 'hidden';
$lblShortContent=($this->request->data['Page']['id']==1) ? 'One-Liners': 'Banner Text';
$txtlink=($this->request->data['Page']['id']==1) ? 'Start Link': 'Parts Link';
$isChild=(!empty($this->request->data['Menu']['parent_id'])) ? true: false;
$isFooter=(!empty($this->request->data['Menu']['parent_id']) && $this->request->data['Menu']['parent_id']==28) ? true: false;
if ($isFooter) {
  $type_area='textarea';
  $isContact=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']=='/contact123') ? true: false;
  $isContent=(!empty($this->request->data['Menu']['routing_link']) && $this->request->data['Menu']['routing_link']!='/contact123') ? true: false;
  if (!$isContact) {
   $type_area='hidden';
  }
}

?>

<div class="pages form pos">
<?php echo $this->Session->flash(); ?>
<div id="page-heading">

        <?php if ($this->params['action']=='admin_edit'): ?>
            <strong>Page [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Page [Edit]', array('controller' => 'pages','action'=>'edit',$this->request->data['Page']['id'])); ?>
        <?php endif ?>
        |
        <?php if ($this->params['action']=='admin_points'): ?>
            <strong>Points [Edit]</strong>
        <?php else: ?>
            <?php echo $this->Html->link('Points [Edit]', array('controller' => 'pages','action'=>'points',$this->request->data['Page']['id'])); ?>
        <?php endif ?>

    </div><br>

<?php echo $this->Form->create('Page', array('type'=>'file','action' => 'admin_overview', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
          echo $this->Form->input('id');

          echo $this->Form->hidden('Page.title.eng', array(
                  'value'=>$trans['titleTranslation'][0]['content'],
                   'label' => array('text'=>'Name','class' => 'control-label')
                  ));

           echo $this->Form->hidden('Page.content_short.eng',array('value'=>$trans['shortTranslation'][0]['content'],'class'=>'longtxtarea','type' => $type_area,'label' => array('text'=>$lblShortContent,'class' => 'control-label')));

            echo $this->Form->hidden('Page.content_long.eng',array('value'=>$trans['bodyTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Text','class' => 'control-label'),'class'=>'ckeditor'));

           echo $this->Form->input('pin1',array('class'=>'ckeditor'));
           echo $this->Form->input('pin2',array('class'=>'ckeditor'));
           echo $this->Form->input('pin3',array('class'=>'ckeditor'));
           echo $this->Form->input('pin4',array('class'=>'ckeditor'));

             //dutch
            echo $this->Form->hidden('Page.title.dut', array(
                'value'=>$trans['titleTranslation'][1]['content'],
                'label' => array('text'=>'Name (Dutch)','class' => 'control-label')
                ));
             echo $this->Form->hidden('Page.content_short.dut',array('value'=>$trans['shortTranslation'][1]['content'],'type' => $type_area,'label' => array('text'=>'Short Content (Dutch)','class' => 'control-label')));
             echo $this->Form->hidden('Page.content_long.dut',array('value'=>$trans['bodyTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Content (Dutch)','class' => 'control-label')));

             echo $this->Form->input('metum_id', array('type' => 'hidden'));
        ?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<?php
// add forms for tabs, need exact order as tabs.
// class "pos" div is needed
if(!$this->request->is('ajax')) {
        echo $this->element('../Meta/admin_edit');
}

?>
