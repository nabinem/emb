<div class="pages view">
	<dl>
		<dt><?php echo ___('Id'); ?></dt>
		<dd>
			<?php echo h($page['Page']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Title'); ?></dt>
		<dd>
			<?php echo h($page['Page']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Slug'); ?></dt>
		<dd>
			<?php echo h($page['Page']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Content Short'); ?></dt>
		<dd>
			<?php echo h($page['Page']['content_short']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Content Long'); ?></dt>
		<dd>
			<?php echo h($page['Page']['content_long']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Created'); ?></dt>
		<dd>
			<?php echo h($page['Page']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Modified'); ?></dt>
		<dd>
			<?php echo h($page['Page']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo ___('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(___('Edit Page'), array('action' => 'edit', $page['Page']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(___('Delete Page'), array('action' => 'delete', $page['Page']['id']), null, ___('Are you sure you want to delete # %s?', $page['Page']['id'])); ?> </li>
		<li><?php echo $this->Html->link(___('List Pages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(___('New Page'), array('action' => 'add')); ?> </li>
	</ul>
</div>
