<div class="content_pos_inner" id="content_pos_bot">
<div class="content_pos item_pos overview_pos transp_bt">
    <?php foreach ($allChildren as $skey => $svalue):
    $child=$this->Cms->getPageContent($svalue['Menu']['extra_params']);
    $appendCls=($skey>2) ? 'switch': '';
    
    $partslink=$this->webroot.$page['Page']['routing_link'].$child['Page']['routing_link'];
    
    if ($skey==6) {
        continue;
    }
     ?>
     <div class="cell_item_cont">
      <div class="cell_item <?=$appendCls?>">
        <div class="foto">
          <?php if (!empty($child['Page']['dimage'])): ?>
                 <?php echo $this->Html->link($this->Cms->uImage($child['Page']['dimage'], 'page', 'small'), $partslink,array('escape'=>false,'class'=>'blink','id'=>$svalue['Menu']['extra_params'])); ?>
            <?php endif ?>
        </div>
        <div class="detail blink" id="<?=$svalue['Menu']['extra_params']?>" style="cursor:pointer">
            <p><?php echo $child[0]['Page__i18n_content_long']; ?></a></p>
            <span class="link"><?php echo $child[0]['Page__i18n_title']; ?></span>
            
        </div>
      </div>
    </div>
    <?php endforeach ?>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">
<div class="bottom_item">

    <?php if (!empty($page['Page']['embed'])):
        $videoEmbed = $page['Page']['embed'];
        $doc = new DOMDocument();
        $doc->loadHTML($videoEmbed);

        $src = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');
         ?>
        <?php if (!empty($page['Page']['mimage1'])): ?>
             <?php echo $this->Html->link($this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'), $src,array('escape'=>false,'class'=>'fancybox')); ?>
        <?php endif ?>
        <a class="link fancybox" href="<?=$src?>">instruction link</a>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                if(jQuery('div').hasClass('switch')){
                	jQuery('body >.page').addClass('page_grid');
                        }else{
                        	jQuery('body >.page').removeClass('page_grid');
                        }

                height = 600;

                jQuery(".fancybox").fancybox({
                    helpers : {
                        media: true
                    },
                    width       : 16/9. * height,
                    height      : height,
                    aspectRatio : true,
                    scrolling   : 'no',
                    padding:0,
                    margin:0
                });
            });
        </script>
    <?php else: ?>
    <?php if (!empty($page['Page']['mimage1'])): ?>
            <?php echo $this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'); ?>
    <?php endif ?>
    <a class="link" href="javascript:void(0)">video</a>
    <script type="text/javascript">
            jQuery(document).ready(function($) {
                if(jQuery('div').hasClass('switch')){
                	jQuery('body >.page').addClass('page_grid');
                }else{
                	jQuery('body >.page').removeClass('page_grid');
                }
            });
        </script>
    <?php endif ?>

</div>
<div class="cell_big cell_ban transp">
    <span class="bold_tag">
            <?php echo $page['Page']['content_long']; ?>
    </span>
</div>
</div>
<div class="clear"></div>
<?php echo $this->element('front/footer'); ?>
</div>
</div>