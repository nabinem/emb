
<?php
    $this->extend('/Common/admin/afterlogin');
    
    $this->end();
?>

<div class="banners form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo __('Edit Banner'); ?></h2>
<?php echo $this->Form->create('Banner', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('icon');
		echo $this->Form->input('url');
		echo $this->Form->input('overlay_text');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
