
<?php

        $this->extend('/Common/admin/afterlogin');
        $this->append('more_tab');
        echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>";
        $this->end();
        
?>

<div class="banners form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo __('Admin Edit Banner'); ?></h2>
<?php echo $this->Form->create('Banner', array('type' => 'file', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('icon', array('type' => 'file', 'after' => $this->Cms->uImage($this->request->data['Banner']['icon'], 'category', 'small').'</div>'));
		echo $this->Form->input('url');
		echo $this->Form->input('overlay_text');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
