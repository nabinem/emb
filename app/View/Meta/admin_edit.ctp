
<?php

$selected_tab = '';
if ($this->name == 'Meta' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Meta (Edit)</span></a></li>\n";
        $this->end();
}
for ($i=0; $i <=2; $i++) {

$metrans['mtTranslation'][$i]['content']=(!empty($metrans['mtTranslation'][$i]['content'])) ? $metrans['mtTranslation'][$i]['content']: '';
$metrans['mdTranslation'][$i]['content']=(!empty($metrans['mdTranslation'][$i]['content'])) ? $metrans['mdTranslation'][$i]['content']: '';
$metrans['mkTranslation'][$i]['content']=(!empty($metrans['mkTranslation'][$i]['content'])) ? $metrans['mkTranslation'][$i]['content']: '';
}

?>
<div class="meta form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Metum', array('action' => 'admin_edit', 'controller' => 'Meta', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		 echo $this->Form->input('id');

		 echo $this->Form->input('Metum.meta_title.eng',array('value'=>$metrans['mtTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Meta Title','class' => 'control-label')));
         echo $this->Form->input('Metum.meta_description.eng',array('value'=>$metrans['mdTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Meta Description','class' => 'control-label')));
         echo $this->Form->input('Metum.meta_keywords.eng',array('value'=>$metrans['mkTranslation'][0]['content'],'type' => 'textarea','label' => array('text'=>'Meta Keywords','class' => 'control-label')));

         echo $this->Form->hidden('Metum.meta_title.dut',array('value'=>$metrans['mtTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Meta Title (Dutch)','class' => 'control-label')));
         echo $this->Form->hidden('Metum.meta_description.dut',array('value'=>$metrans['mdTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Meta Description (Dutch)','class' => 'control-label')));
         echo $this->Form->hidden('Metum.meta_keywords.dut',array('value'=>$metrans['mkTranslation'][1]['content'],'type' => 'textarea','label' => array('text'=>'Meta Keywords (Dutch)','class' => 'control-label')));

         echo $this->Form->hidden('Metum.meta_title.tha',array('value'=>$metrans['mtTranslation'][2]['content'],'type' => 'textarea','label' => array('text'=>'Meta Title (Thai)','class' => 'control-label')));
         echo $this->Form->hidden('Metum.meta_description.tha',array('value'=>$metrans['mdTranslation'][2]['content'],'type' => 'textarea','label' => array('text'=>'Meta Description (Thai)','class' => 'control-label')));
         echo $this->Form->hidden('Metum.meta_keywords.tha',array('value'=>$metrans['mkTranslation'][2]['content'],'type' => 'textarea','label' => array('text'=>'Meta Keywords (Thai)','class' => 'control-label')));


	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
