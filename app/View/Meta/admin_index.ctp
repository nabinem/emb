
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="meta index pos">
	<?php echo $this->Session->flash(); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('meta_title'); ?></th>
			<th class=""><?php echo $this->Paginator->sort('meta_description'); ?></th>
			<th class=""><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
			<th class="actions imp_1"><?php echo ___('Edit'); ?></th>
		<th class="actions imp_1"><?php echo ___('Delete'); ?></th>
	</tr>
	<?php
	foreach ($meta as $metum): ?>
	<tr>
		<td  class="imp_1"><?php echo h($metum['Metum']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($metum['Metum']['meta_title']); ?>&nbsp;</td>
		<td  class=""><?php echo h($metum['Metum']['meta_description']); ?>&nbsp;</td>
		<td  class=""><?php echo h($metum['Metum']['meta_keywords']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $metum['Metum']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $metum['Metum']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), ___('Are you sure you want to delete # %s?', $metum['Metum']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
//	echo $this->Paginator->counter(array(
//	'format' => ___('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	));
	?>	</p>

	<div class="pagination">
	<?php
		echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>