
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="meta form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Metum', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('meta_title', array('after' => '<p class="help-block">'.___('If empty contents title/name will be used').'</p></div>'));
		echo $this->Form->input('meta_description');
		echo $this->Form->input('meta_keywords');
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
