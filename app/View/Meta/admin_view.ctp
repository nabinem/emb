<div class="meta view">
	<dl>
		<dt><?php echo ___('Id'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Name'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Description'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Meta Title'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['meta_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Meta Description'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['meta_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Meta Keywords'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['meta_keywords']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Meta Robots'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['meta_robots']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Created'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Modified'); ?></dt>
		<dd>
			<?php echo h($metum['Metum']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo ___('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(___('Edit Metum'), array('action' => 'edit', $metum['Metum']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(___('Delete Metum'), array('action' => 'delete', $metum['Metum']['id']), null, ___('Are you sure you want to delete # %s?', $metum['Metum']['id'])); ?> </li>
		<li><?php echo $this->Html->link(___('List Meta'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(___('New Metum'), array('action' => 'add')); ?> </li>
	</ul>
</div>
