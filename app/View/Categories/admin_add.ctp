
<?php
    $this->extend('/Common/admin/afterlogin');

    $this->end();
?>

<div class="categories form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Category', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'),'type'=>'file')); ?>
	<fieldset>

	<?php
		echo $this->Form->input('Category.name.eng',array('label' => array('text'=>'Title','class' => 'control-label')));
		echo $this->Form->hidden('Category.name.dut',array('label' => array('text'=>'Title','class' => 'control-label')));
		echo $this->Form->hidden('Category.name.tha',array('label' => array('text'=>'Title','class' => 'control-label')));
		echo $this->Form->input('slug');
		echo $this->Form->input('icon', array('type' => 'file', 'class' => 'input-large'));
		echo $this->Form->input('parent_id', array('escape' => false, 'empty' => ___('No Parent')));
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
