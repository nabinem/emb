
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="categories index pos">
	<div id="page-heading">

       	<?php echo $this->Html->link('All', array('controller' => 'categories', 'action' => 'index', '')); ?>
       	   |
		<?php echo $this->Html->link('Category', array('controller' => 'categories', 'action' => 'index', 'byCategory')); ?>

    </div>
	<?php echo $this->Session->flash(); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('description'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('icon'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions imp_1"><?php echo __('Edit'); ?></th>
		<th class="actions imp_1"><?php echo __('Delete'); ?></th>
	</tr>
	<?php
	foreach ($categories as $category): ?>
	<tr>
		<td  class="imp_1"><?php echo h($category['Category']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($category['Category']['name']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($category['Category']['description']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo $this->Cms->uImage($category['Category']['icon'], 'category', 'small'); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($category['Category']['modified']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $category['Category']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $category['Category']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="pagination">
	<?php
		// echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		// echo $this->Paginator->numbers(array('separator' => ''));
		// echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>