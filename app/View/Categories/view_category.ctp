<div id="content" class="clearfix">
      <div class="one_third floatL">
        <?php echo $this->element('front/main_category'); ?>
        <?php echo $this->element('front/sale'); ?>
      </div>
    <div class="two_third floatR">
            <div class="box">
                    <h2><?php echo $category['Category']['name']; ?></h2>
                    <a class="category_banner"> <img src="<?php echo $this->webroot; ?>files/category/<?php echo $category['Category']['icon']; ?>" width="660" height="340" alt="Glod plated" /> </a>
                    <div class="clearfix">
                    <?php $sub=$this->Cms->getSubCategory($category['Category']['id']); ?>
                    <?php foreach($sub as $subcategory): ?> 
                        <div class="grid_3x_slot">
                                <div class="prod_img"><img src="<?php echo $this->webroot; ?>files/category/<?php echo $subcategory['Category']['icon']; ?>" width="200" alt="product" /></div>
                                <div class="prod_desc">
                                        <h3><a href="#"><?php echo $subcategory['Category']['name']; ?></a></h3>
                                        <p class="intro_desc"><?php echo $subcategory['Category']['description']; ?></p>
                                        <p class="more_link"><a href="#">[ + more]</a></p>
                                </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
            </div>
    </div>
</div>