<div id="content" class="clearfix">
	  <div class="one_third floatL">
	    <?php echo $this->element('front/main_category'); ?>
	    <?php echo $this->element('front/sale'); ?>
	  </div>
	<div class="two_third floatR">
		<div class="box">
			<h2><?php echo $category['Category']['name']; ?></h2>
			<?php echo $this->Html->link($this->Cms->uImage($category['Category']['icon'], 'category', 'big'), '/categories/'.$category['Category']['slug'] ,array('escape'=>false,'class'=>'category_banner')); ?>
			<div class="clearfix">
			<?php if (!empty($products)):
			$i=0;
			?>
			<?php foreach ($products as $product):
			$hlink='/products/'.$product['Product']['slug'];
			?>
				<div class="grid_3x_slot <?php if(($i%3)==0) echo "clearfix";?>">
					<div class="prod_img">
						<?php echo $this->Html->link($this->Cms->uImage($product['Product']['image'], 'product', 'semi_medium'), $hlink,array('escape'=>false)); ?>
					</div>
					<div class="prod_desc">
						<h3>
							<?php echo $this->Html->link(substr($product['Product']['name'],0,19), $hlink); ?>
						</h3>
						<?php if (!empty($product['Product']['short_description'])): ?>
							<p class="intro_desc">
								<?php echo substr($product['Product']['short_description'],0,180); ?>
							</p>
							<p class="more_link">
								<?php echo $this->Html->link('[ + more]', $hlink); ?>
							</p>
						<?php endif ?>
					</div>
				</div>
			<?php
			$i++;
			endforeach ?>
			<?php endif ?>
			</div>
		</div>
	</div>
</div>
