<div id="content" class="clearfix">
  <div class="one_third floatL">
    <?php echo $this->element('front/main_category'); ?>
    <?php echo $this->element('front/sale'); ?>    
  </div>
    <div class="two_third floatR">
        <?php foreach ($categories as $category): ?>
        <div class="box">
            <h2>
                <?php echo $this->Html->link($category['Category']['name'],'/categories/'.$category['Category']['slug']); ?>
            </h2>  
            <?php echo $this->Html->link($this->Cms->uImage($category['Category']['icon'], 'category', 'big'), '/categories/'.$category['Category']['slug'] ,array('escape'=>false,'class'=>'category_banner')); ?> 
        </div>
        <?php endforeach ?>       

    </div>
</div>