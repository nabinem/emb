<div id="content" class="clearfix">
    <div class="one_third floatL">
        <?php echo $this->element('front/main_category'); ?>
        <?php echo $this->element('front/sale'); ?>        
    </div>
    <div class="two_third floatR">
            <div class="box">
                    <h2><?php echo $category['Category']['name']; ?></h2>
                    <div class="clearfix">
                    <?php foreach($pro as $product): ?>
                        <div class="product_grid_slot clearfix">
                                <div class="prod_img"><img src="<?php echo $this->webroot; ?>files/product/<?php echo $product['Product']['image']; ?>" width="150" alt="product" /></div>
                                <div class="prod_desc">
                                                        <h5><a href="#"><?php echo $product['Product']['name']; ?></a></h5>
                                                        <div class="price"><?php echo '&pound;'.$product['Product']['price']; ?></div>
                                </div>
                        </div>
                    <?php endforeach; ?>
                    </div>

                    <div class="paginnation clearfix">
                            <ul>
                                   
                                    <li class="off" id="page_prev">&laquo; Prev</li>                                    
                                    <li><?php echo $this->Paginator->numbers(); ?></li>                                    
                                    <li id="page_next"><a href="gallery2.php">Next &raquo;</a></li>
                            </ul>
                    </div>
            </div>



    </div>
</div>