
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>
";
    $this->end();
?>

<div class="categories form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Category', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'), 'type' => 'file')); ?>
	<fieldset>

	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Category.name.eng', array(
                    'value'=>$trans['titleTranslation'][0]['content'],
                     'label' => array('text'=>'Name (English)','class' => 'control-label')
                    ));
        echo $this->Form->hidden('Category.name.dut', array(
                    'value'=>$trans['titleTranslation'][1]['content'],
                     'label' => array('text'=>'Name (Dutch)','class' => 'control-label')
                    ));
        echo $this->Form->hidden('Category.name.tha', array(
                    'value'=>$trans['titleTranslation'][2]['content'],
                     'label' => array('text'=>'Name (Thai)','class' => 'control-label')
                    ));

		echo $this->Form->input('slug');
		echo $this->Form->input('icon', array('type' => 'hidden'));
		echo $this->Form->input('icon', array('type' => 'file', 'class' => 'input-large', 'after' => $this->Cms->uImage($this->request->data['Category']['icon'], 'category', 'small').'</div>'));
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
