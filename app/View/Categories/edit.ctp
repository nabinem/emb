
<?php
    $this->extend('/Common/admin/afterlogin');
    
    $this->end();
?>

<div class="categories form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo __('Edit Category'); ?></h2>
<?php echo $this->Form->create('Category', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('content');
		echo $this->Form->input('icon');
		echo $this->Form->input('dir');
		echo $this->Form->input('parent_id');
		echo $this->Form->input('lft');
		echo $this->Form->input('rght');
		echo $this->Form->input('metum_id');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
