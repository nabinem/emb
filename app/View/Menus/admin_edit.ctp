
<?php
$selected_tab = '';
if ($this->name == 'Menus' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}
if(!$this->request->is('ajax')) {
    $this->append('more_tab');
    echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Menu (Edit)</span></a></li>\n";
    $this->end();
}
?>

<div class="menus form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Menu', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('Menu.name.eng', array('value'=>$mtrans['nameTranslation'][0]['content'], 'label' => array('text'=>'Name','class' => 'control-label')));
        echo $this->Form->hidden('Menu.name.dut', array('value'=>$mtrans['nameTranslation'][1]['content'], 'label' => array('text'=>'Name (Dutch)','class' => 'control-label')));
        echo $this->Form->hidden('Menu.name.tha', array('value'=>$mtrans['nameTranslation'][2]['content'], 'label' => array('text'=>'Name (Thai)','class' => 'control-label')));
		echo $this->Form->input('routing_link', array('class' => 'input-xlarge'));
		echo $this->Form->input('is_visible',array('label'=>array('text'=>'Sidebar Visible','class'=>'control-label')));
		echo $this->Form->input('is_active');
		echo $this->Form->input('parent_id', array('escape' => false, 'empty' => ___('No Parent')));
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
<?php echo $this->Form->postLink(___('Delete'), array('action' => 'delete', $this->request->data['Menu']['id']), array('class'=>'tt btn-large btn btn-danger', 'data-original-title'=>'delete', 'escape' => false), ___('Are you sure you want to delete # %s?', $this->request->data['Menu']['id'])); ?>
</div>

<?php

if(!empty($this->request->data['Page']['id'])){
        // echo $this->element('../Pages/admin_edit');
}

?>
