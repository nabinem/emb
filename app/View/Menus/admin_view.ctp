<div class="menus view">
	<dl>
		<dt><?php echo ___('Id'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Name'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Routing Link'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['routing_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Is Visible'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['is_visible']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Is Active'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Parent Id'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['parent_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Lft'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['lft']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Rght'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['rght']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Controller Name'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['controller_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Action Name'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['action_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Extra Params'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['extra_params']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Created'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Modified'); ?></dt>
		<dd>
			<?php echo h($menu['Menu']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo ___('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(___('Edit Menu'), array('action' => 'edit', $menu['Menu']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(___('Delete Menu'), array('action' => 'delete', $menu['Menu']['id']), null, ___('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?> </li>
		<li><?php echo $this->Html->link(___('List Menus'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(___('New Menu'), array('action' => 'add')); ?> </li>
	</ul>
</div>
