
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="menus form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Menu', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('Menu.name.eng',array('label' => array('text'=>'Name','class' => 'control-label','type'=>'hidden')));
		 echo $this->Form->hidden('Menu.name.dut',array('label' => array('text'=>'Name (Dutch)','class' => 'control-label')));
		  echo $this->Form->hidden('Menu.name.tha',array('label' => array('text'=>'Name (Thai)','class' => 'control-label')));

		echo $this->Form->input('from_link', array('class' => 'input-large js-from-links', 'empty' => 'Select'));
		echo $this->Form->input('routing_link', array('class' => 'input-xlarge'));
		echo $this->Form->input('is_visible',array('label'=>array('text'=>'Sidebar Visible','class'=>'control-label')));
		echo $this->Form->input('is_active');
		echo $this->Form->input('parent_id', array('escape' => false, 'empty' => ___('No Parent')));
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
