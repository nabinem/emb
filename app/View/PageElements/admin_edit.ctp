
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
    echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>\n";
    $this->end();
?>

<div class="pageElements form pos">
<?php echo $this->Session->flash(); ?>
<h2><?php echo __('Admin Edit Page Element'); ?></h2>
<?php echo $this->Form->create('PageElement', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('type');
		echo $this->Form->input('description');
		echo $this->Form->input('content', array('class' => 'ckeditor'));
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
