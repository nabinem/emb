<div class="pageElements view">
<h2><?php  echo __('Page Element'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($pageElement['PageElement']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Page Element'), array('action' => 'edit', $pageElement['PageElement']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Page Element'), array('action' => 'delete', $pageElement['PageElement']['id']), null, __('Are you sure you want to delete # %s?', $pageElement['PageElement']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Page Elements'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Page Element'), array('action' => 'add')); ?> </li>
	</ul>
</div>
