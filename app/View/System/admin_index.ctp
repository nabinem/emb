
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="menus index pos">
    <?php echo $this->Session->flash(); ?>

        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
    <tr>
            <th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
            <th class="imp_1"><?php echo $this->Paginator->sort('name'); ?></th>
            <th class="imp_1"><?php echo $this->Paginator->sort('parent_id'); ?></th>
            <th class="imp_1"><?php echo $this->Paginator->sort('is_active'); ?></th>
            <th class="actions imp_1"><?php echo ___('Edit Menu'); ?></th>
            <th class="actions imp_1"><?php echo ___('Edit Page'); ?></th>
        <th class="actions imp_1"><?php echo ___('Delete'); ?></th>
    </tr>
    <?php
    foreach ($menus as $menu): ?>
    <tr>
        <td  class="imp_1"><?php echo h($menu['Menu']['id']); ?>&nbsp;</td>
        <td  class="imp_1"><?php echo h($menu['Menu']['name']); ?>&nbsp;</td>
        <td  class="imp_1"><?php echo h($menu['ParentMenu']['name']); ?>&nbsp;</td>
        <td class="actions imp_1">
            <?php if ($menu['Menu']['is_active']): ?>
                <?php echo $this->Html->link('<i class="icon-eye-open"></i>',"javascript:void(0)", array('class'=>'tt', 'data-original-title'=>'Visible', 'escape' => false)); ?>
            <?php else: ?>
            <?php echo $this->Html->link('<i class="icon-eye-close"></i>',"javascript:void(0)", array('class'=>'tt', 'data-original-title'=>'Hidden', 'escape' => false)); ?>
            <?php endif ?>
        </td>
        <td class="actions imp_1">
            <?php echo $this->Html->link('<i class="icon-edit"></i>', array('controller'=>'menus','action' => 'edit', $menu['Menu']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
        </td>
        <td class="actions imp_1">
            <?php echo $this->Html->link('<i class="icon-edit"></i>', array('controller'=>'pages','action' => 'edit', $menu['Page']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
        </td>
        <td class="actions imp_1">
            <?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('controller'=>'menus','action' => 'delete', $menu['Menu']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), ___('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?>
        </td>
    </tr>
<?php endforeach; ?>
    </table>
    <p>
    <?php
//  echo $this->Paginator->counter(array(
//  'format' => ___('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//  ));
    ?>  </p>

    <div class="pagination">
    <?php
        echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
    </div>
</div>
