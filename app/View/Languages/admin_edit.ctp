
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>
";
    $this->end();
?>

<div class="languages form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo __('Admin Edit Language'); ?></h2>
<?php echo $this->Form->create('Language', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('code');
		echo $this->Form->input('name');
		echo $this->Form->input('native');
		echo $this->Form->input('direction');
		echo $this->Form->input('icon');
		echo $this->Form->input('status');
		echo $this->Form->input('ordering');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
