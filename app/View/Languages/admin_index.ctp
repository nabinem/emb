
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="languages index pos">
	<h2><?php echo __('Languages'); ?></h2>
	<?php echo $this->Session->flash(); ?> 
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('code'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('native'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('direction'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('icon'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('ordering'); ?></th>
			<th class="actions imp_1"><?php echo __('Edit'); ?></th>
		<th class="actions imp_1"><?php echo __('Delete'); ?></th>
	</tr>
	<?php
	foreach ($languages as $language): ?>
	<tr>
		<td  class="imp_1"><?php echo h($language['Language']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['code']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['name']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['native']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['direction']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['icon']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['status']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($language['Language']['ordering']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $language['Language']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $language['Language']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), __('Are you sure you want to delete # %s?', $language['Language']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="pagination">
	<?php
//		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//		echo $this->Paginator->numbers(array('separator' => ''));
//		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>