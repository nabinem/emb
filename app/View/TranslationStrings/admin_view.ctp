<div class="translationStrings view">
<h2><?php  echo ___('Translation String'); ?></h2>
	<dl>
		<dt><?php echo ___('Id'); ?></dt>
		<dd>
			<?php echo h($translationString['TranslationString']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Name'); ?></dt>
		<dd>
			<?php echo h($translationString['TranslationString']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Value'); ?></dt>
		<dd>
			<?php echo h($translationString['TranslationString']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Modified'); ?></dt>
		<dd>
			<?php echo h($translationString['TranslationString']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo ___('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(___('Edit Translation String'), array('action' => 'edit', $translationString['TranslationString']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(___('Delete Translation String'), array('action' => 'delete', $translationString['TranslationString']['id']), null, ___('Are you sure you want to delete # %s?', $translationString['TranslationString']['id'])); ?> </li>
		<li><?php echo $this->Html->link(___('List Translation Strings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(___('New Translation String'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(___('List Static I18ns'), array('controller' => 'static_i18ns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(___('New Value Translation String'), array('controller' => 'static_i18ns', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo ___('Related Static I18ns'); ?></h3>
	<?php if (!empty($translationString['valueTranslationString'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo ___('Id'); ?></th>
		<th><?php echo ___('Locale'); ?></th>
		<th><?php echo ___('Model'); ?></th>
		<th><?php echo ___('Foreign Key'); ?></th>
		<th><?php echo ___('Field'); ?></th>
		<th><?php echo ___('Content'); ?></th>
		<th class="actions"><?php echo ___('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($translationString['valueTranslationString'] as $valueTranslationString): ?>
		<tr>
			<td><?php echo $valueTranslationString['id']; ?></td>
			<td><?php echo $valueTranslationString['locale']; ?></td>
			<td><?php echo $valueTranslationString['model']; ?></td>
			<td><?php echo $valueTranslationString['foreign_key']; ?></td>
			<td><?php echo $valueTranslationString['field']; ?></td>
			<td><?php echo $valueTranslationString['content']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(___('View'), array('controller' => 'static_i18ns', 'action' => 'view', $valueTranslationString['id'])); ?>
				<?php echo $this->Html->link(___('Edit'), array('controller' => 'static_i18ns', 'action' => 'edit', $valueTranslationString['id'])); ?>
				<?php echo $this->Form->postLink(___('Delete'), array('controller' => 'static_i18ns', 'action' => 'delete', $valueTranslationString['id']), null, ___('Are you sure you want to delete # %s?', $valueTranslationString['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(___('New Value Translation String'), array('controller' => 'static_i18ns', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
