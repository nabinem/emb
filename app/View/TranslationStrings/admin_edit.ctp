
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
    echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>\n";
    $this->end();
?>

<div class="translationStrings form pos">
<?php echo $this->Session->flash(); ?>
<h2><?php echo ___('Admin Edit Translation String'); ?></h2>
<?php echo $this->Form->create('TranslationString', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('disabled' => 'disabled'));
		echo $this->Cms->form('value', array('type' => 'textarea', 'label' => array('class' => 'control-label')), 'input');
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
