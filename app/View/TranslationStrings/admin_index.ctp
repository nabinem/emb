
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="translationStrings index pos">
	<h2><?php echo ___('Translation Strings'); ?></h2>
	<?php echo $this->Session->flash(); ?> 
        <?php echo $this->element('admin/search_bar', array('placeholder' => '...Name')); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('value'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions imp_1"><?php echo ___('Edit'); ?></th>
		<th class="actions imp_1"><?php echo ___('Delete'); ?></th>
	</tr>
	<?php
	foreach ($translationStrings as $translationString): ?>
	<tr>
		<td  class="imp_1"><?php echo h($translationString['TranslationString']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($translationString['TranslationString']['name']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($translationString['TranslationString']['value']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($translationString['TranslationString']['modified']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $translationString['TranslationString']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $translationString['TranslationString']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), ___('Are you sure you want to delete # %s?', $translationString['TranslationString']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
//	echo $this->Paginator->counter(array(
//	'format' => ___('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	));
	?>	</p>

	<div class="pagination">
	<?php
		echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>