
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="translationStrings form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo ___('Admin Add Translation String'); ?></h2>
<?php echo $this->Form->create('TranslationString', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('name');
		echo $this->Cms->form('value', array('type' => 'textarea', 'label' => array('class' => 'control-label')), 'input');
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
