<?php
    $this->extend('/Common/admin/main');
    $this->end();
?>
<?php echo $this->element('admin/sidebar'); ?>
<section class="menu_cont">
    <?php echo $this->element('admin/menu_tab'); ?>
    <?php echo $this->element('admin/content_tab'); ?>
    <section class="app_stage">
        <?php echo $this->fetch('content'); ?>
    </section>
</section>