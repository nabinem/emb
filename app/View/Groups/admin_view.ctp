<div class="groups view details">
<h2><?php  echo ___('Group');?></h2>
<?php echo $this->Html->link(___('Edit Group'), array('action' => 'edit', $group['Group']['id']), array('class' => 'floatR btn_black')); ?>
<?php echo $this->Html->link(___('List Groups'), array('action' => 'index'), array('class' => 'floatR btn_black')); ?>
<?php echo $this->Html->link(___('New Group'), array('action' => 'add'), array('class' => 'floatR btn_black')); ?>
	<dl>
		<dt><?php echo ___('Id'); ?></dt>
		<dd>
			<?php echo h($group['Group']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Name'); ?></dt>
		<dd>
			<?php echo h($group['Group']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Description'); ?></dt>
		<dd>
			<?php echo h($group['Group']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Created'); ?></dt>
		<dd>
			<?php echo h($group['Group']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo ___('Modified'); ?></dt>
		<dd>
			<?php echo h($group['Group']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>


