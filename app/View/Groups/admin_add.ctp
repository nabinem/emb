<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<?php
    $defaults = array(
        'between'   => '<div class="controls">',
        'after'     => '</div>',
        'div'       => array('class' => 'control-group'),
        'label'     => array('class' => 'control-label')
    );
?>
<div class="groups form pos">
<h2><?php echo ___('Admin Add Group'); ?></h2>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Group', array('inputDefaults' => $defaults, 'class' => 'form-horizontal'));?>
	<fieldset>
		
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
