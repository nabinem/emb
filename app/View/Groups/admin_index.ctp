<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="groups index pos">
    <h2>Add Groups</h2>
        <table  border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-striped" width="100%">
          <tr>
            <th class="imp_1"><?php echo $this->Paginator->sort('id');?></th>
            <th class="imp_1"><?php echo $this->Paginator->sort('name');?></th>
            <th><?php echo $this->Paginator->sort('description');?></th>
            <th><?php echo $this->Paginator->sort('created');?></th>
            <th class="imp_2"><?php echo $this->Paginator->sort('modified');?></th>
            <th class="imp_1" style="text-align:center;">Edit  </th>
            <th class="imp_1" style="text-align:center;">Delete</th>
          </tr>
          <?php foreach($groups as $group){ ?><tr>
            <td class="imp_1"><?php echo h($group['Group']['id']); ?>&nbsp;</td>
            <td class="imp_1"><?php echo h($group['Group']['name']); ?>&nbsp;</td>
            <td><?php echo h($group['Group']['description']); ?>&nbsp;</td>
            <td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
            <td class="imp_2"><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
            <td class="imp_1" style="text-align:center;"><a href="<?php echo $this->Html->url(array('action' => 'edit', $group['Group']['id'])); ?>" class="tt" data-original-title="edit"><i class="icon-edit"></i></a></td>
            <td class="imp_1" style="text-align:center;"><?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $group['Group']['id']), array('class' => 'tt', 'escape' => false), ___('Are you sure you want to delete # %s?', $group['Group']['id'])); ?></td>
          </tr> <?php } ?>
        </table>
    <?php
//	echo $this->Paginator->counter(array(
//	'format' => ___('Page {:page} of {:pages}, showing ({:start} - {:end}) / {:count} records')
//	));
	?>	</p>

	<div class="pagination">
        <?php
		echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
        
<?php /* ?>    
    <h2><?php echo ___('Groups');?></h2>
        <?php echo $this->Html->link(___('+ New Group'), array('action' => 'add'), array('class' => 'floatR btn_black')); ?>	<table cellpadding="0" cellspacing="0"  width="100%" class="styled_table">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo ___('Actions');?></th>
	</tr>
	<?php
	foreach ($groups as $group): ?>
	<tr>
		<td><?php echo h($group['Group']['id']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['description']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
		<td class="actions textC">
			<?php echo $this->Html->link(___(''), array('action' => 'view', $group['Group']['id']), array('class' => 'view_ico hover_tool', 'rel' => 'view')); ?>
			<?php echo $this->Html->link(___(''), array('action' => 'edit', $group['Group']['id']), array('class' => 'edit_ico hover_tool', 'rel' => 'edit')); ?>
			<?php echo $this->Form->postLink(___(''), array('action' => 'delete', $group['Group']['id']), array('class' => 'delete_ico hover_tool', 'rel' => 'delete'), ___('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => ___('Page {:page} of {:pages}, showing ({:start} - {:end}) / {:count} records')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
        <?php */ ?>
</div>
