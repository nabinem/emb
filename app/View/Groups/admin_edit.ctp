<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
    echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>\n";
    $this->end();
?>

<?php
    $defaults = array(
        'between'   => '<div class="controls">',
        'after'     => '</div>',
        'div'       => array('class' => 'control-group'),
        'label'     => array('class' => 'control-label')
    );
?>
<div class="groups form pos">
<h2><?php echo ___('Admin Edit Group'); ?></h2>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Group', array('inputDefaults' => $defaults, 'class' => 'form-horizontal'));?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
