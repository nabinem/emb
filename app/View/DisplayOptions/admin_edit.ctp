
<?php
    $selected_tab = '';
if ($this->name == 'DisplayOptions' AND !$this->request->is('ajax')) {
        $this->extend('/Common/admin/afterlogin');
        $selected_tab = 'selected_tab';
}

if(!$this->request->is('ajax')) {
        $this->append('more_tab');
        echo "<li class=\"js-content-tab $selected_tab\" ><a href=\"\"><span>Display Options</span></a></li>\n";
        $this->end();
}
?>

<div class="displayOptions form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php // echo __('Admin Edit Display Option'); ?></h2>
<h4><?php echo __('To edit Elements Go to Page Elements'); ?></h4>
<?php echo $this->Form->create('DisplayOption', array('action' => 'admin_edit', 'class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('header', array('type' => 'select', 'empty' => 'Default'));
		echo $this->Form->input('top_banner', array('type' => 'select', 'empty' => 'Default'));
		echo $this->Form->input('page_description', array('type' => 'select', 'empty' => 'Default'));
		echo $this->Form->input('footer', array('type' => 'select', 'empty' => 'Default'));
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
