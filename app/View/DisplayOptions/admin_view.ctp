<div class="displayOptions view">
<h2><?php  echo __('Display Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Header'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['header']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Top Banner'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['top_banner']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Page Description'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['page_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Footer'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['footer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($displayOption['DisplayOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Display Option'), array('action' => 'edit', $displayOption['DisplayOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Display Option'), array('action' => 'delete', $displayOption['DisplayOption']['id']), null, __('Are you sure you want to delete # %s?', $displayOption['DisplayOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Display Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Display Option'), array('action' => 'add')); ?> </li>
	</ul>
</div>
