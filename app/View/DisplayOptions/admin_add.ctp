
<?php
    $this->extend('/Common/admin/afterlogin');
    
    $this->end();
?>

<div class="displayOptions form pos">
<?php echo $this->Session->flash(); ?> 
<h2><?php echo __('Admin Add Display Option'); ?></h2>
<?php echo $this->Form->create('DisplayOption', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('header');
		echo $this->Form->input('top_banner');
		echo $this->Form->input('page_description');
		echo $this->Form->input('footer');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
