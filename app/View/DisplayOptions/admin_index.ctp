
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="displayOptions index pos">
	<h2><?php echo __('Display Options'); ?></h2>
	<?php echo $this->Session->flash(); ?> 
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('header'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('top_banner'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('page_description'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('footer'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions imp_1"><?php echo __('Edit'); ?></th>
		<th class="actions imp_1"><?php echo __('Delete'); ?></th>
	</tr>
	<?php
	foreach ($displayOptions as $displayOption): ?>
	<tr>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['header']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['top_banner']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['page_description']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['footer']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['created']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($displayOption['DisplayOption']['modified']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $displayOption['DisplayOption']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $displayOption['DisplayOption']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), __('Are you sure you want to delete # %s?', $displayOption['DisplayOption']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="pagination">
	<?php
//		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//		echo $this->Paginator->numbers(array('separator' => ''));
//		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>