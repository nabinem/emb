<div class="content_pos_inner content_pos_outer">
    <div class="content_pos item_pos overview_pos transp_bt font_resize">
        <div class="cell_item_container"><?php
        foreach ($allChildren as $skey => $svalue):
            $child = $this->Cms->getPageContent($svalue['Menu']['extra_params']);
            $appendCls = ($skey > 2) ? 'switch' : '';

            $partslink = $page['Menu']['routing_link'] . $svalue['Menu']['routing_link'];
            ?>
            <div class="cell_item_cont">
                <div class="cell_item <?= $appendCls ?>">                    
                    <div class="foto">
                        <?php if (!empty($child['Page']['dimage'])): ?>                            
                            <?php echo $this->Html->link($this->Cms->uImage($child['Page']['dimage'], 'page', 'small'), $partslink,array('escape'=>false,'class'=>'blink','id'=>$svalue['Menu']['extra_params']));  ?>            
                        <?php endif ?>
                    </div>
                  <a class="cell-sp-pro" href="<?php echo $this->base.$partslink; ?>">
                        <div class="detail blink" id="<?= $svalue['Menu']['extra_params'] ?>" style="cursor:pointer">
                            <span class="link link-sep">
                                <span class="tri-shape"></span>
                                <span class="p_title"><?php echo $child[0]['Page__i18n_title']; ?></span></span>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach ?></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="footer_wrap">
        <div class="bottom_pos">
            <div class="bottom_item">

                <?php if (!empty($page['Page']['embed'])): ?>
                    <a class="fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>"><img width="210" height="110" src="http://i1.ytimg.com/vi/<?= $this->Cms->getVideoId($page['Page']['embed']) ?>/3.jpg" alt=""></a>
                    <a class="link fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>">Instruction Film</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                            height = 600;

                            jQuery(".fancybox").fancybox({
                                helpers: {
                                    media: true
                                },
                                width: 16 / 9. * height,
                                height: height,
                                aspectRatio: true,
                                scrolling: 'no',
                                padding: 0,
                                margin: 0
                            });
                        });
                    </script>
                <?php else: ?>
                    <?php if (!empty($page['Page']['mimage1'])): ?>
                        <?php echo $this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'); ?>
                    <?php endif ?>
                    <a class="link" href="javascript:void(0)">Instruction link</a>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                        });
                    </script>
                <?php endif ?>

            </div>
            <div class="cell_big cell_ban transp">
                <span class="bold_tag">
                    <?php echo $page['Page']['content_long']; ?>
                </span>
                <?php if ($isMobile && strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')): ?>

                    <span div="start" style="position: relative; z-index: 99999;">                      

                            <?php echo $this->Html->link($this->Cms->aImage('start.png'), 'javascript:void(0)', array('escape' => false, 'class' => 'link-video-start')); ?>

                        

                </span>

            <?php else: ?>

                <span class="start" style="position: relative;
    z-index: 99999;">            

                        <?php echo $this->Cms->aImage('start.png'); ?>                   

                </span>

            <?php endif; ?>
            </div>
        </div>
        <div class="clear"></div>
        
       <script> 
	   jQuery(document).ready(function($){
		   var photo_height;
		   function calc_bluebar(){
			   
			    //photo_height = jQuery('.cell_item:first').find('.foto img').height();
				photo_height = jQuery('.transp_bt').height();
				jQuery('.cell_item').each(function(){
					jQuery(this).find('.cell-sp-pro .detail').css({'padding-top':photo_height*(2/3)+'px'});
				
				});
				
				jQuery('.item_pos .cell_item .foto').css({'height':photo_height});
				//jQuery('.item_grid_cont .flexslider .slides > li img').css({'height':photo_height*0.7, 'width':'auto'});
				/*if(jQuery('.item_grid_cont .flexslider .slides > li img').width()>slot_width){
					jQuery('.item_grid_cont .flexslider .slides > li img').css({'width':'100%' , 'height':'auto'});
				}*/
				
		   }
		   calc_bluebar();
		   
		   jQuery(window).resize(function() {
            		calc_bluebar();
       		 });
		});
       </script>
        <?php echo $this->element('front/footer'); ?>
    