<meta charset="utf-8">
<?php //echo $this->Html->charset('utf-8')."\n"; ?>
<?php
$meta_title=(!empty($meta['Metum']['meta_title'])) ? $meta['Metum']['meta_title']: Configure::read('cmsSiteSetting.meta_title');
$meta_description=(!empty($meta['Metum']['meta_description'])) ? $meta['Metum']['meta_description']: Configure::read('cmsSiteSetting.meta_title');
$meta_keywords=(!empty($meta['Metum']['meta_keywords'])) ? $meta['Metum']['meta_keywords']: Configure::read('cmsSiteSetting.meta_keywords');
 ?>
<title><?php echo $meta_title; ?></title>
<?php echo "\n".$this->Html->meta('description', $meta_description ); ?>
<?php echo "\n".$this->Html->meta('keywords', $meta_keywords)."\n"; ?>
