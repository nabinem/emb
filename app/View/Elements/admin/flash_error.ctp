<div class="alert alert-error">
    <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Error!</strong> <?php echo $message; ?>
</div>