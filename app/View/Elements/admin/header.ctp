<header>
    <section class="user_bar">
        <div class="container">
            <!-- <div class="lang_chooser">
                <a href="<?php echo $this->webroot.'common/language/eng'; ?>" class="tt" data-original-title="English" data-placement="bottom">
                    <img src="<?php echo $this->Html->assetUrl('/admin-assets/images/gb.png'); ?>" />
                </a>
                <a href="<?php echo $this->webroot.'common/language/dut'; ?>" class="tt" data-original-title="Dutch" data-placement="bottom">
                    <img src="<?php echo $this->Html->assetUrl('/admin-assets/images/nl.png'); ?>" />
                </a>
               <a href="<?php echo $this->webroot.'common/language/tha'; ?>"  class="tt" data-original-title="Thai" data-placement="bottom">
                <img src="<?php echo $this->Html->assetUrl('/admin-assets/images/thai.png'); ?>" />
            </a>
            </div> -->
            <div class="user_ctrl">
            <?php if($this->Session->read('Auth.User.id')){ ?>
                <a href="<?php echo $this->Html->url(aca('index','users')); ?>">
                    <i class="icon-home icon-white "></i> home </a>
                <a href="<?php echo $this->Html->url(aca('view','users',$this->Session->read('Auth.User.id'))); ?>">
                    <i class="icon-user icon-white "></i><?php echo $this->Session->read('Auth.User.username'); ?></a>
                <a href="<?php echo $this->Html->url(aca('logout','users')); ?>">
                    <i class="icon-off icon-white "></i> logout </a>
            <?php }else{ ?>
                <a href="#">
                       <!-- <i class="icon-question-sign icon-white "></i> Help -->
                </a>
            <?php } ?>
            </div>
        </div>
    </section>
</header>
