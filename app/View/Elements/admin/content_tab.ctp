<?php

/* Example array

$displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same',
                'parent_tab' => 'same',
                'visible_action' => 'edit',
            );
*/

    $displayLinks = array();
    // content tab for specific actions
    switch(strtolower($this->name)){

        case 'groups':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'users':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'system':
            $displayLinks[] = array(
                'url' => 'menus/add',
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            break;

        case 'pages':
            $displayLinks[] = array(
                'url' => BASE_URL.'/admin/system',
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'meta':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'menus':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => BASE_URL.'/admin/system',
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'translationstrings':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'pageelements':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'carousels':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'legends':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'categories':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

     case 'products':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'emailtemplates':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'customers':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'orders':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'payments':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'discounts':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'coupons':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'newsletters':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

    case 'subscribers':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'displayoptions':
            $displayLinks[] = array(
                'url' => null,
                'name' => 'New',
                'action' => 'admin_add',
                'linkType' => 'same'
            );
            $displayLinks[] = array(
                'url' => null,
                'name' => 'List',
                'action' => 'admin_index',
                'linkType' => 'same'
            );
            break;

        case 'sitesettings':

            break;

        default:
            break;
    }

?>
<div class="page_content gradient">
<?php if($this->view != 'admin_dashboard'){ ?>
    <div class="nav_tabs">
            <h2><?php echo (!empty($title_for_tab)) ? $title_for_tab: $this->name; ?></h2>
            <ul>
            <?php foreach($displayLinks as $link){ ?>
                <li <?php if($link['action'] == $this->view){ ?>class="selected_tab"<?php } ?>><a href="<?php echo ($link['url'])? $link['url'] : $this->Html->url(array('action' => $link['action'])); ?>"><span><?php echo $link['name']; ?></span></a></li>
            <?php } ?>
            <?php
                echo $this->fetch('more_tab');
            ?>
            </ul>
    </div>
<?php }else{ ?>
        <h1>Dashboard</h1>
<?php } ?>
</div>
