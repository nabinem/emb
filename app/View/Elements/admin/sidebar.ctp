<section class="logo_cont">
    <div class="logo_container">
        <a href="<?=$this->Html->url('/admin/users/dashboard')?>"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/logo.png'); ?>"></a>
    </div>
    <section class="page_manager">
        <h2 class="menu_title menu_title_text">Page Manager</h2>
        <h2 class="menu_title menu_ctrl"><i class="icon-chevron-down icon-white"></i>Page Manager</h2>
        <?php echo $this->Session->flash('admin_sidebar'); ?>
        <div class="v_menu">
            <?php echo $this->Cms->getAdminSidebarMenu(); ?>
        </div>
    </section>
</section>