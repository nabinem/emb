<div class="alert alert-info">
    <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Info!</strong> <?php echo $message; ?>
</div>