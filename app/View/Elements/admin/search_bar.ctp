<?php

echo $this->Form->create(Inflector::singularize($this->name), array('action' => 'index', 'class' => 'well form-inline'));
echo $this->Form->input('search', array('div' => false, 'class' => 'input-xlarge', 'placeholder' => @$placeholder));
echo $this->Form->submit('Search', array('type' => 'submit', 'div' => false, 'class' => 'btn btn-info'));
echo $this->Form->end();

?>