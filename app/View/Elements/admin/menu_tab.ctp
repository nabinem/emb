<?php
 $activeClass = 'class="current"';
 $vControllers = array(
     'system',
     'users',
     'site_settings'
 );
?>
<ul class="menu">
<?php

echo '<li '.((strtolower($this->view) == "admin_dashboard") ? $activeClass: '').'>
<a href="'.$this->Html->url(array('controller' => "users", 'action'=>'dashboard')).'" >Dashboard</a>
</li>'."\n";

// echo '<li '.((strtolower($this->name) == "menus" && $this->action=="add") ? $activeClass: '').'>
// <a href="'.$this->Html->url(array('controller' => "menus", 'action'=>'add')).'" >Add New Page</a>
// </li>'."\n";

foreach($vControllers as $control){

     echo '<li '.(($this->name == Inflector::camelize($control) AND (strtolower($this->view) != "admin_dashboard")) ? $activeClass: '').'>
     <a href="'.$this->Html->url(array('controller' => Inflector::underscore($control), 'action'=>'index')).'" >'. ___(Inflector::humanize($control)).'</a>
     </li>'."\n";
}
?>
</ul>
<div class="compact_menu">
</div>
