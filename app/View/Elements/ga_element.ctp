<?php
$ga_active = Configure::read('cmsSiteSetting.activate_google_analytics');
$ga_code = Configure::read('cmsSiteSetting.google_analytics_code');
if(empty($ga_code) OR empty($ga_active)){
        return;
}
?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php echo @$ga_code; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>