<?php $menuItems = $this->Cms->getUserSidebarMenu(false, 1, 'all'); 
?>
<header>
    <h1 class="logo"><a href="<?=$this->Html->url('/')?>">
    <?php echo $this->Html->image('../front-assets/images/logo.png', array('alt' => 'EMB'))?>
    </a>
    </h1>
    <nav>
      <ul>
      	<?php foreach ($menuItems as $key => $menuItem):
        	if ($key==0) { continue; }
        	if ($menuItem['Menu']['routing_link']=='/footer_links') { continue; }
        	$headLinker=$menuItem['Menu']['routing_link'];
        	echo "<li><a id=\"{$menuItem['Menu']['extra_params']}\" href=\"{$this->Html->url($headLinker)}\">{$menuItem[0]['Menu__i18n_name']}</a></li>";
           ?>
          <?php endforeach ?>
      </ul>
    </nav>
</header>