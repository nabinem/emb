<section class="content">
	<div class="col_cont-3 clearfix">
    <h2><?php echo $page['Page']['title']; ?></h2>
    <p><?php echo $page['Page']['detail_text_left']; ?></p>
    </div>
    
<div class="clear"></div>
<a href="javascript:void(0)" class="back-btn" id="<?php echo $this->Cms->getExtraParamById($page['Menu']['parent_id']); ?>">< back</a>
</div>
<script type="text/javascript">
                jQuery(document).ready(function($) {
                	jQuery('body >.page').removeClass('page_grid');
                    height = 600;

                    jQuery(".fancybox")
                    .attr("rel", "gallery")
                    .fancybox({
                        helpers : {
                            media: true
                        },
                        width       : 16/9. * height,
                        height      : height,
                        aspectRatio : true,
                        scrolling   : 'no',
                        padding:0,
                        margin:0
                    });               
                });
            </script>
<div class="clear"></div>
<div class="footer_wrap">
<div class="bottom_pos">
    <div class="cell_big transp">
        <span class="bold_tag"><?php echo $page['Page']['detail_text_bottom']; ?></span>
    </div>
</div>
<div class="clear"></div>
<?php echo $this->element('front/footer'); ?>