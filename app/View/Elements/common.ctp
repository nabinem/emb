<section class="content">
    <div class="col_cont-3 clearfix">
	<?php echo $page['Page']['content_long']; ?>
	<?php if (!empty($page['PageImage'])) { ?>
	<div class="col">
	<figure class="fluid">
    	<div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-pause-on-hover="true" data-cycle-speed="200" data-cycle-timeout="2000">
		<div class="cycle-prev"></div>
    	<div class="cycle-next"></div>
		<?php foreach ($page['PageImage'] as $detail) { ?>
		<img src="<?php echo $this->webroot.'files/PageImage/'.$page['Page']['id'].'/'.$detail['image']?>" width="194" alt="image" />
		
		<?php } ?>
	</figure>
	</div>
	<?php }?>
	</div>
</section>
<aside class="bottom clearfix">
    <div class="box box_small floatL">
    <?php if (!empty($page['Page']['embed'])): ?>
		<a class="fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>"><img width="210" height="110" src="http://i1.ytimg.com/vi/<?= $this->Cms->getVideoId($page['Page']['embed']) ?>/3.jpg" alt=""></a>
        <a class="link fancybox" href="<?= $this->Cms->getVideoSrc($page['Page']['embed']) ?>">Instruction Film</a>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
        	if (jQuery('div').hasClass('switch')) {
            	jQuery('body >.page').addClass('page_grid');
         	} else {
            	jQuery('body >.page').removeClass('page_grid');
         	}
			height = 600;

            jQuery(".fancybox").fancybox({
            	helpers: {
                media: true
                },
                width: 16 / 9. * height,
                height: height,
                aspectRatio: true,
                scrolling: 'no',
                padding: 0,
                margin: 0
          	});
      	});
        </script>
        <?php else: ?>
        	<?php if (!empty($page['Page']['mimage1'])): ?>
            	<?php echo $this->Cms->uImage($page['Page']['mimage1'], 'page', 'small'); ?>
			<?php endif ?>
            <a class="link" href="javascript:void(0)">Instruction link</a>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                            if (jQuery('div').hasClass('switch')) {
                            	jQuery('body >.page').addClass('page_grid');
                            } else {
                            	jQuery('body >.page').removeClass('page_grid');
                            }
                        });
                    </script>
                <?php endif ?>
    </div>
    <div class="box box_med  floatR"><span class="font_m"><?php echo $page['Page']['detail_text_bottom']; ?></span></div>
  </aside>
<?php //pr($page['PageImage'])?>  