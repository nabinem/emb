<?php
$this->extend('/Common/admin/afterlogin');
$this->end();
?>

<div class="dashboard pos">
        <h2 class="title1"><?php echo ___('Content Management System'); ?></h2>
        <div class="home_icons">
                <a href="<?php echo $this->Html->url(aca('index', 'menus/add')); ?>" class="home_icons_slot ">
                        <div class="icon_cont"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/new_page.png'); ?>" ></div>
                        <?php echo ___('Add New Menu'); ?> </a>
                <a href="<?php echo $this->Html->url(aca('index', 'users')); ?>" class="home_icons_slot ">
                        <div class="icon_cont"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/uer_mgmt.png'); ?>" ></div>
                        <?php echo ___('Users'); ?> </a>

                <a href="<?php echo $this->Html->url(aca('index', 'menus')); ?>" class="home_icons_slot ">
                        <div class="icon_cont"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/settings.png'); ?>" ></div>
                        <?php echo ___('Pages'); ?></a>

                <a href="<?php echo $this->Html->url(aca('index', 'site_settings')); ?>" class="home_icons_slot ">
                        <div class="icon_cont"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/settings2.png'); ?>" ></div>
                        <?php echo ___('Site Setting'); ?></a>

                <div class="clear"></div>
        </div>

</div>