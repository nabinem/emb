<h1>Stats</h1>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
<form class="form">
    <fieldset>
        <div class="small_select">
            <div class="input select">
                <label>Select time frame</label>
                <select>
                    <option >Selecxt one ----</option>
                </select>
                <select>
                    <option >Selecxt one ----</option>
                </select>
                <select>
                    <option >Selecxt one ----</option>
                </select>
            </div>
        </div>
    </fieldset>
</form>
<div class="clear"></div>

<ul class="idTabs clearfix">
    <li>
        <h2><a href="#idTab1">Desktop</a></h2>
    </li>
    <li>
        <h2><a href="#idTab2">Tablet</a></h2>
    </li>
    <li>
        <h2><a href="#idTab3">Mobile</a></h2>
    </li>
    <li>
        <h2><a href="#idTab4">TV</a></h2>
    </li>
</ul>
<div class="" id="idTab1">
    <form class="form">
        <fieldset>
            <div class="input checkbox radio">
                <label class="out_label">Field Name</label>
                <div class="input_group">
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                    <input type="checkbox">
                    <label> check buttons</label>
                </div>
            </div>

            <div class="input select">
                <label>Select page</label>
                <select>
                    <option >Selecxt one ----</option>
                </select>
            </div>
            <div class="input text">
                <label>Name</label>
                <input type="text" />
            </div>
            
            <?php
            $languages = array(
                'eng' => 'English',
                'ger' => 'German',
                'fre' => 'French'
            );
            
            echo $this->AdminCms->multiLanguageTab($languages);
            
            
            
            ?>

            <ul class="idTabs clearfix smallTab">
                <li><a href="#idTab11">Dutch</a></li>
                <li><a href="#idTab22" class="selected">English</a></li>
                <li><a href="#idTab33">German</a></li>
            </ul>

            <div class="tabCont"> <div class="" id="idTab11">
                    <div class="input text">
                        <label>Dutch</label>
                        <input type="text" value="Dutch" />
                    </div>
                </div>
                <div class="" id="idTab22">
                    <div class="input text">
                        <label>English</label>
                        <input type="text" value="English" />
                    </div>
                </div>
                <div class="" id="idTab33">
                    <div class="input text">
                        <label>German</label>
                        <input type="text" value="German" />
                    </div>
                </div></div>

            <div class="input text">
                <label>Google Analytics Code</label>
                <input type="text" />
            </div>
            <div class="input text">
                <label>Google Analytics Profile Code</label>
                <input type="text" />
            </div>
            <ul class="idTabs clearfix smallTab textareaTab tab_right">
                <li><a href="#idTab111">Dutch</a></li>
                <li><a href="#idTab222" class="selected">English</a></li>
                <li><a href="#idTab333">German</a></li>
            </ul>

            <div class="tabCont textareaTab"><div class="" id="idTab111">
                    <div class="textarea input editor_input">
                        <label>Content</label>
                        <div class="editor_cont">
                            <textarea rows="20" cols="20" class="ckeditor"  name="editor11"></textarea>
                        </div>
                    </div>
                </div>
                <div class="" id="idTab222">
                    <div class="textarea input editor_input">
                        <label>Content</label>
                        <div class="editor_cont">
                            <textarea rows="20" cols="20" class="ckeditor"  name="editor22"></textarea>
                        </div>
                    </div>
                </div>
                <div class="" id="idTab333">
                    <div class="textarea input editor_input">
                        <label>Content</label>
                        <div class="editor_cont">
                            <textarea rows="20" cols="20" class="ckeditor"  name="editor33"></textarea>
                        </div>
                    </div>
                </div></div>

            <div class="submit input">
                <label>&nbsp;</label>
                <input type="submit" value="Submit Button" />
            </div>
        </fieldset>
    </form>
</div>
<div class="" id="idTab2">
    <form class="form">
        <fieldset>
            <div class="input select">
                <label>Select page</label>
                <select>
                    <option >Selecxt one ----</option>
                </select>
            </div>
            <div class="textarea input editor_input">
                <label>Content</label>
                <div class="editor_cont">
                    <textarea rows="20" cols="20" class="ckeditor"  name="editor2"></textarea>
                </div>
            </div>
            <div class="submit input">
                <label>&nbsp;</label>
                <input type="submit" value="Submit Button" />
            </div>
        </fieldset>
    </form>
</div>
<div class="" id="idTab3">
    <div class="success_msg"> <span><strong>Success message</strong> |   Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</span> </div>
    <div class="fail_msg"> <span><strong>Success message</strong> |   Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</span> </div>
    <form class="form">
        <fieldset>
            <div class="textarea input editor_input">
                <label>Content</label>
                <div class="editor_cont">
                    <textarea rows="20" cols="20" class="ckeditor"  name="editor3"></textarea>
                </div>
            </div>
            <div class="submit input">
                <label>&nbsp;</label>
                <input type="submit" value="Submit Button" />
            </div>
        </fieldset>
    </form>
</div>
<div class="" id="idTab4">
    <form class="form">
        <fieldset>


            <div class="input text">
                <label>Sub Domain</label>
                <input type="text" />
            </div>
            <div class="input text">
                <label>Google Analytics Code</label>
                <input type="text" />
            </div>
            <div class="input text">
                <label>Google Analytics Profile Code</label>
                <input type="text" />
            </div>

            <div class="textarea input editor_input">
                <label>Content</label>
                <div class="editor_cont">
                    <textarea rows="20" cols="20" class="ckeditor"  name="editor4"></textarea>
                </div>
            </div>
            <div class="submit input">
                <label>&nbsp;</label>
                <input type="submit" value="Submit Button" />
            </div>
        </fieldset>
    </form>
</div>