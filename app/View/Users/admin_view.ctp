<?php
$this->extend('/Common/admin/afterlogin');
$this->end();
?>

<div class="users view details pos">
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Html->link(___('Edit User'), array('action' => 'edit', $user['User']['id']), array('class' => 'floatR btn_black')); ?>
	<dl>
		<dt><label style="width:100px;float:left;text-align:left;padding-right:10px;font-weight:bold;"><?php echo ___('Username'); ?> :</label>
			<span style="font-weight:normal"><?php echo h($user['User']['username']);?></span>
		</dt><br>
		<dt><label style="width:100px;float:left;text-align:left;padding-right:10px;font-weight:bold;"><?php echo ___('Email'); ?> :</label>
			<span style="font-weight:normal"><?php echo h($user['User']['email']); ?><span>
			</dt><br>
		<dt><label style="width:100px;float:left;text-align:left;padding-right:10px;font-weight:bold;"><?php echo ___('Group'); ?> :</label>
			<span style="font-weight:normal"><?php echo h($user['Group']['name']); ?><span>
			 </dt><br>
		<dt><label style="width:100px;float:left;text-align:left;padding-right:10px;font-weight:bold;"><?php echo ___('Time Zone'); ?> :</label>
			<span style="font-weight:normal"><?php echo timeZoneList($user['User']['time_zone']); ?><span>
			</dt><br>
		<dt><label style="width:100px;float:left;text-align:left;padding-right:10px;font-weight:bold;"><?php echo ___('Last Login'); ?> :</label>
			<span style="font-weight:normal"><?php echo h($user['User']['last_login']); ?><span>
			</dt><br>
	</dl>
</div>
