<?php
$this->extend('/Common/admin/main');
$this->end();
?>
<div class="logo_container textC">
    <h1><a href="#"><img src="<?php echo $this->Html->assetUrl('/admin-assets/images/logo.png'); ?>" /></a></h1>
</div>
<div class="login_modal">
    <div class="login_title"><h1>Admin login</h1></div>
    <div class="pos">
        <?php echo $this->Session->flash('auth'); ?>
       <?php echo $this->Form->create('User', array('class' => 'form login')); ?>
            <div class="control-group"><label>Username</label>
                    <input tabindex="1" type="text" class="span3" placeholder="Type username" name="data[User][username]">
<!--                <p class="help-block">Example block-level help text here.</p></div>-->

            <div class="control-group"><label>Password</label>
                <input tabindex="2" type="password" class="span3" placeholder="Type password" name="data[User][password]"></div>
            <div class="control-group"><label class="checkbox">
                    <input tabindex="3" type="checkbox" name="logged_in"> Keep me logged in
                </label></div>
            <button tabindex="4" type="submit" class="btn btn-large btn-primary">LOGIN</button>
       <?php echo $this->Form->end();  ?>
    </div>
</div>
<?php /* ?>
<div class="login_box">
    <h1>User login</h1>
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Session->flash('auth'); ?>
  
    <?php 
    
        echo $this->Form->create('User', array('class' => 'form login'));
            
        echo $this->Form->inputs(
                array(
                    'username',
                    'password',
                    'Remember Me' => array('type' => 'checkbox'),
                    ___('Submit Button') => array('type' => 'submit'),
                    'legend' => false
                ));
        
        echo $this->Form->end(); 
    
    ?>
</div>
<?php */ ?>