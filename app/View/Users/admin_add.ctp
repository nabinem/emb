<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="users form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults')));?>
	<fieldset>
	<?php
		echo $this->Form->input('username');
                echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('group_id');
		echo $this->Form->input('time_zone', array('type' => 'select', 'options' => timeZoneList(), 'escape' => false, 'selected' => '0.0'));
		// echo $this->Form->input('status', array('type' => 'select'));

	?>
	</fieldset>

<?php echo $this->Form->input(___('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

