
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>
";
    $this->end();
?>

<div class="users form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('User');?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('new_password', array('type' => 'password'));
		echo $this->Form->input('retype_password', array('type' => 'password'));
		echo $this->Form->input('email');
		echo $this->Form->input('group_id');
        echo $this->Form->input('time_zone', array('type' => 'select', 'options' => timeZoneList(), 'escape' => false));
		// echo $this->Form->input('status', array('type' => 'select'));
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

