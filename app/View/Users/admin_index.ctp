<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="users index pos">
        <?php echo $this->Session->flash(); ?>
        <table  border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id');?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('username');?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('email');?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('group_id');?></th>
			<th><?php echo $this->Paginator->sort('last_login');?></th>
			<th class="actions imp_1"><?php echo __('Edit'); ?></th>
			<th class="actions imp_1"><?php echo __('Delete'); ?></th>
	</tr>
	<?php
	foreach ($users as $user): ?>
	<tr>
		<td class="imp_1"><?php echo $this->Html->link($user['User']['id'], array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>&nbsp;</td>
		<td class="imp_1"><?php echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>&nbsp;</td>
		<td class="imp_1"><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['Group']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['last_login']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $user['User']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php if ($user['Group']['id']!=1 && $this->Session->read('Auth.User.id')!=$user['User']['id']):
			echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $user['User']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), __('Are you sure you want to delete # %s?', $user['User']['username']));
			endif ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
        <div class="pagination">
	<p>
	<?php
//	echo $this->Paginator->counter(array(
//	'format' => ___('Page {:page} of {:pages}, showing ({:start} - {:end}) / {:count} records')
//	));
	?>	</p>

	<div class="paging">
	<?php
		// echo $this->Paginator->prev('< ' . ___('previous'), array(), null, array('class' => 'prev disabled'));
		// echo $this->Paginator->numbers(array('separator' => ''));
		// echo $this->Paginator->next(___('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
        </div>
</div>