<?php
/**
 * Helper functions for the CMS.
 */
App::uses('Helper', 'View');

/**
 * CMS helper
 *
 * Helpers required by the CMS goes here.
 *
 * @package       app.View.Helper
 * @property    Menu $Menu
 */
class CmsHelper extends AppHelper {

    public $helpers = array(
        'Html',
        'Form'
    );

    public function getAdminSidebarMenu() {

        App::uses('Menu', 'Model');

        $this->Menu = ClassRegistry::init('Menu');

        $menuTree   = $this->Menu->getMenuTreeForDisplay();

        return $this->_menuAdminLevelHtml($menuTree);
    }

    protected function _menuAdminLevelHtml($array, $level=0) {

    $joinStrings = '';
    foreach($array as $k => $vals) {
            $joinStrings .= '<li><big>';
            $joinStrings .= (empty($vals['children']))? '':'<span class="plus"><i class="icon-plus icon-white"></i></span>';
            $joinStrings .= '<a target="_blank" href="'.$this->Html->url($vals['Menu']['routing_link']).'">'.$vals['Menu']['name'].'</a>';
            $joinStrings .= '<a href="'.$this->Html->url(aca('change_order', 'menus', 'up', $vals['Menu']['id'])).'" class="ico_btn tt" data-original-title="move up"><i class="icon-white  icon-arrow-up"></i></a>';
            $joinStrings .= '<a href="'.$this->Html->url(aca('change_order', 'menus', 'down', $vals['Menu']['id'])).'" class="ico_btn ico_btn2 tt"  data-original-title="move down"><i class="icon-white  icon-arrow-down" ></i></a>';
            $joinStrings .= '<a href="'.$this->Html->url(aca('edit', 'pages', $vals['Menu']['extra_params'])).'" class="ico_btn ico_btn3 tt"  data-original-title="edit"><i class="icon-white  icon-edit"></i></a>';
            $joinStrings .= '<a href="#" class="ico_btn tt ico_btn4" data-original-title="Visible/Hidden"><i class="icon-white  '.(($vals['Menu']['is_active'])? 'icon-eye-open' : 'icon-eye-close' ).'"></i></a>';
            $joinStrings .= '</big>';
            $joinStrings .= (empty($vals['children']))? '': $this->_menuAdminLevelHtml($vals['children']);
            $joinStrings .= '</li>'."\n";
    }

    return '<ul>'.$joinStrings.'</ul>';
    }


    protected function _menuUserLevelHtml($array, $level = 0) {

            $joinStrings = '';
            foreach ($array as $k => $vals) {
                    $joinStrings .= '<li><big>';
                    $joinStrings .= (empty($vals['children'])) ? '' : '<span class="plus"><i class="icon-plus icon-white"></i></span>';
                    $joinStrings .= '<a href="' . $this->Html->url($vals['Menu']['routing_link']) . '">' . $vals['Menu']['name'] . '</a>';
                    $joinStrings .= '</big>';
                    $joinStrings .= (empty($vals['children'])) ? '' : $this->_menuUserLevelHtml($vals['children']);
                    $joinStrings .= '</li>' . "\n";
            }

            return '<ul>' . $joinStrings . '</ul>';
    }

    public function getUserSidebarMenu($html=true, $level=false, $findType = 'threaded', $childrenOf=null) {

            App::uses('Menu', 'Model');
            $this->Menu = new Menu();

            $menuTree = $this->Menu->getMenuTreeForDisplay(1,'visible', $level, $findType, $childrenOf);

            if(!$html) {
                    return $menuTree;
            }

            return $this->_menuUserLevelHtml($menuTree);
    }

/**
 * Returns multi-inputs for different loacales.
 *
 * @return string
 */
    function form(){

            $params = $args = func_get_args();

            $type = end($params);

            $currentModel = $this->Form->model();

            $fieldNameParts = explode(".", $params[0]);

            $ModelClass = ClassRegistry::init($currentModel);

            if(!$ModelClass) {
                    return;
            }else if(!isset($ModelClass->actsAs['Translate'])){
                    return call_user_func_array(array($this->Form, $type), $args);
            }

            // Append the Model name if not there.
            if(count($fieldNameParts) == 1){
                    $params[0] = $args[0] = "$currentModel.$params[0]";
            }

            $parts = explode(".", $params[0]);
            $fieldName = Inflector::humanize($parts[1]);

            $htmlBuild = '';

            $availableLanguages = Configure::read('Setting.languages');
            foreach($availableLanguages as $code => $languageName){
                    $args[0] = $params[0].'.'.$code;
                    if(!empty($params[1]['label'])){
                            $label = $params[1]['label'];
                            if(is_array($label)){
                                    $args[1]['label'] = array_merge($label, array(
                                    'text' => $fieldName." ($languageName)"
                                    ));
                            }
                            if(is_string($label)){
                                    $args[1]['label'] = $label." ($languageName)";
                            }
                    }
                    $htmlBuild .= call_user_func_array(array($this->Form, $type), $args)."\n";
            }

            return $htmlBuild;
    }
/**
 * Returns multi-inputs for different loacales.
 *
 * @return string
 */
    function multiform(){

            $params = $args = func_get_args();

            $type = end($params);

        	$currentModel = $this->Form->model();

            $fieldNameParts = explode(".", $params[0]);

            $ModelClass = ClassRegistry::init($currentModel);

            if(!$ModelClass) {
                    return;
            }else if(!isset($ModelClass->actsAs['MultiTranslate'])){
                    return call_user_func_array(array($this->Form, $type), $args);
            }

            // Append the Model name if not there.
            if(count($fieldNameParts) == 1){
                    $params[0] = $args[0] = "$currentModel.$params[0]";
            }

            $parts = explode(".", $params[0]);
            $fieldName = Inflector::humanize($parts[1]);

            $htmlBuild = '';

            $availableLanguages = Configure::read('Setting.languages');
            foreach($availableLanguages as $code => $languageName){
                    $args[0] = $params[0].'.'.$code;
                    if(!empty($params[1]['label'])){
                            $label = $params[1]['label'];
                            if(is_array($label)){
                                    $args[1]['label'] = array_merge($label, array(
                                    'text' => $fieldName." ($languageName)"
                                    ));
                            }
                            if(is_string($label)){
                                    $args[1]['label'] = $label." ($languageName)";
                            }
                    }
                    $htmlBuild .= call_user_func_array(array($this->Form, $type), $args)."\n";
            }

            return $htmlBuild;
    }

/**
 * Gets the image tag with original or thumbnails.
 *
 * Useful for the images on Upload plugin
 * @param string $filename
 * @param type $model
 * @param type $type There are 'normal','big','medium','small' options
 * @param type $options
 * @param type $justUrl Sends just url if true
 * @return type
 */
    public function uImage($filename, $model, $type='normal', $options = array(), $justUrl = false) {
            if($type != 'normal'){
                    $filename = $type."_".$filename;
            }
            if($justUrl){
                    return $this->Html->assetUrl(BASE_URL."/files/$model/$filename");
            }
            return $this->Html->image("/files/$model/$filename", $options);
    }

    public function aImage($filename,$options = array()) {
            return $this->Html->image("/front-assets/images/$filename",$options);
    }


    public function pageElement($name, $field = 'content', $status = true) {
            App::uses('PageElement', 'Model');
            $Element = new PageElement();

            $element = $Element->findByNameAndStatus($name, $status);

            if($element){
                    return ($field == 'all')? $element['PageElement']: ((isset($element['PageElement'][$field]))? $element['PageElement'][$field]: '');
            }
            return '';
    }

    public function getMenuContent($pageId){
       App::uses('Menu', 'Model');
        $this->Menu=new Menu;
        $content = $this->Menu->findById($pageId);
        return $content;
    }

    public function getPageContent($pageId){
       App::uses('Page', 'Model');
        $this->Page=new Page;
        $this->Page->recursive=-1;
        $content = $this->Page->findById($pageId);
        return $content;
    }

    public function getFooter($id=Null){
        App::uses('Menu', 'Model');
        $this->Menu=new Menu;
        $footer=$this->Menu->children($id);
        return $footer;
    }

    public function getParent($id=Null){
        App::uses('Menu', 'Model');
        $this->Menu=new Menu;
        $menu= $this->Menu->getParentNode($id);
        return $menu['Menu']['routing_link'];
    }
    public function getExtraParamById($id=Null){
        App::uses('Menu', 'Model');
        $this->Menu=new Menu;
        $menu= $this->Menu->findById($id);
        return $menu['Menu']['extra_params'];
    }
    function _toSlug($string) {
        return Inflector::slug(utf8_encode(strtolower($string)), '-');
    }

     function getVideoSrc($embedCode) {
            $doc = new DOMDocument();
            $doc->loadHTML($embedCode);
            $url = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');
            return $url;
        }

        function getVideoId($embedCode) {
            $doc = new DOMDocument();
            $doc->loadHTML($embedCode);
            $url = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');
            if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
              $values = $id[1];
            } else {
            // not an youtube video
            }
            return $values;
        }
}
