
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->append('more_tab');
echo "<li class=\"js-content-tab selected_tab\" ><a href=\"\"><span>Edit</span></a></li>
";
    $this->end();
?>

<div class="siteSettings form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('SiteSetting', array('type'=>'file','class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('site_name');
		echo $this->Form->input('site_email');
		echo $this->Form->input('meta_title');
		echo $this->Form->input('meta_description');
		echo $this->Form->input('meta_keywords');
		echo $this->Form->input('activate_google_analytics');
		echo $this->Form->input('google_analytics_code');
		echo $this->Form->input('facebook');
		echo $this->Form->input('twitter');
		echo $this->Form->input('linkedin');
		echo $this->Form->input('video_chrome', array(
                          'type' => 'file',
                          'class' => 'input-large',
                          'label' => array('text'=>'Video MP4','class' => 'control-label'),
                          'after' => $this->request->data['SiteSetting']['video_chrome']
                          ));
        echo $this->Form->input('video_ie', array(
                          'type' => 'file',
                          'class' => 'input-large',
                          'label' => array('text'=>'Video IE','class' => 'control-label'),
                          'after' => $this->request->data['SiteSetting']['video_ie']
                          ));
        echo $this->Form->input('video_others', array(
                          'type' => 'file',
                          'class' => 'input-large',
                          'label' => array('text'=>'Video OGV','class' => 'control-label'),
                          'after' => $this->request->data['SiteSetting']['video_others']
                          ));
						  
			  
		 echo $this->Form->input('image', array(
                        'type' => 'file',
                        'class' => 'input-large',
                        'label' => array('text'=>'Site Background','class' => 'control-label'),
                        'after' => $this->Html->link(
						$this->Cms->uImage($this->request->data['SiteSetting']['image'], 'video','small',array('class'=>'fancybox')).'</div>'
   ,'/files/video/'.$this->request->data['SiteSetting']['image'], // or an array('controller' => 'mycontroller', 'action' => 'myaction')
 array('escape' => false, 'class'=>'fancybox'))
                        ));			    
						  
 //       echo $this->Form->input('image', array(
//                        'type' => 'file',
//                        'class' => 'input-large',
//                        'label' => array('text'=>'Site Background','class' => 'control-label'),
//                        'after' => $this->Cms->uImage($this->request->data['SiteSetting']['image'], 'video','small').'</div>'
//                        ));
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<link href="<?php echo $this->webroot.'/source/jquery.fancybox.css'; ?>?t=4" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $this->webroot.'/source/jquery.fancybox.js'; ?>"></script>
<script type="text/javascript">
jQuery(document).ready(function() { jQuery("a.fancybox").fancybox(); });
</script>