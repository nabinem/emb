<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>

<div class="siteSettings view pos">
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Site Name'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['site_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Title'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['meta_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Site Email'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['site_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Site Email From'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['site_email_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Description'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['meta_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Keywords'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['meta_keywords']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Robots'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['meta_robots']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Activate Google Analytics'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['activate_google_analytics']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Google Analytics Code'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['google_analytics_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($siteSetting['SiteSetting']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Site Setting'), array('action' => 'edit', $siteSetting['SiteSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Site Setting'), array('action' => 'delete', $siteSetting['SiteSetting']['id']), null, __('Are you sure you want to delete # %s?', $siteSetting['SiteSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Site Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Site Setting'), array('action' => 'add')); ?> </li>
	</ul>
</div>
