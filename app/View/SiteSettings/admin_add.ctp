
<?php
    $this->extend('/Common/admin/afterlogin');

    $this->end();
?>

<div class="siteSettings form pos">
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('SiteSetting', array('class' => 'form-horizontal', 'inputDefaults' => Configure::read('inputDefaults'))); ?>
	<fieldset>

	<?php
		echo $this->Form->input('site_name');
		echo $this->Form->input('meta_title');
		echo $this->Form->input('site_email');
		echo $this->Form->input('site_email_from');
		echo $this->Form->input('meta_description');
		echo $this->Form->input('meta_keywords');
		echo $this->Form->input('meta_robots');
		echo $this->Form->input('activate_google_analytics');
		echo $this->Form->input('google_analytics_code');
	?>
	</fieldset>
<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'label' => '&nbsp;', 'class' => 'btn-large btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
