
<?php
    $this->extend('/Common/admin/afterlogin');
    $this->end();
?>
<div class="siteSettings index pos">
	<?php echo $this->Session->flash(); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" width="100%">
	<tr>
			<th class="imp_1"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('site_name'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('meta_title'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('site_email'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('site_email_from'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('meta_description'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('meta_robots'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('activate_google_analytics'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('google_analytics_code'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="imp_1"><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions imp_1"><?php echo __('Edit'); ?></th>
		<th class="actions imp_1"><?php echo __('Delete'); ?></th>
	</tr>
	<?php
	foreach ($siteSettings as $siteSetting): ?>
	<tr>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['id']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['site_name']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['meta_title']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['site_email']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['site_email_from']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['meta_description']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['meta_keywords']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['meta_robots']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['activate_google_analytics']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['google_analytics_code']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['created']); ?>&nbsp;</td>
		<td  class="imp_1"><?php echo h($siteSetting['SiteSetting']['modified']); ?>&nbsp;</td>
		<td class="actions imp_1">
			<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $siteSetting['SiteSetting']['id']), array('class'=>'tt', 'data-original-title'=>'edit', 'escape' => false)); ?>
		</td>
		<td class="actions imp_1">
			<?php echo $this->Form->postLink('<i class="icon-trash"></i>', array('action' => 'delete', $siteSetting['SiteSetting']['id']), array('class'=>'tt', 'data-original-title'=>'delete', 'escape' => false), __('Are you sure you want to delete # %s?', $siteSetting['SiteSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="pagination">
	<?php
//		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//		echo $this->Paginator->numbers(array('separator' => ''));
//		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>