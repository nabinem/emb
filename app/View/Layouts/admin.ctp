<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<title>Proshore CMS</title>

<script type="text/javascript" >
        var PCMS = {};
        PCMS.webroot = '<?php echo $this->webroot; ?>';
</script>
<link href="<?php echo $this->Html->assetUrl('/admin-assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $this->Html->assetUrl('/admin-assets/css/common.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $this->Html->assetUrl('/admin-assets/css/layout.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $this->Html->assetUrl('/admin-assets/css/form.css'); ?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/modernizr.custom.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/jquery.cycle.all.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/jquery.idTabs.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/bootstrap-tooltip.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/bootstrap-alert.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/admin-assets/js/main.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->Html->assetUrl('/ckeditor/adapters/jquery.js'); ?>"></script>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
</head>

<body>
<?php echo $this->element('admin/header'); ?>
<?php echo $this->fetch('content'); ?>
<div class="content content_below"> </div>
</body>
</html>
