<!DOCTYPE HTML>
<html>
<head>
  <?php echo $this->element('meta_element')."\n"; ?>
  <meta name="viewport" content="width=1025">
  <link href="<?php echo $this->Html->assetUrl('front-assets/css/common.css'); ?>?t=4" rel="stylesheet" type="text/css">
  <link href="<?php echo $this->Html->assetUrl('front-assets/css/layout.css'); ?>?t=4" rel="stylesheet" type="text/css">
  <link href="<?php echo $this->Html->assetUrl('front-assets/css/form.css'); ?>?t=4" rel="stylesheet" type="text/css">
  
  <script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/jquery.js'); ?>?t=2"></script>
  <script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/modernizr.custom.js'); ?>?t=2"></script>
  <script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/jquery.cycle2.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo $this->Html->assetUrl('front-assets/js/main.js'); ?>?t=2"></script>
  <?php echo $this->element('fancybox_links'); ?>
  <script type="text/javascript" >
  jQuery(document).ready(function($){
	var PCMS = {};
    PCMS.webroot = '<?php echo $this->webroot; ?>';
	function wr(){
      var windowWidth = jQuery(window).width(); 
      var windowHeight = jQuery(window).height();

      if(jQuery(window).width()/jQuery(window).height()<1.33){
    	  jQuery('.bg_video_container').removeClass('wide');
        if(windowHeight>412){
          var em=(windowHeight/412)*0.7;
          if(em<1){em=1;}
          jQuery('body').css({'font-size':em+'em'});
        }
        else if(windowHeight<412){
        	jQuery('body').css({'font-size':'1em'});
        }
      }
      else if(jQuery(window).width()/jQuery(window).height()<1.77){
    	  jQuery('.bg_video_container').removeClass('wide');
        if(windowWidth>592){
          var em=(windowWidth/592)*0.85;
          if(em<1){em=1;}
          jQuery('body').css({'font-size':em+'em'})
        }
        else if(windowWidth<592){
        	jQuery('body').css({'font-size':'1em'});
        }
      }
      else{
    	  jQuery('.bg_video_container').addClass('wide');
        if(windowWidth>592){
          var em=(windowWidth/592)*0.585;
          if(em<1){em=1;}
          jQuery('body').css({'font-size':em+'em'});
        }
        else if(windowWidth<592){
        	jQuery('body').css({'font-size':'1em'});
        }
      }
      var windowProportion = windowWidth / windowHeight;
      var origWidth = 480; origHeight = 270; var origProportion = origWidth / origHeight;
      var newWidth = 0; var newHeight = 0;
      if (windowProportion >= origProportion) {
        proportion = windowWidth / origWidth;
      } 
      else {
        proportion = windowHeight / origHeight;
      }
      newWidth = proportion * origWidth; newHeight = proportion * origHeight;
      //console.log('Window Height:%s, newWidth: %s, newHeight: %s',windowHeight,newWidth,newHeight);
      jQuery('.bg_video').attr('width',newWidth);
      jQuery('.bg_video').attr('height',newHeight);
      jQuery('.bg_video').css('margin-left',-newWidth/2);
    }

	jQuery(window).resize(function(){ wr(); });
        wr();
            	
        jQuery('.start').toggle(function(event){
        	jQuery(this).addClass('play');
        	jQuery('.bg_video').get(0).play(); event.preventDefalut()
        },
        function(){
        	jQuery(this).removeClass('play');
        	jQuery('.bg_video').get(0).pause();  event.preventDefalut()
        }
        );
      });
  </script>
  <?php //echo $this->element('ga_element'); ?>
</head>
<?php $showPage=($this->action=='contact') ? 'class="contact_page"': ''; ?>
<body  style="font-size:1em;" <?=$showPage?>>
	<?php
    if( isset($page['Page']['video_chrome']) && !is_null($page['Page']['video_chrome']) &&
  		(trim($page['Page']['video_chrome']) != '')){
  		$video_chrome =  trim($page['Page']['video_chrome']);
	}
	else {	
		$video_chrome= Configure::read('cmsSiteSetting.video_chrome');
	}
	
     $video_ie= Configure::read('cmsSiteSetting.video_ie');
     if( isset($page['Page']['video_others']) && !is_null($page['Page']['video_others']) &&
  	 	(trim($page['Page']['video_others']) != '')){
  		$video_others =  trim($page['Page']['video_others']);
	}
	else {	
		$video_others= Configure::read('cmsSiteSetting.video_others');
	}
     
     $image= Configure::read('cmsSiteSetting.image');
     $replaceImg=$this->Cms->uImage($image, 'video');
     ?>
     
	<?php if ($this->action=="contact"): ?>
    <div class="bg_video_container">
    	<?=$this->Cms->uImage($page['Page']['mimage1'], 'page','normal',array('class'=>'bg_video'));?>
	</div>
    <div class="video_overlay"></div>
    <?php else:
    	if (!$isMobile): ?>
      	<div class="bg_video_container">
        <video autoplay class="bg_video" loop>
          <source src="<?=BASE_URL.'/files/video/'.$video_chrome?>?t=1" type="video/mp4" />
          <source src="<?=BASE_URL.'/files/video/'.$video_others?>?t=1" type="video/ogg" />
          <source src="<?=BASE_URL.'/files/video/'.$video_ie?>?t=1" type="video/webm" />
          <?=$replaceImg?>
        </video>
          <div class="video_overlay"></div>
      </div>
     <?php else: ?>
     <?php if (strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod')): ?>
      <div class="bg_video_container">
        <?=$replaceImg?>
      </div>
     <?php else: ?>
     
      <div class="bg_video_container">        
        <video id="bg_video" loop style="height:100%;width:100%;">
          <source src="<?=BASE_URL.'/files/video/'.$video_chrome?>" type="video/mp4" />          
        </video>
        <div class="video_overlay"></div>         
      </div>

     <?php endif ?>
     <?php endif ?>
     <?php endif ?>

    


  <div class="page">
	<?php echo $this->element('front/header'); ?>
    <?php echo $this->fetch('content'); ?>
    <footer>
    <?php echo $this->element('front/footer'); ?>
    </footer>
  </div>
</body>
</html>
