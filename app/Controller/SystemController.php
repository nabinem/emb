<?php
App::uses('AppController', 'Controller');
class SystemController extends AppController {

//////////////////////////////////////////////////

	public $uses = 'Menu';

//////////////////////////////////////////////////
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$this->Menu->recursive = 0;

         $this->paginate = array(
                // 'conditions' => array('Menu.is_visible' => 0)
            );

        $this->set('menus', $this->paginate());

        $this->set('title_for_tab', ___('System Content'));

	}

}