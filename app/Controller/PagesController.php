<?php
App::uses('AppController', 'Controller');
/**
 * Pages Controller
 *
 * @property Page $Page
 */
error_reporting(0);
class PagesController extends AppController {

	public $components = array('Email','RequestHandler','ImageUpload');

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->RequestHandler->isMobile()) {
			$this->set('isMobile', true );
		}else{
			$this->set('isMobile', false );
		}
	}
	/**
	 * Displays a view
	 *
	 * @param string What page to display
	 */
	public function display() {

		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage'));
		$meta=$this->Page->Metum->read(null,$page['Page']['id']);
		$this->set(compact('meta'));
		$this->render(implode('/', $path));
	}

	public function view($id=null){
		$this->autoRender = false;
		$this->disableCache();
		$this->viewPath = 'Elements';

		$page = $this->Page->read(null, $id);


		$meta=$this->Page->Metum->read(null,$page['Page']['id']);

		$this->loadModel('Menu');
		$allChildren = $this->Menu->children($page['Menu']['id']);

		$this->set(compact('meta','page','allChildren'));

/*		if ($page['Menu']['parent_id']==28) {
			// $this->view = 'contentpage';
			$this->render('/Elements/ajaxfootreturn');
		}else{
			// $this->view = 'cmspage';
			$this->render('/Elements/ajaxmenureturn');
		}

		if ($id==30) $this->render('/Elements/ajaxreturn');
*/		$this->render('/Elements/common');

	}

	public function show($slug2= null) {
		
		$this->view = 'cms_subpage';
		$this->loadModel('Menu');
		$page= $this->Menu->findByRoutingLink('/'.$slug2);
		
//		$page_images = $this->Page->PageImage->findAllByPageId($page['Page']['id']);
	
		$this->set(compact('page'));
		$this->set('page_images',$page_images);
		
		
	}

	public function overview_menu() {
		if ($this->request->is('ajax')) {
			$this->loadModel('Menu');
			$page= $this->Menu->findByExtraParams($this->request->data['id']);
		}
		echo json_encode($page['Page']);
		$this->autoRender = false;
	}

	public function ajax_navmenu($id) { //overview page
		$this->autoRender = false;
		$this->loadModel('Menu');
		$page= $this->Menu->findByExtraParams($id);
		$meta=$this->Page->Metum->read(null,$page['Page']['id']);
		$allChildren = $this->Menu->children($page['Menu']['id']);
		$this->set(compact('meta','page','allChildren'));
		$this->disableCache();
		// render an element
		$this->viewPath = 'Elements';
		$this->render('/Elements/ajaxmenureturn');
	}

	public function ajax_navfootmenu($id) {
		$this->autoRender = false;
		$this->loadModel('Menu');
		$page= $this->Menu->findByExtraParams($id);
		$this->set(compact('page'));
		$this->disableCache();
		// render an element
		$this->viewPath = 'Elements';
		$this->render('/Elements/ajaxfootreturn');
		if ($id==30) $this->render('/Elements/ajaxreturn');
	}

	public function ajax_action($id) {  //detail page
		$this->autoRender = false;
		$this->loadModel('Menu');
		$page= $this->Menu->findByExtraParams($id);
		$this->set(compact('page'));
		$this->disableCache();
		// render an element
		$this->viewPath = 'Elements';
		$this->render('/Elements/ajaxreturn');
	}

	public function contact() {
		if ($this->request->is('ajax')) {
			$siteEmail= Configure::read('cmsSiteSetting.site_email');
			$name= $this->request->data['Page']['name'];
			$reciever= $this->request->data['Page']['email'];
			$subject= $this->request->data['Page']['subject'];
			$message= "Name: ".$name."\r\n";
			if (!empty($this->request->data['Page']['tel'])) {
				$message.= "Phone: ".$this->request->data['Page']['tel']."\r\n";
			}
			$message.= "Message: ".$this->request->data['Page']['message'];

			$email = new CakeEmail('default');
			$email ->from(array($reciever => $name))
			->to('info@emb.nl')
			->subject($subject)
			->send($message);

			$email1 = new CakeEmail('default');
			$email1 ->from(array($reciever => $name))
			->to('info@embav.nl')
			->subject($subject)
			->send($message);
		}
		$this->view = 'contact';
		$this->loadModel('Menu');
		$page= $this->Menu->findByRoutingLink('/contact');
		$this->set(compact('page'));
	}

	public function load_home(){
		$page = $this->Page->findById(1);
		$this->set('page', $page);
		$contact_page = $this->Page->findById(27);
		$meta=$this->Page->Metum->read(null,$page['Page']['id']);
		$this->set(compact('meta','contact_page'));
		$this->view = 'front_view';
//		$this->render('/Elements/common');
	}

	public function search()
	{
		$this->Page->recursive = 1;

		if(!empty($this->request->query['search'])) {
			$search_string=$this->request->query['search'];
			$conditions = array();
			$search_terms = explode(' ', $search_string);
			foreach($search_terms as $search_term) {
				$conditions[] = array('Page.title Like' => '%'.$search_term.'%');
				$conditions[] = array('Page.content_short Like' => '%'.$search_term.'%');
				$conditions[] = array('Page.content_long Like' => '%'.$search_term.'%');
				$conditions[] = array('Page.detail_text_left Like' => '%'.$search_term.'%');
				$conditions[] = array('Page.detail_text_bottom Like' => '%'.$search_term.'%');
			}

			$this->paginate=array(
                'conditions' => array('OR' => $conditions),
                'order' => array('Page.id' => 'desc'),
                'limit'=>100
			);
		}else{

			$this->paginate = array(
                'order' => array('Page.id' => 'desc'),
                'limit'=>100
			);
		}
		$this->set('search', $this->paginate());

		$this->set('title_for_layout', 'Search');

	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->Page->locale = $this->Session->read('Config.language');
		$this->set('pages', $this->paginate('Page'));
		$this->set('title_for_tab', ___('Static Pages'));

	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(___('Invalid page'));
		}
		$this->set('page', $this->Page->read(null, $id));
		$this->set('title_for_tab', ___('Pages'));

	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		$this->Page->setLocale(array('eng','dut','tha'));
		$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
		$this->Page->multiTranslateOptions(array('validate'=>true));

		if ($this->request->is('post')) {
			$this->Page->create();
			$this->request->data['Metum']['meta_title'] = $this->request->data['Page']['title'];
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(___('The page has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'edit', $this->Page->id));
			} else {
				$this->Session->setFlash(___('The page could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
		$this->set('title_for_tab', ___('Paged Page'));

	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->Page->setLocale(array('eng','dut','tha'));
		$this->Page->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(___('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				
				$this->Session->setFlash(___('The page has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
					$this->redirect($this->referer());
				}
			} else {
				debug($this->Page->validationErrors);
				exit();
				
				$this->Session->setFlash(___('The page could not be saved. Please, try again.'), 'admin/flash_error');
			}
			if($this->request->is('ajax')){
				$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
				$trans= $this->Page->find('first', array(
                    'recursive' => 1,
                    'conditions'=>array(
                        'Page.id'=>$id
				)
				));
				$this->set(compact('trans'));
				$this->layout = 'ajax';
				return;
			}
		} else {
			$this->request->data = $this->Page->read(null, $id);
			$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
			$trans= $this->Page->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Page.id'=>$id
			)
			));
			$this->set(compact('trans'));

			$this->Page->Metum->bindTranslation(array(
                'meta_title' => 'mtTranslation',
                'meta_description'=>'mdTranslation',
                'meta_keywords'=>'mkTranslation'
                )
                );

                $metrans= $this->Page->Metum->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Metum.id'=>$this->request->data['Page']['metum_id']
                )
                ));
                $this->set(compact('metrans'));


                // check if it has meta, if not create it
                if(empty($this->request->data['Metum']['id'])){
                	$this->request->data['Metum']['meta_title'] = $this->request->data['Page']['title'];

                	$this->loadModel('Metum');

                	$this->Metum->create();
                	if ($this->Metum->save($this->request->data)) {
                		$newInsertId = $this->Metum->id;
                		$this->Page->id=$id;
                		$this->Page->saveField('metum_id',$newInsertId);
                	}


                	$this->request->data = $this->Page->read(null, $id);
                }
		}
		$this->set('title_for_tab', ___('Edit Page'));

	}

	/**
	 * admin_overview method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_overview($id = null) {
		$this->Page->setLocale(array('eng','dut','tha'));
		$this->Page->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(___('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				$page_id = $this->Page->id;

				if(!empty($this->request->data['Page']['image'])){
					$result = array();

					$ImageFiles = $this->request->data['Page']['image'];
					foreach($ImageFiles as $key=>$ImageFile) {
						$_FILES['image']['name'] = $ImageFile['name'];
						$_FILES['image']['type'] = $ImageFile['type'];
						$_FILES['image']['tmp_name'] = $ImageFile['tmp_name'];
						$_FILES['image']['error'] = $ImageFile['error'];
						$_FILES['image']['size'] = $ImageFile['size'];

						$result[$key] = $this->ImageUpload->upload_images('PageImage', $page_id, 'image');
					}
					unset($this->request->data['Page']);
					foreach($result as $key=>$res){
						if(!isset($res['error'])){
							$this->request->data[] = array('image'=>$res['filename'],'page_id' => $page_id);
						}
					}
				}
				$this->Page->PageImage->saveAll($this->request->data);
				$this->Session->setFlash(___('The page has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
					$this->redirect($this->referer());
				}
			} else {
				$this->Session->setFlash(___('The page could not be saved. Please, try again.'), 'admin/flash_error');
			}
			if($this->request->is('ajax')){
				$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
				$trans= $this->Page->find('first', array(
                    'recursive' => 1,
                    'conditions'=>array(
                        'Page.id'=>$id
				)
				));
				$this->set(compact('trans'));
				$this->layout = 'ajax';
				return;
			}
		} else {
			$this->request->data = $this->Page->read(null, $id);
			$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
			$trans= $this->Page->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Page.id'=>$id
			)
			));
			$this->set(compact('trans'));

			$this->Page->Metum->bindTranslation(array(
                'meta_title' => 'mtTranslation',
                'meta_description'=>'mdTranslation',
                'meta_keywords'=>'mkTranslation'
                )
                );

                $metrans= $this->Page->Metum->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Metum.id'=>$this->request->data['Page']['metum_id']
                )
                ));
                $this->set(compact('metrans'));


                // check if it has meta, if not create it
                if(empty($this->request->data['Metum']['id'])){
                	$this->request->data['Metum']['meta_title'] = $this->request->data['Page']['title'];

                	$this->loadModel('Metum');

                	$this->Metum->create();
                	if ($this->Metum->save($this->request->data)) {
                		$newInsertId = $this->Metum->id;
                		$this->Page->id=$id;
                		$this->Page->saveField('metum_id',$newInsertId);
                	}


                	$this->request->data = $this->Page->read(null, $id);
                }
		}
		$this->set('title_for_tab', ___('Edit Page'));

	}
	/**
	 * admin_overview method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_points($id = null) {
		$this->Page->setLocale(array('eng','dut','tha'));
		$this->Page->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(___('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(___('The page has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
					$this->redirect($this->referer());
				}
			} else {
				$this->Session->setFlash(___('The page could not be saved. Please, try again.'), 'admin/flash_error');
			}
			if($this->request->is('ajax')){
				$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
				$trans= $this->Page->find('first', array(
                    'recursive' => 1,
                    'conditions'=>array(
                        'Page.id'=>$id
				)
				));
				$this->set(compact('trans'));
				$this->layout = 'ajax';
				return;
			}
		} else {
			$this->request->data = $this->Page->read(null, $id);
			$this->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));
			$trans= $this->Page->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Page.id'=>$id
			)
			));
			$this->set(compact('trans'));

			$this->Page->Metum->bindTranslation(array(
                'meta_title' => 'mtTranslation',
                'meta_description'=>'mdTranslation',
                'meta_keywords'=>'mkTranslation'
                )
                );

                $metrans= $this->Page->Metum->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Metum.id'=>$this->request->data['Page']['metum_id']
                )
                ));
                $this->set(compact('metrans'));


                // check if it has meta, if not create it
                if(empty($this->request->data['Metum']['id'])){
                	$this->request->data['Metum']['meta_title'] = $this->request->data['Page']['title'];

                	$this->loadModel('Metum');

                	$this->Metum->create();
                	if ($this->Metum->save($this->request->data)) {
                		$newInsertId = $this->Metum->id;
                		$this->Page->id=$id;
                		$this->Page->saveField('metum_id',$newInsertId);
                	}


                	$this->request->data = $this->Page->read(null, $id);
                }
		}
		$this->set('title_for_tab', ___('Edit Page'));

	}

	/**
	 * admin_delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		$pageData = $this->Page->read(null);
		if (!$pageData) {
			throw new NotFoundException(___('Invalid page'));
		}
		if ($this->Page->delete()) {
			$this->Page->Metum->delete($pageData['Metum']['id']);
			$this->Session->setFlash(___('Page deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(___('Page was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
	/**
	 * Delete image
	 */
	public function admin_delete_image(){
		$id = $this->request->data('id');
		if ($this->request->is('Ajax')) {
			if (!$this->Page->PageImage->exists($id)) {
				$status = false;
				$msg = __('Image does not exist');
			}
			$this->Page->PageImage->id = $id;
			$image_info = $this->Page->PageImage->findById($id);
			if ($this->Page->PageImage->delete()){
				App::uses('File', 'Utility');
				$file = new File(WWW_ROOT . 'files' . DS . 'PageImage' . DS . $image_info['PageImage']['page_id'] . DS . $image_info['PageImage']['image'], true, 0644);

				if(file_exists($file->path)){
					$file->delete();
					$file->close();
					$status = true;
					$msg = __('Image deleted successfully.');
				}

				$file = new File(WWW_ROOT . 'files' . DS . 'PageImage' . DS . $image_info['PageImage']['page_id'] . DS .'thumb'. DS .'thumb_'. $image_info['PageImage']['image'], true, 0644);

				if(file_exists($file->path)){
					$file->delete();
					$file->close();
					$status = true;
					$msg = __('Image deleted successfully.');
				}

			}
			echo json_encode(array('status' => $status,'msg' => $msg));
			exit;
		}
	}

	public function admin_choose_banner($id=null) {

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(___('Invalid page'));
		}

		$banners = $this->Page->Banner->find('list');
		$this->set(compact('banners'));

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(___('The banner choice has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(___('The banner choice could not be saved. Please, try again.'), 'admin/flash_error');
			}
			if($this->request->is('ajax')){
				$this->layout = 'ajax';
				return;
			}
		}
		$this->set('title_for_tab', ___('Choose Page Banner'));
	}
}
