<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

        public $helpers = array(
            'Text',
            'Time',
            'Cms',
            'Session',
            'Fancybox.Fancybox'
        );

        public $components = array(
            'Auth' => array(
                'flash' => array(
                    'element' => 'admin/flash_error',
                    'key' => 'auth',
                    'params' => array()
                )
            ),
            'RequestHandler',
            'Session'
        );


        public function beforeFilter() {
                parent::beforeFilter();
                $this->setLayout();
                $this->setConfigure();
                @define('BASE_URL', 'http://' . $_SERVER['SERVER_NAME'] . $this->base);
        }

         public function appError($error) {
            $this->redirect('/');
        }

        public function setLayout()
        {
          if (!empty($this->request->params['admin'])) {

                    $this->Auth->loginRedirect = aca('dashboard', 'users');
                    if($this->here !== $this->webroot.'l2g1n'){
                            $this->Auth->loginAction = '/';
                    }
                    // set different layout for admin
                    $this->layout = 'admin';
            } else {
                    // allow everything for now
                    $this->Auth->allow('*');
                    $this->layout = 'front';
            }
        }

        public function setConfigure()
        {
          // language
                // set language in session
                $languageChosen = $this->Session->read('User.language');
                if($languageChosen){
                  Configure::write('Config.language', 'eng');
                }

                // site setting
                $this->loadModel('SiteSetting');
                $siteSettings = $this->SiteSetting->getSiteSetting();
                Configure::write('cmsSiteSetting', @$siteSettings['SiteSetting']);
        }

        /**
         * Search function
         */
        protected function _setSearch($modelName = null) {
                if(!$modelName){
                        $modelName = $this->modelClass;
                }
                if ($this->request->is('post')) {
                        $this->Session->write('search.' . $this->name, $this->request->data[$modelName]['search']);
                }

                // set the search value
                $searchValue = trim($this->Session->read('search.'.$this->name));
                $this->request->data[$modelName]['search'] = $searchValue;

                return $searchValue;
        }

        public function beforeRender() {
                parent::beforeRender();
                $menuData = Configure::read('currentMenuData.Menu');
                $this->loadModel('DisplayOption');
                $this->loadModel('PageElement');
                if($menuData) {
                        $options = array(
                                'conditions' => array(
                                        'DisplayOption.id' => $menuData['display_option_id']
                                ),
                                'recursive' => -1
                                );

                        // get elements
                        $pr = $this->DisplayOption->find('first', $options);
                }

                $cmsDisplayElement['header'] = $this->PageElement->getDefaultElement('header',@$pr['DisplayOption']['header']);
                $cmsDisplayElement['footer'] = $this->PageElement->getDefaultElement('footer',@$pr['DisplayOption']['footer']);
                $cmsDisplayElement['page_description'] = $this->PageElement->getDefaultElement('page_description',@$pr['DisplayOption']['page_description']);
                $cmsDisplayElement['top_banner'] = $this->PageElement->getDefaultElement('top_banner',@$pr['DisplayOption']['top_banner']);
                Configure::write('cmsDisplayElement', $cmsDisplayElement);



        }
}
