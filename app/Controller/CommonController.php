<?php
App::uses('AppController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 */
class CommonController extends AppController {

        public function beforeFilter() {
                parent::beforeFilter();

                $this->autoRender = false;
        }


        public function language($lang=null) {

                if(in_array($lang, array_keys(Configure::read('Setting.languages')))){
                        $this->Session->write('User.language', $lang);
                        if ($lang == 'eng') {
                                $this->Session->write('lang_id', 1);
                        }
                        else if ($lang == 'dut') {
                                $this->Session->write('lang_id', 2);
                        }
                        else if ($lang == 'tha') {
                                $this->Session->write('lang_id', 3);
                        }
                }
                $this->redirect($this->referer());
        }

        public function html() {

                $this->render(false, 'merkur');

        }

}
