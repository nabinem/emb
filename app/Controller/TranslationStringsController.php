<?php
App::uses('AppController', 'Controller');
/**
 * TranslationStrings Controller
 *
 * @property TranslationString $TranslationString
 */
class TranslationStringsController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

                // begin for search
                $searchValue = strtoupper($this->_setSearch());

                $this->paginate = array(
                    'conditions' => array(
                        array('OR' => array(' UPPER(TranslationString.name) LIKE' => "%$searchValue%"))
                    ),
                    'order' => array('TranslationString.id' => 'desc')
                );

                if($searchValue === false) {
                        unset($this->paginate['conditions']);
                }
                // end for search

		$this->TranslationString->recursive = 0;
		$this->TranslationString->locale = Configure::read('Setting.languageDefault');
		$this->set('translationStrings', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->TranslationString->id = $id;
		if (!$this->TranslationString->exists()) {
			throw new NotFoundException(___('Invalid translation string'));
		}
		$this->set('translationString', $this->TranslationString->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TranslationString->create();
			if ($this->TranslationString->save($this->request->data)) {
				$this->Session->setFlash(___('The translation string has been saved'), 'admin/flash_success');
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(___('The translation string could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->TranslationString->id = $id;
		if (!$this->TranslationString->exists()) {
			throw new NotFoundException(___('Invalid translation string'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TranslationString->save($this->request->data)) {
				$this->Session->setFlash(___('The translation string has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(___('The translation string could not be saved. Please, try again.'), 'admin/flash_error');
			}
		} else {
			$this->request->data = $this->TranslationString->translationEdit($id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TranslationString->id = $id;
		if (!$this->TranslationString->exists()) {
			throw new NotFoundException(___('Invalid translation string'));
		}
		if ($this->TranslationString->delete()) {
			$this->Session->setFlash(___('Translation string deleted'), 'admin/flash_success');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(___('Translation string was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
