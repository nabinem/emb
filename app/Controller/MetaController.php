<?php
App::uses('AppController', 'Controller');
/**
 * Meta Controller
 *
 * @property Metum $Metum
 */
class MetaController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Metum->recursive = 0;
		$this->set('meta', $this->paginate());
        $this->set('title_for_tab', ___('Meta'));

	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Metum->id = $id;
		if (!$this->Metum->exists()) {
			throw new NotFoundException(___('Invalid metum'));
		}
		$this->set('metum', $this->Metum->read(null, $id));
        $this->set('title_for_tab', ___('Meta'));

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Metum->create();
			if ($this->Metum->save($this->request->data)) {
				$this->Session->setFlash(___('The metum has been saved'), 'admin/flash_success\n');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(___('The metum could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
        $this->set('title_for_tab', ___('Add Meta Details'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Metum->setLocale(array('eng','dut','tha'));
        $this->Metum->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Metum->id = $id;
		if (!$this->Metum->exists()) {
			throw new NotFoundException(___('Invalid metum'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Metum->save($this->request->data)) {
				$this->Session->setFlash(___('The metum has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
                        $this->redirect(array('action' => 'index'));
                }

			} else {
				$this->Session->setFlash(___('The metum could not be saved. Please, try again.'), 'admin/flash_error');

			}

            if($this->request->is('ajax')){

                $this->Metum->bindTranslation(array(
                    'meta_title' => 'mtTranslation',
                    'meta_description'=>'mdTranslation',
                    'meta_keywords'=>'mkTranslation'
                    )
                );
                $metrans= $this->Metum->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Metum.id'=>$id
                    )
                ));
                $this->set(compact('metrans'));

                $this->layout = 'ajax';
                return;
            }

		} else {
			$this->request->data = $this->Metum->read(null, $id);
			$metrans= $this->Metum->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Metum.id'=>$id
                    )
                ));
           
            $this->set(compact('metrans'));
		}
        $this->set('title_for_tab', ___('Edit Meta Details'));

	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Metum->id = $id;
		if (!$this->Metum->exists()) {
			throw new NotFoundException(___('Invalid metum'));
		}
		if ($this->Metum->delete()) {
			$this->Session->setFlash(___('Metum deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(___('Metum was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
