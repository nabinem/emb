<?php
App::uses('AppController', 'Controller');
/**
 * SiteSettings Controller
 *
 * @property SiteSetting $SiteSetting
 */
class SiteSettingsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
        $this->admin_edit();
	}

 /**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->view = 'admin_edit';
        $setting = $this->SiteSetting->getSiteSetting();
        $this->SiteSetting->id = $setting['SiteSetting']['id'];
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SiteSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The site setting has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The site setting could not be saved. Please, try again.'), 'admin/flash_error');
			}
		} else {
			$this->request->data = $setting;
		}
        $this->set('title_for_tab', ___('Edit Site Setting'));

	}
}
