<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {

/**
 * index method for all category overview
 *
 * @return void
 */
	public function index() {
		$this->Category->recursive = -1;
		$this->paginate = array('conditions' => array('Category.parent_id'=>NULL));
		$this->set('categories', $this->paginate());
	}

/**
 * view method for main category overview
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($slug = null) {

		$category = $this->Category->find('first', array(
			'recursive' => 1,
			'conditions' => array(
				'Category.slug' => $slug
			)
		));
		if(empty($category)) {
			$this->redirect(array('action' => 'index'));
		}

		$this->set(compact('category'));

        $this->loadModel('Product');
        $products = $this->Product->find('all',array(
                'recursive'=>-1,
                'conditions' => array(
                    'Product.category_id' => $category['Category']['id']
                    ),
                'order' => array('Product.id' => 'desc'),
                ));


        $newArray = array();
        foreach ($category['ChildCategory'] as $cat) {
            $newArray[] = $this->Product->find('all',array(
                'recursive'=>-1,
                'conditions' => array(
                    'Product.category_id' => $cat['id']
                    ),
                'order' => array('Product.id' => 'desc'),
                ));
        }
        $temp=array();
        foreach ($newArray as $key => $new) {
            foreach ($new as $n) {
                $temp[]=$n;
            }
        }

        $products=array_merge_recursive($products,$temp);
        $this->set(compact('products'));

	}

    public function array_union_recursive(Array $a, Array $b)
    {
        foreach($a as $k => &$v)
        {
            if (is_array($v) && isset($b[$k]) && is_array($b[$k]))
            {
                $v = array_union_recursive($a[$k], $b[$k]);
            }
        }

        $a += $b;
        return $a;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return voidz
 */
	public function view_category($id = null) {
            $category=$this->Category->findById($id);
            $this->set('category',$category);
	}

        public function view_subcategory() {
            $parent=$this->request->params['category'];
            $id=$this->request->params['subcategory'];
            $category=$this->Category->findById($id);
            $this->set('category',$category);
            $this->paginate = array(
                                'conditions' => array('Product.category_id' => $parent,'Product.category_id' => $id),
                                'fields' => array(
                                    'Product.id',
                                    'Product.image',
                                    'Product.name',
                                    'Product.price',
                                ),
                                'order' => array('Product.id' => 'desc'),
                                'limit' => 2,
                                );

            $pro = $this->paginate('Product');
            $this->set(compact('pro'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($filter=null) {
        $this->Category->locale = $this->Session->read('Config.language');

		if(!empty($filter)){
            if($filter=="byCategory"){
             	$this->Category->recursive = -1;
				$this->paginate = array('conditions' => array('Category.parent_id'=>NULL));
            }
            else{
                $this->Category->recursive = 0;
            }
        }
        $this->set('filter',$filter);
		$this->set('categories', $this->paginate('Category'));
        $this->set('title_for_tab', ___('Categories'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
        $this->Category->recursive = -1;
		$this->paginate = array('conditions' => array('Category.parent_id'=>$id));
		$this->set('categories', $this->paginate());
        $this->set('title_for_tab', ___('Sub Categories'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
        $this->Category->setLocale(array('eng','dut','tha'));
        $this->Category->bindTranslation(array(
            'name' => 'titleTranslation'
            ));
        $this->Category->multiTranslateOptions(array('validate'=>true));

		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}

		$parents = $this->Category->getTreeList();
		$meta = $this->Category->Metum->find('list');
		$this->set(compact('parents', 'meta'));
        $this->set('title_for_tab', ___('Add Category'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Category->setLocale(array('eng','dut','tha'));
        $this->Category->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'admin/flash_error');
			}
		} else {
			$this->request->data = $this->Category->read(null, $id);

            $this->Category->bindTranslation(array('name' => 'titleTranslation'));
            $trans= $this->Category->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Category.id'=>$id
                    )
                ));
            $this->set(compact('trans'));
		}
		$parents = $this->Category->getTreeList();

		$meta = $this->Category->Metum->find('list');
		$this->set(compact('parents', 'meta'));
        $this->set('title_for_tab', ___('Edit Category'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('Category deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
