<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
        $this->set('title_for_tab', ___('Users'));
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(___('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
        $this->set('title_for_tab', ___('Users'));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(___('The user has been saved'), 'admin/flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(___('The user could not be saved. Please, try again.'), 'admin/flash_error');
            }
        }
        $groups = $this->User->Group->find('list');
                $statuses = $this->User->getUserStates();
        $this->set(compact('groups', 'statuses'));
        $this->set('title_for_tab', ___('Add User'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(___('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(___('The user has been saved'), 'admin/flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(___('The user could not be saved. Please, try again.'), 'admin/flash_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
        $groups = $this->User->Group->find('list');
        $statuses = $this->User->getUserStates();
        $this->set(compact('groups', 'statuses'));
        $this->set('title_for_tab', ___('Edit User'));
    }

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(___('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(___('User deleted'), 'admin/flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(___('User was not deleted'), 'admin/flash_error');
        $this->redirect(array('action' => 'index'));
    }


/**
 * Login area for the users.
 */
        public function admin_login() {

        if($this->Auth->loggedIn()){
                        return $this->redirect($this->Auth->redirect());
                }

                App::uses('Security', 'Utility');

                // check for cookie
//                $text = Security::cipher("some test text", Configure::read('Security.cipherSeed'));
//                echo base64_encode($text)." \n";
//                echo Security::cipher(base64_decode("0ivocQGIqoKaPWo9POs="), Configure::read('Security.cipherSeed'));


                if ($this->request->is('post')) {
                        if ($this->Auth->login()) {
                                // do necessery loggin when user login in.
                                $this->User->id = $this->Auth->user('id');
                                $this->User->saveField('last_login', date('Y-m-d H:i:s'));
                                $this->Session->write('KCFINDER.disabled', false);
                                $this->Session->write('KCFINDER.uploadURL', $this->webroot."vfiles");

                                return $this->redirect($this->Auth->redirect());
                        } else {
                                $this->Session->setFlash(___('Username or password is incorrect'), 'admin/flash_error', array(), 'auth');
                        }
                }

        }

/**
 * Logout.
 */
        public function admin_logout() {

                // do things before logout
                $this->Session->write('KCFINDER.disabled', true);

                $this->redirect($this->Auth->logout());
        }

/**
 * Admin dashboard with icons.
 */
    public function admin_dashboard() {}
}
