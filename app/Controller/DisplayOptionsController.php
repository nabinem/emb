<?php
App::uses('AppController', 'Controller');
/**
 * DisplayOptions Controller
 *
 * @property DisplayOption $DisplayOption
 */
class DisplayOptionsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->DisplayOption->recursive = 0;
		$this->set('displayOptions', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->DisplayOption->id = $id;
		if (!$this->DisplayOption->exists()) {
			throw new NotFoundException(__('Invalid display option'));
		}
		$this->set('displayOption', $this->DisplayOption->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->DisplayOption->create();
			if ($this->DisplayOption->save($this->request->data)) {
				$this->Session->setFlash(__('The display option has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The display option could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->DisplayOption->id = $id;
		if (!$this->DisplayOption->exists()) {
			throw new NotFoundException(__('Invalid display option'));
		}
                $this->set('headers', $this->DisplayOption->HeaderElement->getElementList('header'));
                $this->set('topBanners', $this->DisplayOption->HeaderElement->getElementList('top_banner'));
                $this->set('pageDescriptions', $this->DisplayOption->HeaderElement->getElementList('page_description'));
                $this->set('footers', $this->DisplayOption->HeaderElement->getElementList('footer'));
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->DisplayOption->save($this->request->data)) {
				$this->Session->setFlash(__('The display option has been saved'), 'admin/flash_success');
				if(!$this->request->is('ajax')){
                                        $this->redirect(array('action' => 'index'));
                                }
			} else {
				$this->Session->setFlash(__('The display option could not be saved. Please, try again.'), 'admin/flash_error');
			}
                        if($this->request->is('ajax')){
                                $this->layout = 'ajax';
                                return;
                        }
		} else {
			$this->request->data = $this->DisplayOption->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->DisplayOption->id = $id;
		if (!$this->DisplayOption->exists()) {
			throw new NotFoundException(__('Invalid display option'));
		}
		if ($this->DisplayOption->delete()) {
			$this->Session->setFlash(__('Display option deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Display option was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
