<?php
App::uses('AppController', 'Controller');
/**
 * PageElements Controller
 *
 * @property PageElement $PageElement
 */
class PageElementsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PageElement->recursive = 0;
		$this->set('pageElements', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->PageElement->id = $id;
		if (!$this->PageElement->exists()) {
			throw new NotFoundException(__('Invalid page element'));
		}
		$this->set('pageElement', $this->PageElement->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PageElement->create();
			if ($this->PageElement->save($this->request->data)) {
				$this->Session->setFlash(__('The page element has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page element could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
                $this->set('types', $this->PageElement->elementType);
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->PageElement->id = $id;
		if (!$this->PageElement->exists()) {
			throw new NotFoundException(__('Invalid page element'));
		}
                $this->set('types', $this->PageElement->elementType);
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PageElement->save($this->request->data)) {
				$this->Session->setFlash(__('The page element has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page element could not be saved. Please, try again.'), 'admin/flash_error');
			}
		} else {
			$this->request->data = $this->PageElement->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PageElement->id = $id;
		if (!$this->PageElement->exists()) {
			throw new NotFoundException(__('Invalid page element'));
		}
		if ($this->PageElement->delete()) {
			$this->Session->setFlash(__('Page element deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page element was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
