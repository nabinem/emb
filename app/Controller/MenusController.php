<?php
App::uses('AppController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 */
class MenusController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
        $this->Menu->locale = $this->Session->read('Config.language');
         $this->paginate = array(
                // 'conditions' => array('Menu.is_visible' => 0)
            );
        $this->set('menus', $this->paginate('Menu'));
        $this->set('title_for_tab', ___('Menus'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(___('Invalid menu'));
		}
		$this->set('menu', $this->Menu->read(null, $id));
        $this->set('title_for_tab', ___('Menus'));

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {

        $this->Menu->setLocale(array('eng','dut','tha'));
        $this->Menu->bindTranslation(array('name' => 'nameTranslation'));
        $this->Menu->multiTranslateOptions(array('validate'=>false));

		if ($this->request->is('post')) {
			$this->Menu->create();
            $this->Menu->set($this->request->data);
            if($this->Menu->validates() AND $this->request->data['Menu']['from_link'] === 'new-page'){
                    if($this->Menu->Page->save($data)){
                        $this->request->data['Menu']['from_link'] = '/pages/view/'.$this->Menu->Page->id;
                    }
            }
			$this->_parseFromLink();

            if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(___('The menu has been saved'), 'admin/flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(___('The menu could not be saved. Please, try again.'), 'admin/flash_error');
			}
		}
        $this->_getAvailableLinks();
        $this->set('parents', $this->Menu->getTreeList());
        $this->set('title_for_tab', ___('Add Menu'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Menu->setLocale(array('eng','dut','tha'));
        $this->Menu->multiTranslateOptions(array('validate'=>false,'find'=>false));

		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(___('Invalid menu'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
            $this->_parseFromLink();
            if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(___('The menu has been saved'), 'admin/flash_success');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(___('The menu could not be saved. Please, try again.'), 'admin/flash_error');
			}
		} else {
			$this->request->data = $this->Menu->read(null, $id);

            $this->Menu->bindTranslation(array('name' => 'nameTranslation'));
            $mtrans= $this->Menu->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Menu.id'=>$id
                    )
                ));
            $this->set(compact('mtrans'));

            $this->Menu->Page->bindTranslation(array('title' => 'titleTranslation', 'content_short'=>'shortTranslation', 'content_long'=>'bodyTranslation'));

            $trans= $this->Menu->Page->find('first', array(
                'recursive' => 1,
                'conditions'=>array(
                    'Page.id'=>$this->request->data['Page']['id']
                    )
                ));
            $this->set(compact('trans'));

            $this->Menu->Page->Metum->bindTranslation(array(
                'meta_title' => 'mtTranslation',
                'meta_description'=>'mdTranslation',
                'meta_keywords'=>'mkTranslation'
                )
            );

            $metrans= $this->Menu->Page->Metum->find('first', array(
                    'recursive' => 1,
                    'conditions'=>array(
                        'Metum.id'=>$this->request->data['Page']['metum_id']
                        )
                    ));
                $this->set(compact('metrans'));

                 // check if it has meta, if not create it
            if(empty($this->request->data['Metum']['id']) && empty($this->request->data['Page']['metum_id'])){
                $this->request->data['Metum']['meta_title'] = $this->request->data['Page']['title'];

                $this->loadModel('Metum');

                $this->Metum->create();
                if ($this->Metum->save($this->request->data)) {
                    $newInsertId = $this->Metum->id;

                    $this->loadModel('Page');
                    $this->Page->id=$this->request->data['Page']['id'];
                    $this->Page->saveField('metum_id',$newInsertId);
                    $this->request->data = $this->Menu->read(null, $id);
                }
            }

            if($this->request->data['Page']['metum_id']){
               $this->request->data = array_merge($this->request->data, $this->Menu->Page->Metum->findById($this->request->data['Page']['metum_id']));

            }
        }

        $this->_getAvailableLinks();
        $this->set('parents', $this->Menu->getTreeList());
        $this->set('title_for_tab', ___('Edit Menu'));

	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(___('Invalid menu'));
		}        
        $menu = $this->Menu->read(null,$id);
        $this->Menu->Page->id = $menu['Page']['id'];
        $page = $this->Menu->Page->read(null,$menu['Page']['id']);        
		if ($this->Menu->delete()) {
                $this->Menu->Page->delete();
                App::uses('File', 'Utility');
                
				$file = new File(WWW_ROOT . 'files' . DS . 'PageImage' . DS . $image_info['PageImage']['page_id'] . DS . $image_info['PageImage']['image'], true, 0644);
				
				if(file_exists($file->path)){
					$file->delete();
					$file->close();
					$status = true;
					$msg = __('Image deleted successfully.');
				}
				
				$file = new File(WWW_ROOT . 'files' . DS . 'PageImage' . DS . $image_info['PageImage']['page_id'] . DS .'thumb'. DS .'thumb_'. $image_info['PageImage']['image'], true, 0644);
				
				if(file_exists($file->path)){
					$file->delete();
					$file->close();
					$status = true;
					$msg = __('Image deleted successfully.');
				}
			$this->Session->setFlash(___('Menu deleted'), 'admin/flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(___('Menu was not deleted'), 'admin/flash_error');
		$this->redirect(array('action' => 'index'));
	}


/**
 * Change the order of the menu.
 */
        public function admin_change_order($dir, $id) {

                $res = $this->Menu->moveTreeElement($dir, $id, 1);

                if($res){
                       $this->Session->setFlash(___('Menu item moved %s', $dir), 'admin/flash_success', null, 'admin_sidebar');
                }else{
                        $this->Session->setFlash(___('Menu item did not move'), 'admin/flash_error', null, 'admin_sidebar');
                }

                $this->redirect($this->referer());

//
//                $this->admin_order();
//                $layout = ($this->request->isAjax() === true)? false : null ;
//                $this->render('admin_order', $layout);
        }

/**
 * Parse the provided link into array url for saving.
 * @return type
 */
        protected function _parseFromLink() {
                if(isset($this->request->data['Menu']['from_link'])){
                        $parsedRoute = Router::parse($this->request->data['Menu']['from_link']);
                        $this->request->data['Menu']['controller_name'] = $parsedRoute['controller'];
                        $this->request->data['Menu']['action_name'] = $parsedRoute['action'];
                        $this->request->data['Menu']['extra_params'] = implode('/', $parsedRoute['pass']);
                }
		return;
        }

        protected function _getAvailableLinks() {
                $this->loadModel('Page');
                $pages = $this->Page->find('list');

                $links['Pages']['new-page'] = '-- '.___('Add New Page').' --';
                foreach($pages as $k => $page) {
                    $links['Pages']["/pages/view/$k"] = "[$k] $page";
                }

                $this->set('fromLinks', $links);
        }
}
