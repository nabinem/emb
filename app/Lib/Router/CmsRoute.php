<?php

/**
 * Custom router for Proshore CMS.
 *
 * @property Menu $Menu
 */
App::uses('CakeRoute', 'Routing/Route');

class CmsRoute extends CakeRoute {

        public function parse(&$url){             
                $url = rtrim($url, "/");
                App::uses('Menu', 'Model');
                $this->Menu = new Menu();
                $routeUrl = $this->Menu->getMatchingRoute($url);

                if ($routeUrl) {
                        $url = $routeUrl;
                }
                
                return false;
        }

        public function match($url) {                
                App::uses('Menu', 'Model');
                $this->Menu = new Menu();
                $match = $this->Menu->getRoutingMatch($url);                
                return $match;
        }

}