// JavaScript Document
$.noConflict();
  jQuery(document).ready(function($) {
    // Code that uses jQuery's $ can follow here.
		$('.tt').tooltip({placement:'top'});
		
		$('.menu_title.menu_ctrl').click(function(){
			$('.v_menu').slideToggle(300, 'swing');
			
			$(this).find('i').toggleClass('icon-chevron-up icon-chevron-down');
		})
		
		
		$('.lang_chooser').append('<span class="win_s"></span>');
		/*$(window).resize(function(e) {
			var win=$(window).width();
            $('.win_s').text(win);
        });*/
		
		$('.v_menu ul ul').hide();
		$('.v_menu .plus').click(function(e) {
			
            $(this).closest('li').find('ul:first').slideToggle(300, 'swing');
			$(this).find('i').toggleClass('icon-plus icon-minus');
			return false;
			prevent.defaut();
        });
		
		$('.nav_tabs li').each(function(index){
			$(this).css({'z-index':8000-index});
		});
		
		
		$('.compact_menu').append('<select class="menu"></select>')
		$('ul.menu li').each(function(){
					var href=$(this).find('a').attr('href');
					var text=$(this).find('a').text();
					$('select.menu').append('<option value="'+href+'">'+text+'</option>');
					if($(this).is('.current')){
					}
		});
		
		$('select.menu').change(function(){
			document.location.href=$(this).attr('value');
			})
    
    // internal javascript tab, form submission handeling
    var $realTabs = $('.nav_tabs li:not(.js-content-tab)').length;
    $('div.pos:gt(0)').hide();
    $('.js-content-tab').click(
      function(e){
        e.preventDefault();
        var $this = $(this);
        $('div.pos').hide();
        $('div.pos:eq('+($this.index()-$realTabs)+')').show();
        $this.addClass('selected_tab').siblings().removeClass('selected_tab');
        delete $this;
      }
    );
      
    $('div.pos:gt(0) form').live('submit', function(e){
      e.preventDefault();
      var $this = $(this);
      $.post(this.action, $this.serialize(), function(data){
        var $parent = $this.parent('div.pos');
        $parent.after(data);
        var $newForm = $parent.next();
        $parent.remove();
        $newForm.trigger('ajaxFormLoad', [$newForm]);
        delete $this, $parent, $newForm;
      });
      $("html, body").animate({ scrollTop: 0 }, "slow");
    });
    
    $('.js-from-links').change(function(e){
      ($(this).val() == 'custom') ? alert('hi') : null;
    });
    
    $('div.pos').live('ajaxFormLoad', function(newForm){
      var $tAreas = $(newForm.currentTarget).find('textarea.ckeditor');
      $tAreas.each(function(){
        CKEDITOR.instances[this.id].destroy(true);
        $(this).ckeditor();
      });
    });
    
  });
  // Code that uses other library's $ can follow here.
