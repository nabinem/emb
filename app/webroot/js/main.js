$(function(){
  
  $('img.grayscale_image').live({
    mouseenter: function(){
      grayscale.reset(this);
    },
    mouseleave: function(){
      grayscale(this);
    }
  });
  $('input.sticker_toggle').change(function(){
    var $this = $(this);
    var changeType = $this.is(':checked')?'add':'remove';
      $.get(PCMS.webroot+'stickers/collection/'+changeType+'/'+$this.val(), function(data){
        if(data == 'done'){
          if(changeType == 'add'){
            grayscale($this.prev().addClass('grayscale_image'));
          }else{
            grayscale.reset($this.prev().removeClass('grayscale_image'));
          } 
        }
      });
  });
  
});

$(window).load(function(){
  grayscale($('img.grayscale_image'));
})
