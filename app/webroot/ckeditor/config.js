/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

  config.toolbar_MyFull =
  [
  	{ name: 'document', items : [ 'Source','-','NewPage','DocProps','Preview','Print','-','Templates' ] },
  	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
  	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
  	{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
  	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
  	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv']},
        { name: 'paragraph2', items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
  	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
  	{ name: 'insert', items : [ 'Video','Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
  	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
  	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
  	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
  ];

  config.toolbar = 'MyFull';
    config.filebrowserBrowseUrl = PCMS.webroot+'kcfinder/browse.php';
    config.filebrowserImageBrowseUrl = PCMS.webroot+'kcfinder/browse.php';
    config.filebrowserFlashBrowseUrl = PCMS.webroot+'kcfinder/browse.php';
    config.filebrowserUploadUrl = PCMS.webroot+'kcfinder/upload.php';
    config.filebrowserImageUploadUrl = PCMS.webroot+'kcfinder/upload.php';
    config.filebrowserFlashUploadUrl = PCMS.webroot+'kcfinder/upload.php';

    config.extraPlugins = 'mediaembed,video';

    config.enterMode = CKEDITOR.ENTER_BR;
    config.entities_greek = false;
    config.entities_latin = false;

};
