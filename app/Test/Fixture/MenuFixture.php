<?php
/**
 * MenuFixture
 *
 */
class MenuFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'routing_link' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'used for routing', 'charset' => 'utf8'),
		'is_visible' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'is_active' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'comment' => 'menu tree'),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null, 'comment' => 'menu tree'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null, 'comment' => 'menu tree'),
		'controller_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'action_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'extra_params' => array('type' => 'binary', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'routing_link' => 'Lorem ipsum dolor sit amet',
			'is_visible' => 1,
			'is_active' => 1,
			'parent_id' => 1,
			'lft' => 1,
			'rght' => 1,
			'controller_name' => 'Lorem ipsum dolor sit amet',
			'action_name' => 'Lorem ipsum dolor sit amet',
			'extra_params' => 'Lorem ipsum dolor sit amet',
			'created' => '2012-11-22 12:18:07',
			'modified' => '2012-11-22 12:18:07'
		),
	);

}
