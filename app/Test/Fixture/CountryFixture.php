<?php
/**
 * CountryFixture
 *
 */
class CountryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'iso_code_2' => array('type' => 'string', 'null' => false, 'length' => 2, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'iso_code_3' => array('type' => 'string', 'null' => false, 'length' => 3, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'iso_code_2' => '',
			'iso_code_3' => 'L',
			'created' => '2013-03-05 08:22:08',
			'modified' => '2013-03-05 08:22:08'
		),
	);

}
