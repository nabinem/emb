<?php
/**
 * DisplayOptionFixture
 *
 */
class DisplayOptionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'header' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'top_banner' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'page_description' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'footer' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'header' => 1,
			'top_banner' => 1,
			'page_description' => 1,
			'footer' => 1,
			'created' => '2012-12-12 19:21:45',
			'modified' => '2012-12-12 19:21:45'
		),
	);

}
