<?php
App::uses('PageElement', 'Model');

/**
 * PageElement Test Case
 *
 */
class PageElementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.page_element'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PageElement = ClassRegistry::init('PageElement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PageElement);

		parent::tearDown();
	}

}
