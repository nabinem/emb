<?php
App::uses('DisplayOption', 'Model');

/**
 * DisplayOption Test Case
 *
 */
class DisplayOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.display_option',
		'app.menu',
		'app.page',
		'app.metum',
		'app.banner',
		'app.category',
		'app.sticker',
		'app.diaplay_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DisplayOption = ClassRegistry::init('DisplayOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DisplayOption);

		parent::tearDown();
	}

}
