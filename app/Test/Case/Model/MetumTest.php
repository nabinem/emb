<?php
App::uses('Metum', 'Model');

/**
 * Metum Test Case
 *
 */
class MetumTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.metum'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Metum = ClassRegistry::init('Metum');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Metum);

		parent::tearDown();
	}

}
