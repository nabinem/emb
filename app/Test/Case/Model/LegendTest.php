<?php
App::uses('Legend', 'Model');

/**
 * Legend Test Case
 *
 */
class LegendTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.legend'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Legend = ClassRegistry::init('Legend');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Legend);

		parent::tearDown();
	}

}
