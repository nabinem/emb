<?php
App::uses('PageImage', 'Model');

/**
 * PageImage Test Case
 *
 */
class PageImageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.page_image',
		'app.page',
		'app.metum',
		'app.menu',
		'app.display_option',
		'app.page_element'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PageImage = ClassRegistry::init('PageImage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PageImage);

		parent::tearDown();
	}

}
