<?php
App::uses('Analytic', 'Model');

/**
 * Analytic Test Case
 *
 */
class AnalyticTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.analytic',
		'app.customer',
		'app.order',
		'app.order_item',
		'app.product',
		'app.category',
		'app.metum',
		'app.brand',
		'app.image'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Analytic = ClassRegistry::init('Analytic');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Analytic);

		parent::tearDown();
	}

}
