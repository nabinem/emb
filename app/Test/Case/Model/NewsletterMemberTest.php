<?php
App::uses('NewsletterMember', 'Model');

/**
 * NewsletterMember Test Case
 *
 */
class NewsletterMemberTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.newsletter_member'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NewsletterMember = ClassRegistry::init('NewsletterMember');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NewsletterMember);

		parent::tearDown();
	}

}
