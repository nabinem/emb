<?php
App::uses('ProductSize', 'Model');

/**
 * ProductSize Test Case
 *
 */
class ProductSizeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_size',
		'app.product',
		'app.category',
		'app.metum',
		'app.brand',
		'app.image',
		'app.order_item',
		'app.order',
		'app.analytic',
		'app.customer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductSize = ClassRegistry::init('ProductSize');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductSize);

		parent::tearDown();
	}

}
