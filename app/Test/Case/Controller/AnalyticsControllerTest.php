<?php
App::uses('AnalyticsController', 'Controller');

/**
 * AnalyticsController Test Case
 *
 */
class AnalyticsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.analytic',
		'app.customer',
		'app.order',
		'app.order_item',
		'app.product',
		'app.category',
		'app.metum',
		'app.brand',
		'app.image'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
